package dbaccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bookkeeper.CompanyConstants;
import businessobjects.CustomerInvoice;

/**
 * <h1>Customer Invoice Access</h1> 
 * This final class provides functions to access customer invoices from the database.
 */
public final class CustomerInvoiceAccess {

	/**
	 * Constants for the table and column names of the database.
	 */
	private static final String TABLE_NAME = "customer_invoices";

	private static final String COLUMN_NAME_INVOICE_ID = "invoice_id"; 			
	private static final String COLUMN_NAME_CUSTOMER_ID = "customer_id"; 		
	private static final String COLUMN_NAME_STATUS = "status";
	private static final String COLUMN_NAME_ISSUE_DATE = "issue_date";
	private static final String COLUMN_NAME_DUE_DATE = "due_date";
	private static final String COLUMN_NAME_DELIVERY_DATE = "delivery_date";
	private static final String COLUMN_NAME_TAX_RATE = "tax_rate"; 				
	private static final String COLUMN_NAME_DISCOUNT = "discount"; 			

	/*
	 * Private constructor.
	 */
	private CustomerInvoiceAccess() {

	}

	/**
	 * This method adds the submitted customer invoice to database.
	 * @param invoice
	 * @throws SQLException
	 */
	public static CustomerInvoice addCustomerInvoice(CustomerInvoice invoice) throws SQLException {
		String query = "INSERT INTO " + TABLE_NAME + 
				" (" + COLUMN_NAME_CUSTOMER_ID +
				", " + COLUMN_NAME_STATUS + 
				", " + COLUMN_NAME_ISSUE_DATE +
				", " + COLUMN_NAME_DUE_DATE +
				", " + COLUMN_NAME_DELIVERY_DATE +
				", " + COLUMN_NAME_TAX_RATE +
				", " + COLUMN_NAME_DISCOUNT + ") values (?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement statement = Database.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		statement.setInt(1, invoice.getActorId());
		statement.setInt(2, invoice.getStatus());
		statement.setDate(3, new java.sql.Date(invoice.getIssueDate().getTime()));
		statement.setDate(4, new java.sql.Date(invoice.getDueDate().getTime()));
		statement.setDate(5, new java.sql.Date(invoice.getDeliveryDate().getTime()));
		statement.setInt(6, invoice.getTaxRate());
		statement.setInt(7, invoice.getDiscount());

		int affectedRows = statement.executeUpdate();

		if (affectedRows == 0) {
			throw new SQLException("Insert customer invoice failed, no rows affected.");
		}

		try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
			if (generatedKeys.next()) {
				int invoiceNr = generatedKeys.getInt(1);
				invoice.setInvoiceId(invoiceNr);
				return invoice;

			} else {
				throw new SQLException("Insert supplier invoice failed, no ID obtained.");
			}
		}
	}
	
	/**
	 * This method gets all customer invoices from the database.
	 * 
	 * @return customer invoices
	 * @throws SQLException
	 */
	public static List<CustomerInvoice> getAllCustomerInvoices() throws SQLException {
		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME);
		
		ArrayList<CustomerInvoice> invoices = new ArrayList<>();

		while (results.next()) {
			CustomerInvoice invoice = getCustomerInvoiceFromResult(results);
			invoices.add(invoice);
		}
		return invoices;
	}

	/**
	 * This method gets customer invoice with the same id as the submitted invoice id as parameter.
	 * 
	 * @param invoiceId
	 * @return customer invoice
	 * @throws SQLException
	 */
	public static CustomerInvoice getCustomerInvoiceById(int invoiceId) throws SQLException {
		ResultSet result = Database.executeStatement("SELECT * FROM " + TABLE_NAME +  " WHERE " + COLUMN_NAME_INVOICE_ID + " = " + invoiceId);

		if (result.first() == true) {
			CustomerInvoice invoice = getCustomerInvoiceFromResult(result);
			return invoice;
		}

		return null;
	}
	
	/**
	 * This method returns a list of customer invoices with the same id as the submitted invoice id.
	 * 
	 * @param customerId
	 * @return customer invoices
	 * @throws SQLException
	 */
	public static List<CustomerInvoice> getCustomersInvoiceByCustomerId(int customerId) throws SQLException {
		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME +  " WHERE " + COLUMN_NAME_CUSTOMER_ID + " = " + customerId);

		ArrayList<CustomerInvoice> invoices = new ArrayList<>();

		while (results.next()) {
			CustomerInvoice invoice = getCustomerInvoiceFromResult(results);
			invoices.add(invoice);
		}
		return invoices;
	}

	/**
	 * This method returns a list of all customers with the same status as the
	 * submitted parameter.
	 * 
	 * @param status
	 * @return customer invoices
	 * @throws SQLException
	 */
	public static ArrayList<CustomerInvoice> getCustomerInvoicesByStatus(int status) throws SQLException {
		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME + " WHERE status =" + status);

		ArrayList<CustomerInvoice> invoices = new ArrayList<>();

		while (results.next()) {
			CustomerInvoice invoice = getCustomerInvoiceFromResult(results);
			invoices.add(invoice);
		}
		return invoices;
	}

	/**
	 * This method updates the customer invoice in database.
	 * 
	 * @param invoice
	 * @throws SQLException
	 */
	public static void updateCustomerInvoice(CustomerInvoice invoice) throws SQLException {
		String query = "UPDATE " + TABLE_NAME + 
				" SET " + COLUMN_NAME_CUSTOMER_ID + 
				" = ?, " + COLUMN_NAME_STATUS + 
				" = ?, " + COLUMN_NAME_ISSUE_DATE + 
				" = ?, " + COLUMN_NAME_DUE_DATE + 
				" = ?, " + COLUMN_NAME_DELIVERY_DATE + 
				" = ?, " + COLUMN_NAME_TAX_RATE + 
				" = ?, " + COLUMN_NAME_DISCOUNT + 
				" = ? " + " WHERE " + COLUMN_NAME_INVOICE_ID + " = " + invoice.getInvoiceId();

		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.setInt(1, invoice.getActorId());
		preparedStmt.setInt(2, invoice.getStatus());
		preparedStmt.setDate(3, new java.sql.Date(invoice.getIssueDate().getTime()));
		preparedStmt.setDate(4, new java.sql.Date(invoice.getDueDate().getTime()));
		preparedStmt.setDate(5, new java.sql.Date(invoice.getDeliveryDate().getTime()));
		preparedStmt.setInt(6,invoice.getTaxRate());
		preparedStmt.setInt(7, invoice.getDiscount());
		
		preparedStmt.execute();
	}
	
	/**
	 * This method updates only the status of a customer invoice.
	 * 
	 * @param invoiceNr
	 * @param status
	 * @throws SQLException
	 */
	public static void updateCustomerInvoiceStatus(int invoiceNr, int status) throws SQLException {
		String query = "UPDATE " + TABLE_NAME + " SET " + COLUMN_NAME_STATUS + 
				" = ? " + " WHERE " + COLUMN_NAME_INVOICE_ID + " = " + invoiceNr;

		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.setInt(1, status);
		preparedStmt.execute();
	}

	/**
	 * This private method formms result set of database call into a customer invoice.
	 * 
	 * @param result
	 * @return Customer invoice
	 * @throws SQLException
	 */
	private static CustomerInvoice getCustomerInvoiceFromResult(ResultSet result) throws SQLException {
		int invoiceNr = result.getInt(COLUMN_NAME_INVOICE_ID);
		int customerNr = result.getInt(COLUMN_NAME_CUSTOMER_ID);
		int status = result.getInt(COLUMN_NAME_STATUS);
		Date issueDate = new Date(result.getDate(COLUMN_NAME_ISSUE_DATE).getTime());
		Date dueDate = new Date(result.getDate(COLUMN_NAME_DUE_DATE).getTime());
		Date deliveryDate = new Date(result.getDate(COLUMN_NAME_DELIVERY_DATE).getTime());
		int taxRate = result.getInt(COLUMN_NAME_TAX_RATE);
		int discount = result.getInt(COLUMN_NAME_DISCOUNT);

		return new CustomerInvoice(invoiceNr, CompanyConstants.COMPANY_TAX_NUMBER, customerNr, status, issueDate, dueDate, deliveryDate, taxRate, discount);
	}
}
