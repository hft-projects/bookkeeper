package dbaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import businessobjects.User;

/**
 * <h1>Login Access</h1> 
 * This final class provides functions to access user information from the database.
 */
public final class LoginAccess {

	/**
	 * This method searches through the database to find a match for the submitted username and password.
	 * If the parameters match with an already exisiting user in the database the login was successful.
	 * @param username
	 * @param password
	 * @return user
	 * @throws SQLException
	 */
	public static User loginUser(String username, char[] password) throws SQLException {	
		ResultSet usersSet = Database.executeStatement("SELECT * FROM users");
		
		while(usersSet.next()) {
			
			String dbusername = usersSet.getString("username");
			char[] dbpassword = usersSet.getString("password").toCharArray();
			


			if (dbusername.equals(username)) {
				if(Arrays.equals(dbpassword, password)) {

					return new User(username);
				}
			}	
		}		
		return null;
	}
}
