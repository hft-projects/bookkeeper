package dbaccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessobjects.Address;
import businessobjects.Supplier;

/**
 * <h1>Supplier Access</h1> 
 * This final class provides functions to access suppliers from the database.
 */
public final class SupplierAccess {
	
	/**
	 * Constants for the table and column names of the database.
	 */
	private static final String TABLE_NAME = "suppliers";
	
	private static final String COLUMN_NAME_SUPPLIER_ID = "supplier_id";
	private static final String COLUMN_NAME_NAME = "name";
	private static final String COLUMN_NAME_POSTCODE = "postcode";
	private static final String COLUMN_NAME_CITY = "city";
	private static final String COLUMN_NAME_STREET = "street";
	private static final String COLUMN_NAME_COUNTRY = "country";
	private static final String COLUMN_NAME_EMAIL = "e_mail";
	private static final String COLUMN_NAME_TELEPHONE = "telephone";
	private static final String COLUMN_NAME_TAX_NUMBER = "tax_number";
	private static final String COLUMN_NAME_BANK_NAME = "bank_name";
	private static final String COLUMN_NAME_BANK_NUMBER = "bank_number";
	private static final String COLUMN_NAME_BANK_OWNER = "bank_owner";
	
	/*
	 * Private constructor.
	 */
	public SupplierAccess() {
		
	}
	
	/**
	 * This method adds the submitted supplier to database.
	 * 
	 * @param supplier
	 * @throws SQLException
	 */
	public static void addSupplier(Supplier supplier) throws SQLException {
		String query = "INSERT INTO " + TABLE_NAME + 
				" (" + COLUMN_NAME_NAME + 
				", " + COLUMN_NAME_POSTCODE + 
				", " + COLUMN_NAME_STREET + 
				", " + COLUMN_NAME_CITY + 
				", " + COLUMN_NAME_COUNTRY + 
				", " + COLUMN_NAME_EMAIL + 
				", " + COLUMN_NAME_TELEPHONE + 
				", " + COLUMN_NAME_TAX_NUMBER + 
				", " + COLUMN_NAME_BANK_NAME + 
				", " + COLUMN_NAME_BANK_NUMBER + 
				", " + COLUMN_NAME_BANK_OWNER + 
				") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.setString(1, supplier.getName());
		preparedStmt.setInt(2, supplier.getAddress().getPostcode());
		preparedStmt.setString(3, supplier.getAddress().getStreet());
		preparedStmt.setString(4, supplier.getAddress().getCity());
		preparedStmt.setString(5, supplier.getAddress().getCountry());
		preparedStmt.setString(6, supplier.getEmail());
		preparedStmt.setString(7, supplier.getTelephone());
		preparedStmt.setString(8, supplier.getTaxNumber());
		preparedStmt.setString(9, supplier.getBankName());
		preparedStmt.setString(10, supplier.getBankNumber());
		preparedStmt.setString(11, supplier.getBankOwner());
		
		preparedStmt.execute();
	}
	
	/**
	 * This method gets all suppliers from the database.
	 * 
	 * @return suppliers
	 * @throws SQLException
	 */
	public static List<Supplier> getAllSuppliers() throws SQLException {
		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME);
		ArrayList<Supplier> suppliers = new ArrayList<>();

		while (results.next()) {
			Supplier supplier = getSupplierFromResult(results);
			suppliers.add(supplier);
		}
		return suppliers;
	}
	
	/**
	 * This method gets supplier with the same id as the submitted id as parameter.
	 * 
	 * @param supplierId
	 * @return supplier
	 * @throws SQLException
	 */
	public static Supplier getSupplier(int supplierId) throws SQLException {
		ResultSet result = Database.executeStatement("SELECT * FROM " + TABLE_NAME + 
				" WHERE " + COLUMN_NAME_SUPPLIER_ID + " = " + supplierId);

		if (result.first() == true) {
			Supplier supplier = getSupplierFromResult(result);
			return supplier;
		}
		return null;
	}
	
	/**
	 * This method updates the supplier object in database.
	 * 
	 * @param supplierId
	 * @param supplier
	 * @throws SQLException
	 */
	public static void updateSupplier(int supplierId, Supplier supplier) throws SQLException {
		String query = "UPDATE " + TABLE_NAME + 
				" SET " + COLUMN_NAME_NAME + 
				" = ?, " + COLUMN_NAME_POSTCODE + 
				" = ?, " + COLUMN_NAME_STREET + 
				" = ?, " + COLUMN_NAME_CITY + 
				" = ?, " + COLUMN_NAME_COUNTRY +
				" = ?, " + COLUMN_NAME_EMAIL +
				" = ?, " + COLUMN_NAME_TELEPHONE +
				" = ?, " + COLUMN_NAME_TAX_NUMBER +
				" = ?, " + COLUMN_NAME_BANK_NAME +
				" = ?, " + COLUMN_NAME_BANK_NUMBER +
				" = ?, " + COLUMN_NAME_BANK_OWNER +
				" = ? " + " WHERE " + COLUMN_NAME_SUPPLIER_ID + " = " + supplierId;

		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.setString(1, supplier.getName());
		preparedStmt.setInt(2, supplier.getAddress().getPostcode());
		preparedStmt.setString(3, supplier.getAddress().getStreet());
		preparedStmt.setString(4, supplier.getAddress().getCity());
		preparedStmt.setString(5, supplier.getAddress().getCountry());
		preparedStmt.setString(6, supplier.getEmail());
		preparedStmt.setString(7, supplier.getTelephone());
		preparedStmt.setString(8, supplier.getTaxNumber());
		preparedStmt.setString(9, supplier.getBankName());
		preparedStmt.setString(10, supplier.getBankNumber());
		preparedStmt.setString(11, supplier.getBankOwner());

		preparedStmt.execute();
	}

	/**
	 * This private method formms result set of database call into a customer
	 * object.
	 * 
	 * @param result
	 * @return Supplier
	 * @throws SQLException
	 */
	private static Supplier getSupplierFromResult(ResultSet result) throws SQLException {
		int supplierNr = result.getInt(COLUMN_NAME_SUPPLIER_ID);
		String name = result.getString(COLUMN_NAME_NAME);
		int postcode = result.getInt(COLUMN_NAME_POSTCODE);
		String city  = result.getString(COLUMN_NAME_CITY);
		String street = result.getString(COLUMN_NAME_STREET);
		String country = result.getString(COLUMN_NAME_COUNTRY);
		String email = result.getString(COLUMN_NAME_EMAIL);
		String telephone = result.getString(COLUMN_NAME_TELEPHONE);
		String taxNumber = result.getString(COLUMN_NAME_TAX_NUMBER);
		String bankName = result.getString(COLUMN_NAME_BANK_NAME);
		String bankNumber = result.getString(COLUMN_NAME_BANK_NUMBER);
		String bankOwner = result.getString(COLUMN_NAME_BANK_OWNER);
				
		Address address = new Address(street, country, city, postcode);

		return new Supplier(supplierNr, name, address, email, telephone, taxNumber, bankName, bankNumber, bankOwner);	
	}
}
