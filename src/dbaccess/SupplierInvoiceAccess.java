package dbaccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import businessobjects.Invoice;
import businessobjects.SupplierInvoice;

/**
 * <h1>Supplier Invoice Access</h1> 
 * This final class provides functions to access supplier invoices from the database.
 */
public final class SupplierInvoiceAccess {

	/**
	 * Constants for the table and column names of the database.
	 */
	private static final String TABLE_NAME = "supplier_invoices";

	private static final String COLUMN_NAME_INVOICE_ID = "invoice_id";						
	private static final String COLUMN_NAME_EXTERNAL_INVOICE_ID = "external_invoice_id";	
	private static final String COLUMN_NAME_SUPPLIER_ID = "supplier_id";					
	private static final String COLUMN_NAME_STATUS = "status";
	private static final String COLUMN_NAME_ISSUE_DATE = "issue_date";
	private static final String COLUMN_NAME_DUE_DATE = "due_date";
	private static final String COLUMN_NAME_DELIVERY_DATE = "delivery_date";
	private static final String COLUMN_NAME_TAX_RATE = "tax_rate";							
	private static final String COLUMN_NAME_DISCOUNT = "discount";							
	
	/*
	 * Private constructor.
	 */
	private SupplierInvoiceAccess() {
		
	}

	/**
	 * This method adds the submitted supplier invoice to database.
	 * @param invoice
	 * @throws SQLException
	 */
	public static SupplierInvoice addSupplierInvoice(SupplierInvoice invoice) throws SQLException {
		String query = "INSERT INTO " + TABLE_NAME + 
				" (" + COLUMN_NAME_EXTERNAL_INVOICE_ID +
				", " + COLUMN_NAME_SUPPLIER_ID + 
				", " + COLUMN_NAME_STATUS + 
				", " + COLUMN_NAME_ISSUE_DATE +
				", " + COLUMN_NAME_DUE_DATE +
				", " + COLUMN_NAME_DELIVERY_DATE +
				", " + COLUMN_NAME_TAX_RATE +
				", " + COLUMN_NAME_DISCOUNT + ") values (?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement statement = Database.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		statement.setString(1, invoice.getExternalInvoiceId());
		statement.setInt(2, invoice.getActorId());
		statement.setInt(3, invoice.getStatus());
		statement.setDate(4, new java.sql.Date(invoice.getIssueDate().getTime()));
		statement.setDate(5, new java.sql.Date(invoice.getDueDate().getTime()));
		statement.setDate(6, new java.sql.Date(invoice.getDeliveryDate().getTime()));
		statement.setInt(7, invoice.getTaxRate());
		statement.setInt(8, invoice.getDiscount());

		int affectedRows = statement.executeUpdate();

		if (affectedRows == 0) {
			throw new SQLException("Insert supplier invoice failed, no rows affected.");
		}

		try (ResultSet generatedKeys = statement.getGeneratedKeys()) {			
			if (generatedKeys.next()) {
				int invoiceNr = generatedKeys.getInt(1);						
				invoice.setInvoiceId(invoiceNr);
				return invoice;

			} else {
				throw new SQLException("Insert supplier invoice failed, no ID obtained.");
			}
		}
	}

	/**
	 * This method gets all supplier invoices from the database.
	 * 
	 * @return supplier invoices
	 * @throws SQLException
	 */
	public static List<SupplierInvoice> getAllSupplierInvoices() throws SQLException {
		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME);
		
		ArrayList<SupplierInvoice> invoices = new ArrayList<>();

		while (results.next()) {
			SupplierInvoice invoice = getSupplierInvoiceFromResult(results);
			invoices.add(invoice);
		}
		return invoices;
	}

	/**
	 * This method gets a supplier invoice with the same id as the submitted invoice id as parameter.
	 * 
	 * @param invoiceId
	 * @return supplier invoice
	 * @throws SQLException
	 */
	public static SupplierInvoice getSupplierInvoice(int invoiceId) throws SQLException {
		ResultSet result = Database.executeStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_INVOICE_ID + " = " + invoiceId);

		if (result.first() == true) {
			SupplierInvoice invoice = getSupplierInvoiceFromResult(result);
			return invoice;
		}
		return null;
	}
	
	/**
	 * This method gets a supplier invoice with the same id as the submitted supplier id as parameter.
	 * 
	 * @param supplierId
	 * @return supplier invoice
	 * @throws SQLException
	 */
	public static SupplierInvoice getSupplierInvoiceBySupplierId(int supplierId) throws SQLException {

		ResultSet result = Database.executeStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_SUPPLIER_ID + " = " + supplierId);

		if (result.first() == true) {
			SupplierInvoice invoice = getSupplierInvoiceFromResult(result);
			return invoice;
		}
		return null;
	}
	
	/**
	 * This method returns a list of supplier invoices with the same id as the
	 * submitted supplier id as parameter.
	 * 
	 * @param supplierId
	 * @return supplier invoices
	 * @throws SQLException
	 */
	public static List<Invoice> getSupplierInvoicesBySupplierId(int supplierId) throws SQLException {
		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_SUPPLIER_ID + " = " + supplierId);

		ArrayList<Invoice> supplierInvoices = new ArrayList<>();

		while (results.next()) {
			SupplierInvoice invoice = getSupplierInvoiceFromResult(results);
			supplierInvoices.add(invoice);
		}
		return supplierInvoices;
	}

	/**
	 * This method returns a list of supplier invoices depending on the submitted status.
	 * 
	 * @param status
	 * @return supplier invoices
	 * @throws SQLException
	 */
	public static ArrayList<SupplierInvoice> getSupplierInvoicesByStatus(int status) throws SQLException {

		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME + " WHERE "+ COLUMN_NAME_STATUS + " = " + status);

		ArrayList<SupplierInvoice> invoices = new ArrayList<>();

		while (results.next()) {
			SupplierInvoice invoice = getSupplierInvoiceFromResult(results);
			invoices.add(invoice);
		}
		return invoices;
	}
	
	/**
	 * This method updates the supplier invoice in database.
	 * 
	 * @param invoice
	 * @throws SQLException
	 */
	public static void updateSupplierInvoice(SupplierInvoice invoice) throws SQLException {
		String query = "UPDATE " + TABLE_NAME + 
				" SET " + COLUMN_NAME_EXTERNAL_INVOICE_ID + 
				" = ?, " + COLUMN_NAME_SUPPLIER_ID + 
				" = ?, " + COLUMN_NAME_STATUS + 
				" = ?, " + COLUMN_NAME_ISSUE_DATE + 
				" = ?, " + COLUMN_NAME_DUE_DATE + 
				" = ?, " + COLUMN_NAME_DELIVERY_DATE + 
				" = ?, " + COLUMN_NAME_TAX_RATE + 
				" = ?, " + COLUMN_NAME_DISCOUNT + 
				" = ? " + " WHERE " + COLUMN_NAME_INVOICE_ID + " = " + invoice.getInvoiceId();

		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.setString(1, invoice.getExternalInvoiceId());
		preparedStmt.setInt(2, invoice.getActorId());
		preparedStmt.setInt(3, invoice.getStatus());
		preparedStmt.setDate(4, new java.sql.Date(invoice.getIssueDate().getTime()));
		preparedStmt.setDate(5, new java.sql.Date(invoice.getDueDate().getTime()));
		preparedStmt.setDate(6, new java.sql.Date(invoice.getDeliveryDate().getTime()));
		preparedStmt.setInt(7,invoice.getTaxRate());
		preparedStmt.setInt(8, invoice.getDiscount());
		
		preparedStmt.execute();
	}
	
	/**
	 * This method updates only the status of a supplier invoice.
	 * 
	 * @param invoiceNr
	 * @param status
	 * @throws SQLException
	 */
	public static void updateSupplierInvoiceStatus(int invoiceNr, int status) throws SQLException {
		String query = "UPDATE " + TABLE_NAME + " SET " + COLUMN_NAME_STATUS + 
				" = ? " + " WHERE " + COLUMN_NAME_INVOICE_ID + " = " + invoiceNr;

		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.setInt(1, status);
		preparedStmt.execute();
	}
	
	/**
	 * This private method formms result set of database call into a supplier invoice.
	 * 
	 * @param result
	 * @return Supplier invoice
	 * @throws SQLException
	 */
	private static SupplierInvoice getSupplierInvoiceFromResult(ResultSet result) throws SQLException {
		int invoiceNr = result.getInt(COLUMN_NAME_INVOICE_ID);
		String extInvoiceNr = result.getString(COLUMN_NAME_EXTERNAL_INVOICE_ID);
		int supplierNr = result.getInt(COLUMN_NAME_SUPPLIER_ID);
		int status = result.getInt(COLUMN_NAME_STATUS);
		Date issueDate = new Date(result.getDate(COLUMN_NAME_ISSUE_DATE).getTime());
		Date dueDate = new Date(result.getDate(COLUMN_NAME_DUE_DATE).getTime());
		Date deliveryDate = new Date(result.getDate(COLUMN_NAME_DELIVERY_DATE).getTime());
		int taxRate = result.getInt(COLUMN_NAME_TAX_RATE);
		int discount = result.getInt(COLUMN_NAME_DISCOUNT);

		return new SupplierInvoice(invoiceNr, extInvoiceNr, supplierNr, status, issueDate, dueDate, deliveryDate, taxRate, discount);
	}
}
