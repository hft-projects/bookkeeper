package dbaccess;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessobjects.Invoice;
import businessobjects.InvoicePosition;

/**
 * <h1>Invoice Positions Access</h1> 
 * This final class provides functions to access the invoice positions from the database.
 */
public final class InvoicePositionsAccess {

	/**
	 * Constants for the table and column names of the database.
	 */
	private static final String TABLE_NAME = "invoice_positions";

	private static final String COLUMN_NAME_POSITION_ID = "position_id";					
	private static final String COLUMN_NAME_PREFIXED_INVOICE_ID = "prefixed_invoice_id";			
	private static final String COLUMN_NAME_ARTICLE_NUMBER = "article_number";					
	private static final String COLUMN_NAME_ARTICLE_DESC = "article_desc";
	private static final String COLUMN_NAME_QUANTITY = "quantity";
	private static final String COLUMN_NAME_PRICE = "price";
	
	/*
	 * Private constructor.
	 */
	private InvoicePositionsAccess() {
		
	}
	
	/**
	 * This method adds the submitted invoice positions to the database.
	 * 
	 * @param invoice
	 * @param positions
	 * @throws SQLException
	 */
	public static void addPositions(Invoice invoice, List<InvoicePosition> positions) throws SQLException {
		String query = "INSERT INTO " + TABLE_NAME + 
				" (" + COLUMN_NAME_PREFIXED_INVOICE_ID +
				", " + COLUMN_NAME_ARTICLE_NUMBER + 
				", " + COLUMN_NAME_ARTICLE_DESC + 
				", " + COLUMN_NAME_QUANTITY + 
				", " + COLUMN_NAME_PRICE + ") values (?, ?, ?, ?, ?)";

		PreparedStatement preparedStmt = Database.prepareStatement(query);

		for (InvoicePosition position : positions) {
			preparedStmt.setString(1, invoice.getPrefixedInvoiceId());
			preparedStmt.setInt(2, position.getArticleNr());
			preparedStmt.setString(3, position.getArticleDesc());
			preparedStmt.setInt(4, position.getQuantity());
			preparedStmt.setBigDecimal(5, position.getPrice());
			
			preparedStmt.execute();
		}
	}
	
	/**
	 * This method gets all invoice positions from the database depending on the prefixed invoice id.
	 * 
	 * @param prefixedInvoiceId
	 * @return positions
	 * @throws SQLException
	 */
	public static List<InvoicePosition> getAllInvoicePositions(String prefixedInvoiceId) throws SQLException {
		String statement = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_PREFIXED_INVOICE_ID + " = '" + prefixedInvoiceId + "'";
		ResultSet results = Database.executeStatement(statement);

		ArrayList<InvoicePosition> positions = new ArrayList<>();

		while (results.next()) {
			InvoicePosition position = getInvoicePositionFromResult(results);
			positions.add(position);
		}
		return positions;
	}
	
	/**
	 * This method deletes all positions depending on the prefixed invoice id.
	 * This method and the addPositons method is used to update invoice positions easier.
	 * 
	 * @param prefixedInvoiceId
	 * @throws SQLException
	 */
	public static void deletePositions(String prefixedInvoiceId) throws SQLException {
		String query = "DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_PREFIXED_INVOICE_ID + " = '" + prefixedInvoiceId + "'";
		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.execute();
	}
	
	
	/**
	 * This private method formms result set of database call into a invoice position object.
	 * 
	 * @param result
	 * @return
	 * @throws SQLException
	 */
	private static InvoicePosition getInvoicePositionFromResult(ResultSet result) throws SQLException {
		int id = result.getInt(COLUMN_NAME_POSITION_ID);
		String invoiceId = result.getString(COLUMN_NAME_PREFIXED_INVOICE_ID);
		int articleNr = result.getInt(COLUMN_NAME_ARTICLE_NUMBER);
		String articleDesc = result.getString(COLUMN_NAME_ARTICLE_DESC);
		int quantity = result.getInt(COLUMN_NAME_QUANTITY);
		BigDecimal price = result.getBigDecimal(COLUMN_NAME_PRICE);
	
		return new InvoicePosition(id, invoiceId, articleNr,  articleDesc, quantity, price);
	}
}
