package dbaccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessobjects.Address;
import businessobjects.Customer;

/**
 * <h1>Customer Access</h1> 
 * This final class provides functions to access customers from the database.
 */
public final class CustomerAccess {

	/**
	 * Constants for the table and column names of the database.
	 */
	private static final String TABLE_NAME = "customers";

	private static final String COLUMN_NAME_CUSTOMER_ID = "customer_id";
	private static final String COLUMN_NAME_NAME = "name";
	private static final String COLUMN_NAME_POSTCODE = "postcode";
	private static final String COLUMN_NAME_CITY = "city";
	private static final String COLUMN_NAME_STREET = "street";
	private static final String COLUMN_NAME_COUNTRY = "country";
	private static final String COLUMN_NAME_EMAIL = "e_mail";
	private static final String COLUMN_NAME_TELEPHONE = "telephone";

	/*
	 * Private constructor.
	 */
	private CustomerAccess() {
	}

	/**
	 * This method adds the submitted customer to database.
	 * 
	 * @param customer
	 * @throws SQLException
	 */
	public static void addCustomer(Customer customer) throws SQLException {
		String query = "INSERT INTO " + TABLE_NAME 
				+ " (" + COLUMN_NAME_NAME 
				+ ", " + COLUMN_NAME_POSTCODE 
				+ ", " + COLUMN_NAME_STREET 
				+ ", " + COLUMN_NAME_CITY 
				+ ", " + COLUMN_NAME_COUNTRY 
				+ ", " + COLUMN_NAME_EMAIL
				+ ", " + COLUMN_NAME_TELEPHONE 
				+ ") values (?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.setString(1, customer.getName());
		preparedStmt.setInt(2, customer.getAddress().getPostcode());
		preparedStmt.setString(3, customer.getAddress().getStreet());
		preparedStmt.setString(4, customer.getAddress().getCity());
		preparedStmt.setString(5, customer.getAddress().getCountry());
		preparedStmt.setString(6, customer.getEmail());
		preparedStmt.setString(7, customer.getTelephone());

		preparedStmt.execute();
	}

	/**
	 * This method gets all customers from the database.
	 * 
	 * @return customers
	 * @throws SQLException
	 */
	public static List<Customer> getAllCustomers() throws SQLException {
		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME);
		ArrayList<Customer> customers = new ArrayList<>();

		while (results.next()) {
			Customer customer = getCustomerFromResult(results);
			customers.add(customer);
		}
		return customers;
	}

	/**
	 * This method gets customer with the same id as the submitted id as parameter.
	 * 
	 * @param customerId
	 * @return customer
	 * @throws SQLException
	 */
	public static Customer getCustomerByCustomerId(int customerId) throws SQLException {
		ResultSet result = Database.executeStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_CUSTOMER_ID + " = " + customerId);

		if (result.first() == true) {
			Customer customer = getCustomerFromResult(result);
			return customer;
		}
		return null;
	}

	/**
	 * This method returns a list of all customers with the same postcode as the
	 * submitted parameter.
	 * 
	 * @param postcode
	 * @return customers
	 * @throws SQLException
	 */
	public static List<Customer> getCustomersByPostcode(int postcode) throws SQLException {
		ResultSet results = Database.executeStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_POSTCODE + " = " + postcode);
		ArrayList<Customer> customers = new ArrayList<>();

		while (results.next()) {
			Customer customer = getCustomerFromResult(results);
			customers.add(customer);
		}
		return customers;
	}

	/**
	 * This method updates the customer object in database.
	 * 
	 * @param customerId
	 * @param customer
	 * @throws SQLException
	 */
	public static void updateCustomer(int customerId, Customer customer) throws SQLException {
		String query = "UPDATE " + TABLE_NAME 
				+ " SET "  + COLUMN_NAME_NAME 
				+ " = ?, " + COLUMN_NAME_POSTCODE 
				+ " = ?, " + COLUMN_NAME_STREET 
				+ " = ?, " + COLUMN_NAME_CITY 
				+ " = ?, " + COLUMN_NAME_COUNTRY 
				+ " = ?, " + COLUMN_NAME_EMAIL 
				+ " = ?, " + COLUMN_NAME_TELEPHONE 
				+ " = ? " + " WHERE " + COLUMN_NAME_CUSTOMER_ID + " = " + customerId;

		PreparedStatement preparedStmt = Database.prepareStatement(query);
		preparedStmt.setString(1, customer.getName());
		preparedStmt.setInt(2, customer.getAddress().getPostcode());
		preparedStmt.setString(3, customer.getAddress().getStreet());
		preparedStmt.setString(4, customer.getAddress().getCity());
		preparedStmt.setString(5, customer.getAddress().getCountry());
		preparedStmt.setString(6, customer.getEmail());
		preparedStmt.setString(7, customer.getTelephone());

		preparedStmt.execute();
	}

	/**
	 * This private method formms result set of database call into a customer
	 * object.
	 * 
	 * @param result
	 * @return Customer
	 * @throws SQLException
	 */
	private static Customer getCustomerFromResult(ResultSet result) throws SQLException {
		int customerNr = result.getInt(COLUMN_NAME_CUSTOMER_ID);
		String name = result.getString(COLUMN_NAME_NAME);
		int postcode = result.getInt(COLUMN_NAME_POSTCODE);
		String city = result.getString(COLUMN_NAME_CITY);
		String street = result.getString(COLUMN_NAME_STREET);
		String country = result.getString(COLUMN_NAME_COUNTRY);
		String email = result.getString(COLUMN_NAME_EMAIL);
		String telephone = result.getString(COLUMN_NAME_TELEPHONE);

		Address address = new Address(street, country, city, postcode);

		return new Customer(customerNr, name, address, email, telephone);
	}
}
