package dbaccess;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessobjects.Actor;
import businessobjects.Customer;
import businessobjects.CustomerInvoice;
import businessobjects.Invoice;
import businessobjects.Supplier;
import businessobjects.SupplierInvoice;

/**
 * <h1>Actor Access</h1> 
 * This class provides functions to access all actors,
 * meaning customers and suppliers from the database.
 */
public class ActorAccess {

	/**
	 * This method returns the actor of a specific invoice. The actor can be a
	 * customer or a supplier object.
	 * 
	 * @param invoice
	 * @return actor
	 * @throws SQLException
	 */
	public static Actor getActorFromInvoice(Invoice invoice) throws SQLException {
		Actor actor = null;
		if (invoice instanceof SupplierInvoice) {
			actor = SupplierAccess.getSupplier(invoice.getActorId());

		} else if (invoice instanceof CustomerInvoice) {
			actor = CustomerAccess.getCustomerByCustomerId(invoice.getActorId());
		}
		return actor;
	}

	/**
	 * This method returns a list of all actors from the database. First it will get
	 * all customers from the customer access class and then all suppliers from the
	 * supplier access class.
	 * 
	 * @see CustomerAccess
	 * @see SupplierAccess
	 * @param invoice
	 * @return actor
	 * @throws SQLException
	 */
	public static List<Actor> getActorsFromDatabase() throws SQLException {
		List<Actor> actorList = new ArrayList<Actor>();

		List<Customer> customers = CustomerAccess.getAllCustomers();
		List<Supplier> suppliers = SupplierAccess.getAllSuppliers();

		for (Customer c : customers) {
			actorList.add(c);
		}
		for (Supplier s : suppliers) {
			actorList.add(s);
		}

		return actorList;
	}
}
