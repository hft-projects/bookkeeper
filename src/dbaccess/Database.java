package dbaccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * <h1>Database</h1> 
 * This final class provides functions for the connection to the database.
 */
public final class Database {

	/**
	 * Constants for the access information to the database.
	 */
	private static final String URL = "jdbc:mysql://3.125.60.55:3306/";
	private static final String DBNAME = "db4";
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String USERNAME = "db4";
	private static final String PASSWORD = "!db4.pgm2?";

	private static Connection conn = null;

	/*
	 * Private constructor.
	 */
	private Database() {

	}

	/**
	 * This method connects to the database.
	 * 
	 * @return boolean
	 */
	public static boolean connect() {
		System.out.println("Start connection to " + DBNAME + "...");

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL + DBNAME, USERNAME, PASSWORD);

			System.out.println("Connected to " + DBNAME);

			return true;

		} catch (Exception e) {

			System.out.println("Connection failed");

			e.printStackTrace();
			return false;
		}
	}

	/**
	 * This method closes the connection to the database.
	 * 
	 * @return boolean
	 */
	public static boolean close() {
		if (conn == null)
			return false;

		try {
			conn.close();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * This method prepares the statement if the database is still connected.
	 * 
	 * @param query
	 * @param args
	 * @return
	 * @throws SQLException
	 */
	public static PreparedStatement prepareStatement(String query, int... args) throws SQLException {
		if (conn == null) {
			throw new RuntimeException("Not connected.");
		}

		return conn.prepareStatement(query, args);
	}

	/**
	 * This method executes the	statement if the database is still connected.
	 * 
	 * @param sqlStatement
	 * @return
	 * @throws SQLException
	 */
	public static ResultSet executeStatement(String sqlStatement) throws SQLException {
		if(conn == null) {
			throw new RuntimeException("Not connected.");
		}
		
		Statement stmt = conn.createStatement();
		ResultSet rs;

		rs = stmt.executeQuery(sqlStatement);
		
		return rs;
	}
}
