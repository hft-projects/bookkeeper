package services;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import org.jfree.data.time.Day;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimePeriodAnchor;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import businessobjects.CustomerInvoice;
import businessobjects.Invoice;
import businessobjects.Supplier;
import businessobjects.SupplierInvoice;
import dbaccess.CustomerInvoiceAccess;
import dbaccess.SupplierInvoiceAccess;

/**
 * <h1>Chart Dataset</h1> 
 * This final class provides functions to create datasets for the charts.
 */
public final class ChartDataset {

	/**
	 * This method creates the dataset for the revenue by postcode and year.
	 * 
	 * @param year
	 * @param postcode
	 * @return dataset
	 */
	public static XYDataset createYearDataset(int year, int postcode) {

		List<Invoice> customerInvoices = Analysis.getCustomerInvoicesByPostcode(postcode);

		TimeSeries s1 = new TimeSeries("Einnahmen");

		for (int i = 1; i <= 12; i++) {
			s1.add(new Month(i, year),
					PositionLogic.getTotalAmountGross(Analysis.filterInvoicesByMonth(customerInvoices, year, i)));
		}

		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(s1);
		dataset.setXPosition(TimePeriodAnchor.MIDDLE);
		return dataset;
	}

	/**
	 * This method creates the dataset for the revenue by postcode and year and
	 * month.
	 * 
	 * @param year
	 * @param month
	 * @param postcode
	 * @return dataset
	 */
	public static XYDataset createMonthDataset(int year, int month, int postcode) {

		List<Invoice> customerInvoices = Analysis.getCustomerInvoicesByPostcode(postcode);

		TimeSeries s1 = new TimeSeries("Einnahmen");

		int monthLenght = getLenghtOfMonth(month);
		for (int i = 1; i <= monthLenght; i++) {
			List<Invoice> invoices = Analysis.filterInvoicesByDay(customerInvoices, year, month, i);
			BigDecimal gross = PositionLogic.getTotalAmountGross(invoices);
			s1.add(new Day(i, month, year), gross);
		}
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(s1);
		dataset.setXPosition(TimePeriodAnchor.MIDDLE);
		return dataset;
	}

	/**
	 * This method creates the dataset for the expenses by supplier and year.
	 * 
	 * @param year
	 * @param supplier
	 * @return dataset
	 */
	public static XYDataset createYearDataset(int year, Supplier supplier) {

		try {
			List<Invoice> supplierInvoices = SupplierInvoiceAccess
					.getSupplierInvoicesBySupplierId(supplier.getSupplierId());

			TimeSeries s1 = new TimeSeries("Bestellvolumen");

			for (int i = 1; i <= 12; i++) {
				s1.add(new Month(i, year),
						PositionLogic.getTotalAmountGross(Analysis.filterInvoicesByMonth(supplierInvoices, year, i)));
			}

			TimeSeriesCollection dataset = new TimeSeriesCollection();
			dataset.addSeries(s1);
			dataset.setXPosition(TimePeriodAnchor.MIDDLE);
			return dataset;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * This method creates the dataset for the expenses by supplier and year and
	 * month.
	 * 
	 * @param year
	 * @param month
	 * @param supplier
	 * @return dataset
	 */
	public static XYDataset createMonthDataset(int year, int month, Supplier supplier) {

		try {
			List<Invoice> supplierInvoices = SupplierInvoiceAccess
					.getSupplierInvoicesBySupplierId(supplier.getSupplierId());

			TimeSeries s1 = new TimeSeries("Bestellvolumen");

			int monthLenght = getLenghtOfMonth(month);
			for (int i = 1; i <= monthLenght; i++) {
				List<Invoice> invoices = Analysis.filterInvoicesByDay(supplierInvoices, year, month, i);
				BigDecimal gross = PositionLogic.getTotalAmountGross(invoices);
				s1.add(new Day(i, month, year), gross);
			}
			TimeSeriesCollection dataset = new TimeSeriesCollection();
			dataset.addSeries(s1);
			dataset.setXPosition(TimePeriodAnchor.MIDDLE);
			return dataset;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * This method creates the dataset for the total expenses by year.
	 * 
	 * @param year
	 * @return dataset
	 */
	public static XYDataset createYearDatasetExpenses(int year) {

		try {
			List<SupplierInvoice> supplierInvoices = SupplierInvoiceAccess.getAllSupplierInvoices();

			TimeSeries s1 = new TimeSeries("Ausgaben");

			for (int i = 1; i <= 12; i++) {
				s1.add(new Month(i, year),
						PositionLogic.getTotalAmountGross(Analysis.filterInvoicesByMonth(supplierInvoices, year, i)));
			}

			TimeSeriesCollection dataset = new TimeSeriesCollection();
			dataset.addSeries(s1);
			dataset.setXPosition(TimePeriodAnchor.MIDDLE);
			return dataset;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * This method creates the dataset for the total revenue by year.
	 * 
	 * @param year
	 * @return dataset
	 */
	public static XYDataset createYearDatasetRevenue(int year) {

		try {
			List<CustomerInvoice> customerInvoices = CustomerInvoiceAccess.getAllCustomerInvoices();

			TimeSeries s1 = new TimeSeries("Einnahmen");

			for (int i = 1; i <= 12; i++) {
				s1.add(new Month(i, year),
						PositionLogic.getTotalAmountGross(Analysis.filterInvoicesByMonth(customerInvoices, year, i)));
			}

			TimeSeriesCollection dataset = new TimeSeriesCollection();
			dataset.addSeries(s1);
			dataset.setXPosition(TimePeriodAnchor.MIDDLE);
			return dataset;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * This private method returns the lenght of a submitted month.
	 * 
	 * @param month
	 * @return month length
	 */
	private static int getLenghtOfMonth(int month) {

		int result = 0;

		switch (month) {
		case 0:
			System.out.println("Der Monat ist Null");
			break;
		case 1: result = 31; break;
		case 2: result = 28; break;
		case 3: result = 31; break;
		case 4: result = 30; break;
		case 5: result = 31; break;
		case 6: result = 30; break;
		case 7: result = 31; break;
		case 8: result = 31; break;
		case 9: result = 30; break;
		case 10: result = 31; break;
		case 11: result = 30; break;
		case 12: result = 31; break;
		default: break;
		}
		return result;
	}
}
