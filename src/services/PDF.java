package services;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import bookkeeper.CompanyConstants;
import businessobjects.Actor;
import businessobjects.Invoice;
import businessobjects.InvoicePosition;
import dbaccess.ActorAccess;
import dbaccess.InvoicePositionsAccess;
import gui.ColorCodes;
import gui.utils.BookkeeperLogo;

/**
 * <h1>PDF</h1> 
 * This class provides functions to create pdf files for the company invoices.
 */
public class PDF {

	/**
	 * This method creates the pdf file for invoices.
	 * 
	 * @param invoice
	 * @param filepath
	 */
	public static void createPDFfromIvoice(Invoice invoice, String filepath) {

		float positionsTableCellPadding = 5;
		
		String FILE_NAME = filepath;
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(new File(FILE_NAME)));
			document.open();
			
			Image logoTop = Image.getInstance(BookkeeperLogo.getLogoImage().getDescription());
			logoTop.setAlignment(Element.ALIGN_LEFT);
			logoTop.scaleToFit(150, 150);

			Actor actor = null;
			try {
				actor = ActorAccess.getActorFromInvoice(invoice);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			if (actor == null) {
				return;
			}

			PdfPTable topTable = new PdfPTable(3);
			topTable.setWidthPercentage(100);

			Font invoiceTop = new Font();
			invoiceTop.setColor(new BaseColor(Color.decode(ColorCodes.MAINRED)));
			invoiceTop.setSize(25f);
			invoiceTop.setStyle(Font.BOLD);
			
			Paragraph pInvoiceTop = new Paragraph();
			pInvoiceTop.setFont(invoiceTop);
			pInvoiceTop.add("Rechnung");
			pInvoiceTop.add(Chunk.NEWLINE);
			
			Paragraph pNumbersAndDate = new Paragraph();
			pNumbersAndDate.add("Rechnungsnr: " + invoice.getPrefixedInvoiceId());
			pNumbersAndDate.add(Chunk.NEWLINE);
			pNumbersAndDate.add("Kundennr: " + invoice.getActorId());
			pNumbersAndDate.add(Chunk.NEWLINE);
			pNumbersAndDate.add(Chunk.NEWLINE);

			SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
			String issueDate = "" + format.format(invoice.getIssueDate());
			String dueDate = "" + format.format(invoice.getDueDate());
			String deliverDate = "" + format.format(invoice.getDeliveryDate());

			pNumbersAndDate.add("Datum: " + issueDate);
			pNumbersAndDate.add(Chunk.NEWLINE);
			pNumbersAndDate.add("Fälligkeitsdatum: " + dueDate);
			pNumbersAndDate.add(Chunk.NEWLINE);
			pNumbersAndDate.add("Lieferdatum: " + deliverDate);
			pNumbersAndDate.add(Chunk.NEWLINE);
			pNumbersAndDate.setAlignment(Element.ALIGN_RIGHT);

			PdfPCell logoCell = new PdfPCell(logoTop);
			logoCell.setPadding(0);
			logoCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			logoCell.setBorder(PdfPCell.NO_BORDER);
			


			Paragraph pActorDetails = new Paragraph();
			pActorDetails.setAlignment(Element.ALIGN_LEFT);
			pActorDetails.add(Chunk.NEWLINE);
			pActorDetails.add("" + actor.getName());
			pActorDetails.add(Chunk.NEWLINE);
			pActorDetails.add("" + actor.getAddress().getStreet());
			pActorDetails.add(Chunk.NEWLINE);
			pActorDetails.add("" + actor.getAddress().getPostcode());
			pActorDetails.add(" " + actor.getAddress().getCity());
			pActorDetails.add(Chunk.NEWLINE);
			pActorDetails.add("" + actor.getAddress().getCountry());
			pActorDetails.add(Chunk.NEWLINE);
			
			
			Paragraph pHighSpeedDetails = new Paragraph();
			pHighSpeedDetails.add(Chunk.NEWLINE);
			pHighSpeedDetails.add("HighSpeed GmbH");
			pHighSpeedDetails.add(Chunk.NEWLINE);
			pHighSpeedDetails.add(CompanyConstants.CONTACT_NAME);
			pHighSpeedDetails.add(Chunk.NEWLINE);
			pHighSpeedDetails.add(CompanyConstants.CONTACT_EMAIL);
			pHighSpeedDetails.add(Chunk.NEWLINE);
			pHighSpeedDetails.add(CompanyConstants.CONTACT_WEBSITE);
			pHighSpeedDetails.add(Chunk.NEWLINE);
			pHighSpeedDetails.setAlignment(Element.ALIGN_RIGHT);

			topTable.addCell(logoCell);
			topTable.addCell(getCell(new Paragraph(), PdfPCell.ALIGN_LEFT));
			topTable.addCell(getCell(pInvoiceTop, PdfPCell.ALIGN_RIGHT));
			
			topTable.addCell(getCell(pActorDetails, PdfPCell.ALIGN_LEFT));
			topTable.addCell(getCell(new Paragraph(), PdfPCell.ALIGN_LEFT));
			topTable.addCell(getCell(pNumbersAndDate, PdfPCell.ALIGN_RIGHT));
			
			topTable.addCell(getCell(new Paragraph(), PdfPCell.ALIGN_LEFT));
			topTable.addCell(getCell(new Paragraph(), PdfPCell.ALIGN_LEFT));
			topTable.addCell(getCell(pHighSpeedDetails, PdfPCell.ALIGN_RIGHT));

			document.add(topTable);

			document.add(new Paragraph(Chunk.NEWLINE));
			document.add(new Paragraph(Chunk.NEWLINE));
			document.add(new Paragraph("Unsere Produkte stellen wir Ihnen wie folgt in Rechnung."));
			document.add(new Paragraph(Chunk.NEWLINE));
			document.add(new Paragraph("Positionen:"));
			document.add(new Paragraph(Chunk.NEWLINE));

			PdfPTable positionsTable = new PdfPTable(4); // 4 columns.
			positionsTable.setWidthPercentage(100);
			
			Font tableHeaderFont = new Font();
			tableHeaderFont.setColor(new BaseColor(Color.WHITE));
			tableHeaderFont.setSize(12f);
					
			PdfPCell cell1 = new PdfPCell(new Paragraph("Artikelnr.", tableHeaderFont));
			cell1.setBackgroundColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			cell1.setPadding(positionsTableCellPadding);
			PdfPCell cell2 = new PdfPCell(new Paragraph("Beschreibung", tableHeaderFont));
			cell2.setPadding(positionsTableCellPadding);
			cell2.setBackgroundColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			PdfPCell cell3 = new PdfPCell(new Paragraph("Menge", tableHeaderFont));
			cell3.setPadding(positionsTableCellPadding);
			cell3.setBackgroundColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			PdfPCell cell4 = new PdfPCell(new Paragraph("Preis / Stk.", tableHeaderFont));
			cell4.setBackgroundColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			cell4.setPadding(positionsTableCellPadding);
			
			positionsTable.addCell(cell1);
			positionsTable.addCell(cell2);
			positionsTable.addCell(cell3);
			positionsTable.addCell(cell4);

			List<InvoicePosition> positions;
			
			try {
				positions = InvoicePositionsAccess.getAllInvoicePositions(invoice.getPrefixedInvoiceId());

				for (InvoicePosition ip : positions) {

					PdfPCell cell1f = new PdfPCell(new Paragraph("" + ip.getArticleNr()));
					cell1f.setPadding(positionsTableCellPadding);
					PdfPCell cell2f = new PdfPCell(new Paragraph("" + ip.getArticleDesc()));
					cell2f.setPadding(positionsTableCellPadding);
					PdfPCell cell3f = new PdfPCell(new Paragraph("" + ip.getQuantity()));
					cell3f.setPadding(positionsTableCellPadding);
					PdfPCell cell4f = new PdfPCell(new Paragraph(ip.getPrice() + "€"));
					cell4f.setPadding(positionsTableCellPadding);
					positionsTable.addCell(cell1f);
					positionsTable.addCell(cell2f);
					positionsTable.addCell(cell3f);
					positionsTable.addCell(cell4f);

				}

				BigDecimal nettPriceBD = getTotalPrice(positions);
				String nettoPrice = "" + nettPriceBD.setScale(2, RoundingMode.FLOOR);
				positionsTable.addCell(getBiggerCell(new Paragraph("Betrag(Netto): "), 3));
				positionsTable.addCell(new Paragraph(nettoPrice + "€"));

				BigDecimal bonusPriceBD =nettPriceBD
						.multiply(new BigDecimal(invoice.getDiscount()).divide(new BigDecimal(100)));
				String bonusPrice = "-" + bonusPriceBD.setScale(2, RoundingMode.FLOOR);
				positionsTable.addCell(getBiggerCell(new Paragraph("Bonus(" + invoice.getDiscount() + "%): "), 3));
				positionsTable.addCell(new Paragraph(bonusPrice + "€"));

				BigDecimal taxPriceBD = nettPriceBD.subtract(bonusPriceBD)
						.multiply(new BigDecimal(invoice.getTaxRate()).divide(new BigDecimal(100)));
				String taxPrice = "" + taxPriceBD.setScale(2, RoundingMode.FLOOR);
				positionsTable.addCell(getBiggerCell(new Paragraph("Steuer (" + invoice.getTaxRate() + "%): "), 3));
				positionsTable.addCell(new Paragraph(taxPrice + "€"));

				BigDecimal totalPriceBD = nettPriceBD.subtract(bonusPriceBD).add(taxPriceBD);
				String totalPrice = "" + totalPriceBD.setScale(2, RoundingMode.FLOOR);
				
				PdfPCell totalPriceCellDesc = new PdfPCell(getBiggerCell(new Paragraph("Gesamtbetrag: ", tableHeaderFont), 3));
				totalPriceCellDesc.setBackgroundColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
				positionsTable.addCell(totalPriceCellDesc);
				
				PdfPCell totalPriceCell = new PdfPCell(new Paragraph(totalPrice + "€"));
				Font priceFont =new Font(tableHeaderFont);
				priceFont.setStyle(Font.BOLD);
				totalPriceCell.setBackgroundColor(new BaseColor(Color.decode(ColorCodes.MAINRED)));
				positionsTable.addCell(totalPriceCell);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			document.add(positionsTable);

			document.add(new Paragraph(Chunk.NEWLINE));
			document.add(new Paragraph("Die Zahlung ist innerhalb von 14 Tagen fällig."));
			document.add(new Paragraph(Chunk.NEWLINE));
			
			PdfPTable bottomTable = new PdfPTable(3);
			bottomTable.setWidthPercentage(100);
			

			
			Paragraph bankDetails = new Paragraph();
			bankDetails.add(new Paragraph("Bankdaten:"));
			bankDetails.add(Chunk.NEWLINE);
			bankDetails.add("Bankinhaber: "+CompanyConstants.COMPANY_BANK_ACCOUNT_OWNER);
			bankDetails.add(Chunk.NEWLINE);
			bankDetails.add("Bankname: "+CompanyConstants.COMPANY_BANK_NAME);
			bankDetails.add(Chunk.NEWLINE);
			bankDetails.add("IBAN: "+CompanyConstants.COMPANY_BANK_NUMBER);
			bankDetails.add(Chunk.NEWLINE);
			bankDetails.add("USt. Nr.: "+CompanyConstants.COMPANY_TAX_NUMBER);
			bankDetails.add(Chunk.NEWLINE);
			
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			
			bottomTable.addCell(getCell(bankDetails, PdfPCell.ALIGN_LEFT));
			bottomTable.addCell(getCell(new Paragraph(), PdfPCell.ALIGN_BOTTOM));
			bottomTable.addCell(getCell(new Paragraph(), PdfPCell.ALIGN_BOTTOM));
			
			
			
			document.add(bottomTable);
			
			document.close();

			System.out.println("PDF "+invoice.getPrefixedInvoiceId()+".pdf created");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * This method sums up all position prices
	 * @param positions
	 * @return total amount
	 */
	private static BigDecimal getTotalPrice(List<InvoicePosition> positions) {

		BigDecimal total = new BigDecimal(0);

		for (InvoicePosition ip : positions) {

			BigDecimal currentPrice = ip.getPrice();
			total = total.add(currentPrice.multiply(new BigDecimal(ip.getQuantity())));
		}

		return total;
	}

	/**
	 * This private method creates a big sized cell for the pdf creation.
	 * 
	 * @param p
	 * @param colspan
	 * @return cell
	 */
	private static PdfPCell getBiggerCell(Paragraph p, int colspan) {
		PdfPCell cell = new PdfPCell(p);
		cell.setColspan(colspan);
		cell.setPadding(7);
		return cell;
	}
	
	/**
	 * This private method creates a cell for the pdf creation.
	 * 
	 * @param p
	 * @param alignment
	 * @return cell
	 */
	public static PdfPCell getCell(Paragraph p, int alignment) {
		PdfPCell cell = new PdfPCell(p);
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}
}
