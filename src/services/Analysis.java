package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import businessobjects.Customer;
import businessobjects.CustomerInvoice;
import businessobjects.Invoice;
import dbaccess.CustomerAccess;
import dbaccess.CustomerInvoiceAccess;

/**
 * <h1>Analysis</h1> 
 * This final class provides functions to filter invoices.
 */
public final class Analysis {

	/**
	 * This method gets all customer invoices depending on the submitted postcode.
	 * @param postcode
	 * @return customer invoices
	 */
	public static List<Invoice> getCustomerInvoicesByPostcode(int postcode) {

		List<Invoice> invoices = new ArrayList<Invoice>();

		try {
			List<Customer> customers = CustomerAccess.getCustomersByPostcode(postcode);

			for (Customer customer : customers) {
				int customerId = customer.getCustomerId();
				List<CustomerInvoice> invoice = CustomerInvoiceAccess.getCustomersInvoiceByCustomerId(customerId);
				invoices.addAll(invoice);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return invoices;

	}

	/**
	 * This method filters through a list of invoices and returns only the invoices
	 * within the time range between the start date and end date.
	 * 
	 * @param <T>
	 * @param invoices
	 * @param startDate
	 * @param endDate
	 * @return filteredInvoices
	 */
	public static <T extends Invoice> List<T> filterInvoicesByDate(List<T> invoices, Date startDate, Date endDate) {
		List<T> filteredInvoices = new ArrayList<T>();

		for (T invoice : invoices) {
			Date invoiceDate = invoice.getIssueDate();
			if (invoiceDate.toInstant().toString().contentEquals(startDate.toInstant().toString())
					|| (invoiceDate.after(startDate) && invoiceDate.before(endDate))) {
				filteredInvoices.add(invoice);
			}
		}
		return filteredInvoices;
	}
	
	/**
	 * This method filters through a list of invoices and returns the invoices that
	 * have the same issue day as the submitted parameters.
	 * 
	 * @param <T>
	 * @param invoices
	 * @param year
	 * @param month
	 * @param day
	 * @return filteredInvoices
	 */
	public static <T extends Invoice> List<T> filterInvoicesByDay(List<T> invoices, int year, int month, int day) {
		Calendar gc = new GregorianCalendar();
		gc.set(year, month-1, day, 0, 0, 0);
		gc.set(Calendar.MILLISECOND,0);
		Date dayStart = gc.getTime();
		gc.set(year, month-1, day, 23, 59, 59);
		gc.set(Calendar.MILLISECOND,999);
		Date dayEnd = gc.getTime();
				
		return filterInvoicesByDate(invoices, dayStart, dayEnd);
	}

	/**
	 * This method filters through a list of invoices and returns the invoices that
	 * have the same issue month as the submitted parameters.
	 * 	
	 * @param <T>
	 * @param invoices
	 * @param year
	 * @param month
	 * @return filteredInvoices
	 */
	public static <T extends Invoice> List<T> filterInvoicesByMonth(List<T> invoices, int year, int month) {
		Calendar gc = new GregorianCalendar();
		gc.set(Calendar.YEAR, year);
		gc.set(Calendar.MONTH, month);
		gc.set(Calendar.DAY_OF_MONTH, 1);
		Date monthStart = gc.getTime();
		gc.add(Calendar.MONTH, 1);
		gc.add(Calendar.DAY_OF_MONTH, -1);
		Date monthEnd = gc.getTime();

		return filterInvoicesByDate(invoices, monthStart, monthEnd);
	}
}
