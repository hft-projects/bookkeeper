package services;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jfree.chart.JFreeChart;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.DefaultFontMapper;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import businessobjects.Supplier;
import gui.ColorCodes;
import gui.utils.BookkeeperLogo;

/**
 * <h1>ReportPDF</h1> 
 * This class provides functions to create pdf files for the company reports.
 */
public class ReportPDF {

	/**
	 * This method creates a pdf report of the company revenue by postcode and year.
	 * 
	 * @param year
	 * @param postcode
	 * @param fileName
	 */
	public static void createRevenePLZYearReport(int year, int postcode, String fileName) {

		PdfWriter writer = null;

		JFreeChart chart = ChartCreation.createYearChartPLZ(year, ChartDataset.createYearDataset(year, postcode));

		int width = 550;
		int height = 400;

		try {

			Font h1 = new Font();
			h1.setColor(new BaseColor(Color.decode(ColorCodes.MAINRED)));
			h1.setSize(25f);
			h1.setStyle(Font.BOLD);

			Font p = new Font();
			p.setColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			p.setSize(12f);
			p.setStyle(Font.NORMAL);

			Document document = new Document();

			writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();

			PdfPTable topTable = new PdfPTable(3);
			topTable.setWidthPercentage(100);

			Image logoTop = Image.getInstance(BookkeeperLogo.getLogoImage().getDescription()); // Image.getInstance("assets\\logo.png");
			logoTop.setAlignment(Element.ALIGN_LEFT);
			logoTop.scaleToFit(150, 150);

			PdfPCell logoCell = new PdfPCell(logoTop);
			logoCell.setPadding(0);
			logoCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			logoCell.setBorder(PdfPCell.NO_BORDER);

			Paragraph pReportTitle = new Paragraph();
			pReportTitle.setFont(p);
			pReportTitle.add("Bericht");

			Paragraph pDate = new Paragraph();
			pDate.setFont(p);
			SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			pDate.add(formatter.format(new Date()));

			topTable.addCell(logoCell);
			topTable.addCell(getCell(pReportTitle, PdfPCell.ALIGN_CENTER));
			topTable.addCell(getCell(pDate, PdfPCell.ALIGN_RIGHT));

			document.add(topTable);

			Paragraph pReportSubtitle = new Paragraph();
			pReportSubtitle.setFont(h1);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add("Einnahmen nach PLZ und Zeitraum");
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			document.add(pReportSubtitle);

			Paragraph pDetails = new Paragraph();
			pDetails.setFont(p);
			pDetails.add("Angegebene Postleitzahl: " + postcode);
			pDetails.add(Chunk.NEWLINE);
			pDetails.add("Jahr: " + year);
			document.add(pDetails);

			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(width, height);
			Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
			Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width, height);

			chart.draw(graphics2d, rectangle2d);

			graphics2d.dispose();
			contentByte.addTemplate(template, 0, 0);

			document.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method creates a pdf report of the company revenue by postcode and year and month.
	 * 
	 * @param year
	 * @param month
	 * @param postcode
	 * @param fileName
	 */
	public static void createRevenePLZYearMonthReport(int year, int month, int postcode, String fileName) {

		PdfWriter writer = null;

		JFreeChart chart = ChartCreation.createYearMonthChartPLZ(year, month,
				ChartDataset.createMonthDataset(year, month, postcode));

		int width = 550;
		int height = 400;

		try {

			Font h1 = new Font();
			h1.setColor(new BaseColor(Color.decode(ColorCodes.MAINRED)));
			h1.setSize(25f);
			h1.setStyle(Font.BOLD);

			Font p = new Font();
			p.setColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			p.setSize(12f);
			p.setStyle(Font.NORMAL);

			Document document = new Document();

			writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();

			PdfPTable topTable = new PdfPTable(3);
			topTable.setWidthPercentage(100);

			Image logoTop = Image.getInstance(BookkeeperLogo.getLogoImage().getDescription()); // Image.getInstance("assets\\logo.png");
			logoTop.setAlignment(Element.ALIGN_LEFT);
			logoTop.scaleToFit(150, 150);

			PdfPCell logoCell = new PdfPCell(logoTop);
			logoCell.setPadding(0);
			logoCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			logoCell.setBorder(PdfPCell.NO_BORDER);

			Paragraph pReportTitle = new Paragraph();
			pReportTitle.setFont(p);
			pReportTitle.add("Bericht");

			Paragraph pDate = new Paragraph();
			pDate.setFont(p);
			SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			pDate.add(formatter.format(new Date()));

			topTable.addCell(logoCell);
			topTable.addCell(getCell(pReportTitle, PdfPCell.ALIGN_CENTER));
			topTable.addCell(getCell(pDate, PdfPCell.ALIGN_RIGHT));

			document.add(topTable);

			Paragraph pReportSubtitle = new Paragraph();
			pReportSubtitle.setFont(h1);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add("Einnahmen nach PLZ und Zeitraum");
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			document.add(pReportSubtitle);

			Paragraph pDetails = new Paragraph();
			pDetails.setFont(p);
			pDetails.add("Angegebene Postleitzahl: " + postcode);
			pDetails.add(Chunk.NEWLINE);
			pDetails.add("Jahr: " + year);
			pDetails.add(Chunk.NEWLINE);
			pDetails.add("Monat: " + getMonthNameFromNumber(month));
			document.add(pDetails);

			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(width, height);
			Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
			Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width, height);

			chart.draw(graphics2d, rectangle2d);

			graphics2d.dispose();
			contentByte.addTemplate(template, 0, 0);

			document.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method creates a pdf report of the company expenses by supplier and year.
	 * 
	 * @param year
	 * @param supplier
	 * @param fileName
	 */
	public static void createOrdervolumePLZYearReport(int year, Supplier supplier, String fileName) {

		PdfWriter writer = null;

		JFreeChart chart = ChartCreation.createYearChartSupplier(year, ChartDataset.createYearDataset(year, supplier));

		int width = 550;
		int height = 400;

		try {

			Font h1 = new Font();
			h1.setColor(new BaseColor(Color.decode(ColorCodes.MAINRED)));
			h1.setSize(25f);
			h1.setStyle(Font.BOLD);

			Font p = new Font();
			p.setColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			p.setSize(12f);
			p.setStyle(Font.NORMAL);

			Document document = new Document();

			writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();

			PdfPTable topTable = new PdfPTable(3);
			topTable.setWidthPercentage(100);

			Image logoTop = Image.getInstance(BookkeeperLogo.getLogoImage().getDescription()); // Image.getInstance("assets\\logo.png");
			logoTop.setAlignment(Element.ALIGN_LEFT);
			logoTop.scaleToFit(150, 150);

			PdfPCell logoCell = new PdfPCell(logoTop);
			logoCell.setPadding(0);
			logoCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			logoCell.setBorder(PdfPCell.NO_BORDER);

			Paragraph pReportTitle = new Paragraph();
			pReportTitle.setFont(p);
			pReportTitle.add("Bericht");

			Paragraph pDate = new Paragraph();
			pDate.setFont(p);
			SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			pDate.add(formatter.format(new Date()));

			topTable.addCell(logoCell);
			topTable.addCell(getCell(pReportTitle, PdfPCell.ALIGN_CENTER));
			topTable.addCell(getCell(pDate, PdfPCell.ALIGN_RIGHT));

			document.add(topTable);

			Paragraph pReportSubtitle = new Paragraph();
			pReportSubtitle.setFont(h1);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add("Bestellvolumen pro Lieferant und Zeitraum");
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			document.add(pReportSubtitle);

			Paragraph pDetails = new Paragraph();
			pDetails.setFont(p);
			pDetails.add("Lieferant: " + supplier);
			pDetails.add(Chunk.NEWLINE);
			pDetails.add("Jahr: " + year);
			document.add(pDetails);

			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(width, height);
			Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
			Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width, height);

			chart.draw(graphics2d, rectangle2d);

			graphics2d.dispose();
			contentByte.addTemplate(template, 0, 0);

			document.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method creates a pdf report of the company expenses by supplier and year and month.
	 * 
	 * @param year
	 * @param month
	 * @param supplier
	 * @param fileName
	 */
	public static void createOrdervolumePLZMonthReport(int year, int month, Supplier supplier, String fileName) {

		PdfWriter writer = null;

		JFreeChart chart = ChartCreation.createMonthChartSupplier(year, month,
				ChartDataset.createMonthDataset(year, month, supplier));

		int width = 550;
		int height = 400;

		try {

			Font h1 = new Font();
			h1.setColor(new BaseColor(Color.decode(ColorCodes.MAINRED)));
			h1.setSize(25f);
			h1.setStyle(Font.BOLD);

			Font p = new Font();
			p.setColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			p.setSize(12f);
			p.setStyle(Font.NORMAL);

			Document document = new Document();

			writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();

			PdfPTable topTable = new PdfPTable(3);
			topTable.setWidthPercentage(100);

			Image logoTop = Image.getInstance(BookkeeperLogo.getLogoImage().getDescription()); 
			logoTop.setAlignment(Element.ALIGN_LEFT);
			logoTop.scaleToFit(150, 150);

			PdfPCell logoCell = new PdfPCell(logoTop);
			logoCell.setPadding(0);
			logoCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			logoCell.setBorder(PdfPCell.NO_BORDER);

			Paragraph pReportTitle = new Paragraph();
			pReportTitle.setFont(p);
			pReportTitle.add("Bericht");

			Paragraph pDate = new Paragraph();
			pDate.setFont(p);
			SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			pDate.add(formatter.format(new Date()));

			topTable.addCell(logoCell);
			topTable.addCell(getCell(pReportTitle, PdfPCell.ALIGN_CENTER));
			topTable.addCell(getCell(pDate, PdfPCell.ALIGN_RIGHT));

			document.add(topTable);

			Paragraph pReportSubtitle = new Paragraph();
			pReportSubtitle.setFont(h1);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add("Bestellvolumen pro Lieferant und Zeitraum");
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			document.add(pReportSubtitle);

			Paragraph pDetails = new Paragraph();
			pDetails.setFont(p);
			pDetails.add("Lieferant: " + supplier);
			pDetails.add(Chunk.NEWLINE);
			pDetails.add("Jahr: " + year);
			pDetails.add(Chunk.NEWLINE);
			pDetails.add("Monat: " + getMonthNameFromNumber(month));
			document.add(pDetails);

			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(width, height);
			Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
			Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width, height);

			chart.draw(graphics2d, rectangle2d);

			graphics2d.dispose();
			contentByte.addTemplate(template, 0, 0);

			document.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method creates a pdf finance report of the company expenses and revenue by year.
	 * 
	 * @param year
	 * @param fileName
	 */
	public static void createFinanceReport(int year, String fileName) {

		PdfWriter writer = null;

		JFreeChart chartExpenses = ChartCreation.createYearChartExpenses(year,
				ChartDataset.createYearDatasetExpenses(year));
		JFreeChart chartRevenue = ChartCreation.createYearChartRevenue(year,
				ChartDataset.createYearDatasetRevenue(year));

		int width = 550;
		int height = 250;

		try {

			Font h1 = new Font();
			h1.setColor(new BaseColor(Color.decode(ColorCodes.MAINRED)));
			h1.setSize(25f);
			h1.setStyle(Font.BOLD);

			Font h2 = new Font();
			h2.setColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			h2.setSize(22f);
			h2.setStyle(Font.BOLD);

			Font p = new Font();
			p.setColor(new BaseColor(Color.decode(ColorCodes.MAINBG)));
			p.setSize(12f);
			p.setStyle(Font.NORMAL);

			Document document = new Document();

			writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();

			PdfPTable topTable = new PdfPTable(3);
			topTable.setWidthPercentage(100);

			Image logoTop = Image.getInstance(BookkeeperLogo.getLogoImage().getDescription()); // Image.getInstance("assets\\logo.png");
			logoTop.setAlignment(Element.ALIGN_LEFT);
			logoTop.scaleToFit(150, 150);

			PdfPCell logoCell = new PdfPCell(logoTop);
			logoCell.setPadding(0);
			logoCell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			logoCell.setBorder(PdfPCell.NO_BORDER);

			Paragraph pReportTitle = new Paragraph();
			pReportTitle.setFont(p);
			pReportTitle.add("Bericht");

			Paragraph pDate = new Paragraph();
			pDate.setFont(p);
			SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			pDate.add(formatter.format(new Date()));

			topTable.addCell(logoCell);
			topTable.addCell(getCell(pReportTitle, PdfPCell.ALIGN_CENTER));
			topTable.addCell(getCell(pDate, PdfPCell.ALIGN_RIGHT));

			document.add(topTable);

			Paragraph pReportSubtitle = new Paragraph();
			pReportSubtitle.setFont(h1);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add("Jahesabschluss " + year);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			pReportSubtitle.add(Chunk.NEWLINE);
			document.add(pReportSubtitle);

			Paragraph pRevenueHeading = new Paragraph();
			pRevenueHeading.setFont(p);
			pRevenueHeading.add(Chunk.NEWLINE);
			
			BigDecimal totalSales = PositionLogic.getTotalSalesGross();
			BigDecimal totalExpenses = PositionLogic.getTotalExpenses();
			BigDecimal totalSaldo = totalSales.subtract(totalExpenses);
			
			
			pRevenueHeading.add("Einnahmen: " + totalSales.setScale(2, RoundingMode.FLOOR) + "€");
			pRevenueHeading.add(Chunk.NEWLINE);
			pRevenueHeading.add("Ausgaben: " + totalExpenses.setScale(2, RoundingMode.FLOOR) + "€");
			pRevenueHeading.add(Chunk.NEWLINE);
			pRevenueHeading.add("Gewinn / Verlust: " + totalSaldo.setScale(2, RoundingMode.FLOOR) + "€");
			document.add(pRevenueHeading);
			
		

			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(width, height);
			Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
			Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width, height);

			chartRevenue.draw(graphics2d, rectangle2d);

			graphics2d.dispose();
			contentByte.addTemplate(template, 0, 0);

		

			PdfContentByte contentByte2 = writer.getDirectContent();
			PdfTemplate template2 = contentByte2.createTemplate(width, height);
			Graphics2D graphics2d2 = template2.createGraphics(width, height, new DefaultFontMapper());
			Rectangle2D rectangle2d2 = new Rectangle2D.Double(0, 0, width, height);

			chartExpenses.draw(graphics2d2, rectangle2d2);

			graphics2d2.dispose();
			contentByte2.addTemplate(template2, 0, height+50);

			document.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * This private method creates a cell for the pdf creation.
	 * 
	 * @param p
	 * @param alignment
	 * @return
	 */
	public static PdfPCell getCell(Paragraph p, int alignment) {
		PdfPCell cell = new PdfPCell(p);
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	/**
	 * This method returns the name of the month based on its number
	 * 
	 * @param month
	 * @return name of month
	 */
	public static String getMonthNameFromNumber(int month) {
		switch (month) {
		case 1: return "Januar";
		case 2: return "Februar";
		case 3: return "März";
		case 4:	return "April";
		case 5: return "Mai";
		case 6: return "Juni";
		case 7: return "Juli";
		case 8:	return "August";
		case 9: return "September";
		case 10: return "Oktober";
		case 11: return "November";
		case 12: return "Dezember";
		}
		return "Monat nicht gefunden";
	}
}
