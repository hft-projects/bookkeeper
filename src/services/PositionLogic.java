package services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import businessobjects.CustomerInvoice;
import businessobjects.Invoice;
import businessobjects.InvoicePosition;
import businessobjects.SupplierInvoice;
import dbaccess.CustomerInvoiceAccess;
import dbaccess.InvoicePositionsAccess;
import dbaccess.SupplierInvoiceAccess;

/**
 * <h1>Position Logic</h1> 
 * This class provides functions needed to calculate the amount of invoices.
 */
public class PositionLogic {
	
	/**
	 * This method calculates the total gross amount of all paid customer invoices.
	 * The tax rate is added and the discount of the invoice is subtracted.
	 * 
	 * @return total sales gross
	 */
	public static BigDecimal getTotalSalesGross() {
		BigDecimal total = new BigDecimal(0);
		
		try {
			List<CustomerInvoice> customerInvoiceList = CustomerInvoiceAccess.getAllCustomerInvoices();
			List<CustomerInvoice> filteredInvoices = filterInvoicesByStatus(Invoice.STATUS_PAID, customerInvoiceList);

			// Tax can be different for each invoice
			for (CustomerInvoice invoice : filteredInvoices) {
				BigDecimal net = getPositionsTotalPrice(invoice.getPrefixedInvoiceId());
				BigDecimal discount = getDiscountAmount(net, invoice.getDiscount());
				BigDecimal discounted = net.subtract(discount);
				BigDecimal tax = getTaxAmount(discounted, invoice.getTaxRate());
				BigDecimal gross = discounted.add(tax);
				total = total.add(gross);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return total;
	}
	
	/**
	 * This method calculates the total net amount of all paid customer invoices.
	 * The discount of the invoice is subtracted.
	 * 
	 * @return total sales net
	 */
	public static BigDecimal getTotalSalesNet() {
		BigDecimal total = new BigDecimal(0);

		try {
			List<CustomerInvoice> customerInvoiceList = CustomerInvoiceAccess.getAllCustomerInvoices();
			List<CustomerInvoice> filteredInvoices = filterInvoicesByStatus(Invoice.STATUS_PAID, customerInvoiceList);

			for (CustomerInvoice invoice : filteredInvoices) {
				BigDecimal net = getPositionsTotalPrice(invoice.getPrefixedInvoiceId());
				BigDecimal discount = getDiscountAmount(net, invoice.getDiscount());
				BigDecimal discounted = net.subtract(discount);
				total = total.add(discounted);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return total;
	}
	
	/**
	 * This method calculates the total gross amount of all paid supplier invoices.
	 * The tax rate is added and the discount of the invoice is subtracted.
	 * 
	 * @return total expenses gross
	 */
	public static BigDecimal getTotalExpenses() {
		BigDecimal totalExpenses = new BigDecimal(0);

		try {
			List<SupplierInvoice> supplierInvoiceList = SupplierInvoiceAccess.getAllSupplierInvoices();
			List<SupplierInvoice> filteredInvoices = filterInvoicesByStatus(Invoice.STATUS_PAID, supplierInvoiceList);

			for (Invoice invoice : filteredInvoices) {
				BigDecimal net = getPositionsTotalPrice(invoice.getPrefixedInvoiceId());
				BigDecimal discount = getDiscountAmount(net, invoice.getDiscount());
				BigDecimal discounted = net.subtract(discount);
				BigDecimal tax = getTaxAmount(discounted, invoice.getTaxRate());
				BigDecimal gross = discounted.add(tax);
				totalExpenses = totalExpenses.add(gross);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return totalExpenses;
	}
	
	/**
	 * This method calculates the total gross profit of the company.
	 * 
	 * @return total profit
	 */
	public static BigDecimal getProfit() {
		BigDecimal exp = getTotalExpenses();
		BigDecimal sal = getTotalSalesGross();
		BigDecimal result = sal.subtract(exp);
		return result;
	}
	
	/**
	 * This method calculates the total gross amount of all open, in process and overdue customer invoices.
	 * The tax rate is added and the discount of the invoice is subtracted.
	 * 
	 * @return total revenue open
	 */
	public static BigDecimal getOpenRevenue() {
		BigDecimal totalRevenue = new BigDecimal(0);

		try {
			List<CustomerInvoice> customerInvoiceList = CustomerInvoiceAccess.getAllCustomerInvoices();

			for (CustomerInvoice invoice : customerInvoiceList) {
				if (invoice.getStatus() == Invoice.STATUS_OPEN || invoice.getStatus() == Invoice.STATUS_IN_PROCESS
						|| invoice.getStatus() == Invoice.STATUS_OVERDUE) {
					BigDecimal net = getPositionsTotalPrice(invoice.getPrefixedInvoiceId());
					BigDecimal discount = getDiscountAmount(net, invoice.getDiscount());
					BigDecimal discounted = net.subtract(discount);
					BigDecimal tax = getTaxAmount(discounted, invoice.getTaxRate());
					BigDecimal gross = discounted.add(tax);
					totalRevenue = totalRevenue.add(gross);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return totalRevenue; 
	}
	
	/**
	 * This method calculates the total gross amount of all open, in process and overdue supplier invoices.
	 * The tax rate is added and the discount of the invoice is subtracted.
	 * 
	 * @return total expenses open
	 */
	public static BigDecimal getOpenExpenses() {
		BigDecimal totalExpenses = new BigDecimal(0);

		try {
			List<SupplierInvoice> supplierInvoiceList = SupplierInvoiceAccess.getAllSupplierInvoices();

			for (SupplierInvoice invoice : supplierInvoiceList) {
				if (invoice.getStatus() == Invoice.STATUS_OPEN || invoice.getStatus() == Invoice.STATUS_IN_PROCESS
						|| invoice.getStatus() == Invoice.STATUS_OVERDUE) {
					BigDecimal net = getPositionsTotalPrice(invoice.getPrefixedInvoiceId());
					BigDecimal discount = getDiscountAmount(net, invoice.getDiscount());
					BigDecimal discounted = net.subtract(discount);
					BigDecimal tax = getTaxAmount(discounted, invoice.getTaxRate());
					BigDecimal gross = discounted.add(tax);
					totalExpenses = totalExpenses.add(gross);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return totalExpenses; 
	}
	
	/**
	 * This method calculates the total gross amountof all positions of one specific paid invoice.
	 * 
	 * @param <T>
	 * @param invoices
	 * @return total gross amount
	 */
	public static <T extends Invoice> BigDecimal getTotalAmountGross(List<T> invoices) {
		
		BigDecimal total = new BigDecimal(0);
		
		List<T> filteredInvoices = filterInvoicesByStatus(Invoice.STATUS_PAID, invoices);
		
		for (Invoice invoice : filteredInvoices) {
			if (invoice.getStatus() != 4) {
				BigDecimal net = getPositionsTotalPrice(invoice.getPrefixedInvoiceId());
				BigDecimal discount = getDiscountAmount(net, invoice.getDiscount());
				BigDecimal discounted = net.subtract(discount);
				BigDecimal tax = getTaxAmount(discounted, invoice.getTaxRate());
				BigDecimal gross = discounted.add(tax);
				total = total.add(gross);
			}
		}
		return total;
	}
	
	/**
	 * This method calculates the total net amount of all positions of one specific invoice.
	 * 
	 * @param prefixedInvoiceId
	 * @return total net amount
	 */
	public static BigDecimal getPositionsTotalPrice(String prefixedInvoiceId) {
		BigDecimal totalAmount = new BigDecimal(0);
		
		try {
			List<InvoicePosition> list = InvoicePositionsAccess.getAllInvoicePositions(prefixedInvoiceId);

			for (InvoicePosition position : list) {
				int quantity = position.getQuantity();
				BigDecimal price = position.getPrice();
				BigDecimal amount = price.multiply(new BigDecimal(quantity));
				totalAmount = totalAmount.add(amount);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return totalAmount;
	}

	/**
	 * This method calculates the tax amount which has to be added to the net amount of invoices.
	 * 
	 * @param totalAmount
	 * @param taxRate
	 * @return tax amount
	 */
	public static BigDecimal getTaxAmount(BigDecimal totalAmount, int taxRate) {
		if (taxRate == 19) {
			return totalAmount.multiply(new BigDecimal(0.19));

		} else if (taxRate == 7) {
			return totalAmount.multiply(new BigDecimal(0.07));

		}
		
		throw new IllegalArgumentException("Tax rate of " + taxRate + " not supported.");
	}
	
	/**
	 * This method calculates the discount amount which has to be subtracted from the net amount of invoices.
	 * 
	 * @param totalAmount
	 * @param discount
	 * @return discount amount
	 */
	public static BigDecimal getDiscountAmount(BigDecimal totalAmount, int discount) {
		return totalAmount.multiply(new BigDecimal(discount / 100.0));
	}
	
	/**
	 * This private method filters through invoices by status.
	 * 
	 * @param <T>
	 * @param status
	 * @param invoices
	 * @return filteredInvoices
	 */
	private static <T extends Invoice> List<T> filterInvoicesByStatus(int status, List<T> invoices){
		
		List<T> filteredInvoices = new ArrayList<>();
		
		for(T invoice : invoices) {
			if(invoice.getStatus() == status) {
				filteredInvoices.add(invoice);
			}
		}
		return filteredInvoices;
	}
}

