package services;

import businessobjects.Invoice;

/**
 * <h1>Status Helper</h1> 
 * This class provides functions to form a string status to the actual contant variable and vice versa.
 */
public class StatusHelper {

	/**
	 * This method returns the constant variable depending on the submitted string status.
	 * @param status
	 * @return status
	 */
	public static int getStatusFromString(String status) {
		switch (status) {

		case "In Bearbeitung":
			return Invoice.STATUS_IN_PROCESS;
		case "Bezahlt":
			return Invoice.STATUS_PAID;
		case "Storniert":
			return Invoice.STATUS_CANCELED;
		case "Überfällig":
			return Invoice.STATUS_OVERDUE;
		default:
			return Invoice.STATUS_OPEN;

		}
	}

	/**
	 * This method returns a string status depending on the submitted constant variable.
	 * @param status
	 * @return status
	 */
	public static String getStringFromStatus(int status) {
		switch (status) {

		case Invoice.STATUS_IN_PROCESS:
			return "In Bearbeitung";
		case Invoice.STATUS_PAID:
			return "Bezahlt";
		case Invoice.STATUS_CANCELED:
			return "Storniert";
		case Invoice.STATUS_OVERDUE:
			return "Überfällig";
		default:
			return "Offen";
		}
	}
}
