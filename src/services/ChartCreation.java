package services;

import java.awt.Color;
import java.text.SimpleDateFormat;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Month;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleInsets;

import gui.ColorCodes;

/**
 * <h1>Chart Creation</h1> 
 * This final class provides functions to create charts.
 */
public final class ChartCreation {

	/**
	 * This method creates a year chart for the revenue by postcode and year.
	 * 
	 * @param year
	 * @param dataset
	 * @return chart
	 */
	public static JFreeChart createYearChartPLZ(int year, XYDataset dataset) {
		DateAxis xAxis = new DateAxis("Zeitraum");
		Month jan = new Month(1, year);
		Month dec = new Month(12, year);

		xAxis.setRange(jan.getFirstMillisecond(), dec.getLastMillisecond());
		xAxis.setDateFormatOverride(new SimpleDateFormat("MMM"));
		xAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setUseFillPaint(true);
		renderer.setBaseFillPaint(Color.white);

		NumberAxis yAxis = new NumberAxis("Einnahmen in Euro");
		yAxis.setAutoRange(true);

		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);

		DateAxis hiddenAxis = new DateAxis();
		hiddenAxis.setVisible(false);
		plot.setDomainAxis(1, hiddenAxis);
		plot.setDataset(1, dataset);
		plot.mapDatasetToDomainAxis(1, 1);

		JFreeChart chart = new JFreeChart("Einnahmen nach PLZ", plot);
		chart.setBackgroundPaint(Color.white);

		plot.setBackgroundPaint(Color.decode(ColorCodes.MAINBG));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

		return chart;
	}

	/**
	 * This method creates a month chart for the revenue by postcode and year and month.
	 * 
	 * @param year
	 * @param month
	 * @param dataset
	 * @return chart
	 */
	public static JFreeChart createYearMonthChartPLZ(int year, int month, XYDataset dataset) {
		DateAxis xAxis = new DateAxis("Zeitraum");
		Month monthM = new Month(month, year);

		xAxis.setRange(monthM.getFirstMillisecond(), monthM.getLastMillisecond());
		xAxis.setDateFormatOverride(new SimpleDateFormat("MMM"));
		xAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setUseFillPaint(true);
		renderer.setBaseFillPaint(Color.white);

		NumberAxis yAxis = new NumberAxis("Einnahmen in Euro");
		yAxis.setAutoRange(true);

		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);

		DateAxis hiddenAxis = new DateAxis();
		hiddenAxis.setVisible(false);
		plot.setDomainAxis(1, hiddenAxis);
		plot.setDataset(1, dataset);
		plot.mapDatasetToDomainAxis(1, 1);

		JFreeChart chart = new JFreeChart("Einnahmen nach PLZ", plot);
		chart.setBackgroundPaint(Color.white);

		plot.setBackgroundPaint(Color.decode(ColorCodes.MAINBG));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("dd"));

		return chart;

	}

	/**
	 * This method creates a year chart for the expenses by supplier and year.
	 * @param year
	 * @param dataset
	 * @return chart
	 */
	public static JFreeChart createYearChartSupplier(int year, XYDataset dataset) {
		DateAxis xAxis = new DateAxis("Zeitraum");
		Month jan = new Month(1, year);
		Month dec = new Month(12, year);

		xAxis.setRange(jan.getFirstMillisecond(), dec.getLastMillisecond());
		xAxis.setDateFormatOverride(new SimpleDateFormat("MMM"));
		xAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setUseFillPaint(true);
		renderer.setBaseFillPaint(Color.white);

		NumberAxis yAxis = new NumberAxis("Ausgaben in Euro");
		yAxis.setAutoRange(true);

		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);

		DateAxis hiddenAxis = new DateAxis();
		hiddenAxis.setVisible(false);
		plot.setDomainAxis(1, hiddenAxis);
		plot.setDataset(1, dataset);
		plot.mapDatasetToDomainAxis(1, 1);

		JFreeChart chart = new JFreeChart("Ausgaben nach Lieferant", plot);
		chart.setBackgroundPaint(Color.white);

		plot.setBackgroundPaint(Color.decode(ColorCodes.MAINBG));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

		return chart;
	}
	
	/**
	 * This method creates a year chart for the expenses by supplier and year and month.
	 * 
	 * @param year
	 * @param month
	 * @param dataset
	 * @return chart
	 */
	public static JFreeChart createMonthChartSupplier(int year, int month, XYDataset dataset) {
		DateAxis xAxis = new DateAxis("Zeitraum");
		Month monthM = new Month(month, year);

		xAxis.setRange(monthM.getFirstMillisecond(), monthM.getLastMillisecond());
		xAxis.setDateFormatOverride(new SimpleDateFormat("MMM"));
		xAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setUseFillPaint(true);
		renderer.setBaseFillPaint(Color.white);

		NumberAxis yAxis = new NumberAxis("Ausgaben in Euro");
		yAxis.setAutoRange(true);

		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);

		DateAxis hiddenAxis = new DateAxis();
		hiddenAxis.setVisible(false);
		plot.setDomainAxis(1, hiddenAxis);
		plot.setDataset(1, dataset);
		plot.mapDatasetToDomainAxis(1, 1);

		JFreeChart chart = new JFreeChart("Ausgaben nach Lieferant", plot);
		chart.setBackgroundPaint(Color.white);

		plot.setBackgroundPaint(Color.decode(ColorCodes.MAINBG));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("dd"));

		return chart;

	}
	
	/**
	 * This method creates a chart for the total company revenue of a specific year.
	 * 
	 * @param year
	 * @param dataset
	 * @return chart
	 */
	public static JFreeChart createYearChartRevenue(int year, XYDataset dataset) {
		DateAxis xAxis = new DateAxis("Zeitraum");
		Month jan = new Month(1, year);
		Month dec = new Month(12, year);

		xAxis.setRange(jan.getFirstMillisecond(), dec.getLastMillisecond());
		xAxis.setDateFormatOverride(new SimpleDateFormat("MMM"));
		xAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setUseFillPaint(true);
		renderer.setBaseFillPaint(Color.white);

		NumberAxis yAxis = new NumberAxis("Einnahmen in Euro");
		yAxis.setAutoRange(true);

		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);

		DateAxis hiddenAxis = new DateAxis();
		hiddenAxis.setVisible(false);
		plot.setDomainAxis(1, hiddenAxis);
		plot.setDataset(1, dataset);
		plot.mapDatasetToDomainAxis(1, 1);

		JFreeChart chart = new JFreeChart("Gesamteinnahmen", plot);
		chart.setBackgroundPaint(Color.white);

		plot.setBackgroundPaint(Color.decode(ColorCodes.MAINBG));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

		return chart;
	}
	
	/**
	 * This method creates a chart for the total company expenses of a specific year.
	 * 
	 * @param year
	 * @param dataset
	 * @return chart
	 */
	public static JFreeChart createYearChartExpenses(int year, XYDataset dataset) {
		DateAxis xAxis = new DateAxis("Zeitraum");
		Month jan = new Month(1, year);
		Month dec = new Month(12, year);

		xAxis.setRange(jan.getFirstMillisecond(), dec.getLastMillisecond());
		xAxis.setDateFormatOverride(new SimpleDateFormat("MMM"));
		xAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setUseFillPaint(true);
		renderer.setBaseFillPaint(Color.white);

		NumberAxis yAxis = new NumberAxis("Ausgaben in Euro");
		yAxis.setAutoRange(true);

		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);

		DateAxis hiddenAxis = new DateAxis();
		hiddenAxis.setVisible(false);
		plot.setDomainAxis(1, hiddenAxis);
		plot.setDataset(1, dataset);
		plot.mapDatasetToDomainAxis(1, 1);

		JFreeChart chart = new JFreeChart("Gesamtausgaben", plot);
		chart.setBackgroundPaint(Color.white);

		plot.setBackgroundPaint(Color.decode(ColorCodes.MAINBG));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

		return chart;
	}
}
