package bookkeeper;

import dbaccess.Database;
import gui.screens.LoginScreen;
import gui.screens.SplashScreen;

/**
 * <h1>Main</h1> 
 * Launches the <b>BookKeeper</b> application.
 */
public class Main {

	/**
	 * Launches splash screen and starts connection to database. After successful
	 * connection application starts with login screen.
	 */
	public static void main(String[] args) {

		SplashScreen ss = new SplashScreen();

		try {
			boolean connected = Database.connect();
			if (connected) {
				Thread.sleep(1500L);
				LoginScreen.start();

				ss.close();
			} else {
				ss.showError("Es ist ein Fehler mit der Verbindung aufgetreten!");
			}

		} catch (Exception e) {
			e.printStackTrace();
			ss.showError(e.getMessage());
		}

	}
}
