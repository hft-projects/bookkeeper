package bookkeeper;

/**
 * <h1>Company Constants</h1> 
 * Specifies company values and information which are unlikely to change
 */
public class CompanyConstants {

	// CEO
	public static final String DIRECTOR = "Thomas Köhler";
	public static final String DEPUTY_MANAGEMENT = "Anna Schmidt";

	// Contact
	public static final String CONTACT_NAME = "Lisa Bucher";
	public static final String CONTACT_EMAIL = "bookkeeper@highspeed.com";
	public static final String CONTACT_WEBSITE = "www.highspeed.com";

	// Bank
	public static final String COMPANY_BANK_ACCOUNT_OWNER = "Thomas Köhler";
	public static final String COMPANY_BANK_NAME = "Landesbank Baden-Württemberg Sparkasse";
	public static final String COMPANY_BANK_NUMBER = "DE15 4569 1233 4564";

	// Tax
	public static final String COMPANY_TAX_OFFICE = "Stuttgart";
	public static final String COMPANY_TAX_NUMBER = "DE449291989";

}
