package businessobjects;

import java.util.Date;

/**
 * <h1>Invoice</h1> 
 * This abstract class provides common values and functions for invoices.
 */
public abstract class Invoice {

	/**
	 * Constants for the status of an invoice.
	 */
	public static final int STATUS_OPEN = 1;
	public static final int STATUS_IN_PROCESS = 2;
	public static final int STATUS_PAID = 3;
	public static final int STATUS_CANCELED = 4;
	public static final int STATUS_OVERDUE = 5;

	private int invoiceId;
	private String taxNumber;
	private int actorId;
	private int status;
	private Date issueDate;
	private Date dueDate;
	private Date deliveryDate;
	private int taxRate;
	private int discount;

	/**
	 * Construtor for invoice objects.
	 * 
	 * @param invoiceId
	 * @param taxNumber
	 * @param actorId
	 * @param status
	 * @param issueDate
	 * @param dueDate
	 * @param deliveryDate
	 * @param taxRate
	 * @param discount
	 */
	public Invoice(int invoiceId, String taxNumber, int actorId, int status, Date issueDate, Date dueDate, Date deliveryDate, int taxRate, int discount) {
		this.invoiceId = invoiceId;
		this.taxNumber = taxNumber;
		this.actorId = actorId;
		this.status = status;
		this.issueDate = issueDate;
		this.dueDate = dueDate;
		this.deliveryDate = deliveryDate;
		this.taxRate = taxRate;
		this.discount = discount;
	}

	/**
	 * Returns unique and autoincremental invoice id.
	 * 
	 * @return invoiceId
	 */
	public int getInvoiceId() {
		return invoiceId;
	}
	
	/**
	 * Abstract method to get the prefixed invoice id.
	 * This has to be implemented with class inheritance.
	 * 
	 * @return nothing
	 */
	public abstract String getPrefixedInvoiceId();
	
	/**
	 * Sets new invoice id.
	 * 
	 * @return invoiceId
	 */
	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	/**
	 * Sets new invoice status.
	 * 
	 * @return status
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	/**
	 * Returns tax number.
	 * 
	 * @return taxNumber
	 */
	public String getTaxNumber() {
		return taxNumber;
	}

	/**
	 * Returns actor id.
	 * 
	 * @return actorId
	 */
	public int getActorId() {
		return actorId;
	}

	/**
	 * Returns invoice status.
	 * 
	 * @return status
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * Returns invoice issue date.
	 * 
	 * @return issueDate
	 */
	public Date getIssueDate() {
		return issueDate;
	}

	/**
	 * Returns invoice due date.
	 * 
	 * @return dueDate
	 */
	public Date getDueDate() {
		return dueDate;
	}

	/**
	 * Returns invoice delivery date.
	 * 
	 * @return deliveryDate
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * Returns tax rate.
	 * 
	 * @return taxRate
	 */
	public int getTaxRate() {
		return taxRate;
	}
	
	/**
	 * Returns discount.
	 * 
	 * @return dicount
	 */
	public int getDiscount() {
		return discount;
	}

	/**
	 * This method overrides the toString method
	 * and returns a string with all values of the invoice object.
	 * 
	 * @return String of invoice values
	 */
	@Override
	public String toString() {
		return "Invoice [invoiceId=" + invoiceId + ", taxNumber=" + taxNumber + ", actorId=" + actorId + ", status="
				+ status + ", issueDate=" + issueDate + ", dueDate=" + dueDate + ", deliveryDate=" + deliveryDate
				+ ", taxRate=" + taxRate + ", discount=" + discount + "]";
	}
}
