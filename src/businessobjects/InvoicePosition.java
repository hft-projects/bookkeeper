package businessobjects;

import java.math.BigDecimal;

/**
 * <h1>Invoice Position</h1> 
 * This class provides values and functions for invoice positions.
 */
public class InvoicePosition {

	private int positionId;
	private String prefixedInvoiceId;
	private int articleNumber;
	private String articleDesc;
	private int quantity;
	private BigDecimal price;

	/**
	 * Constructor for invoice position objects without an id.
	 * 
	 * @param prefixedInvoiceId
	 * @param articleNr
	 * @param articleDesc
	 * @param quantity
	 * @param price
	 */
	public InvoicePosition(String prefixedInvoiceId, int articleNr, String articleDesc, int quantity, BigDecimal price) {
		this.positionId = -1;
		this.prefixedInvoiceId = prefixedInvoiceId;
		this.articleNumber = articleNr;
		this.articleDesc = articleDesc;
		this.quantity = quantity;
		this.price = price;
	}

	/**
	 * Constructor for invoice position objects with an existing positions id.
	 * 
	 * @param prefixedInvoiceId
	 * @param articleNr
	 * @param articleDesc
	 * @param quantity
	 * @param price
	 */
	public InvoicePosition(int id, String prefixedInvoiceId, int articleNr, String articleDesc, int quantity, BigDecimal price) {
		this.positionId = id;
		this.prefixedInvoiceId = prefixedInvoiceId;
		this.articleNumber = articleNr;
		this.articleDesc = articleDesc;
		this.quantity = quantity;
		this.price = price;
	}
	
	/**
	 * This method gets the unique and autoincremental position id.
	 * It is only successfull if the position id was already set, otherwise an
	 * IllegalStateException is thrown.
	 * 
	 * @throws IllegalStateException
	 * @return positionId
	 */
	public int getId() {
		if (positionId == -1) throw new IllegalStateException("ID not set");
        return positionId;
	}
	
	/**
	 * Returns prefixed invoice id.
	 * 
	 * @return prefixedInvoiceId
	 */
	public String getPrefixedInvoiceId() {
		return prefixedInvoiceId;
	}

	/**
	 * Returns article number.
	 * 
	 * @return articleNumber
	 */
	public int getArticleNr() {
		return articleNumber;
	}

	/**
	 * Returns article description.
	 * 
	 * @return articleDesc
	 */
	public String getArticleDesc() {
		return articleDesc;
	}

	/**
	 * Returns quantity.
	 * 
	 * @return quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Returns price.
	 * 
	 * @return price
	 */
	public BigDecimal getPrice() {
		return price;
	}
}
