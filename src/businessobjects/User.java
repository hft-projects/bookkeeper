package businessobjects;

/**
 * <h1>User</h1> 
 * This class provides values and functions for users.
 */
public class User {

	private String username;

	/**
	 * Construtor for user objects.
	 * 
	 * @param username
	 */
	public User(String username) {
		super();
		this.username = username;
	}

	/**
	 * Returns name of user
	 * 
	 * @return username
	 */
	public String getName() {
		return username;
	}
}
