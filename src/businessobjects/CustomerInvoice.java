package businessobjects;

import java.util.Date;

/**
 * <h1>Customer Invoice</h1> 
 * This class provides values and functions for customer invoices.
 * Extends the invoice class. 
 * @see Invoice
 */
public class CustomerInvoice extends Invoice {

	/**
	 * Construtor for customer invoice objects.
	 * 
	 * @param invoiceId
	 * @param taxNumber
	 * @param actorId
	 * @param status
	 * @param issueDate
	 * @param dueDate
	 * @param deliveryDate
	 * @param taxRate
	 * @param discount
	 */
	public CustomerInvoice(int invoiceId, String taxNumber, int actorId, int status, Date issueDate, Date dueDate,
			Date deliveryDate, int taxRate, int discount) {
		super(invoiceId, taxNumber, actorId, status, issueDate, dueDate, deliveryDate, taxRate, discount);
	}

	/**
	 * This method gets the unique and autoincremental invoice id and adds an identifier. 
	 * For customer invoices it is the string "AR".
	 * @return prefixed invoice id
	 */
	@Override
	public String getPrefixedInvoiceId() {
		return "AR" + getInvoiceId();
	}

	/**
	 * This method overrides the toString method
	 * and returns a string with all values of the customer invoice object and the invoice object.
	 * 
	 * @return String of customer invoice and invoice values
	 */
	@Override
	public String toString() {
		return "CustomerInvoice [prefixedInvoiceId=" + getPrefixedInvoiceId() + ", invoice=" + super.toString()
				+ "]";
	}
}
