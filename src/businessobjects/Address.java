package businessobjects;

/**
 * <h1>Address</h1> 
 * This class provides common address values and functions for customers and suppliers.
 */
public class Address {

	private int postcode;
	private String country;
	private String street;
	private String city;

	/**
	 * Construtor for address objects.
	 * 
	 * @param street
	 * @param country
	 * @param city
	 * @param postcode
	 */
	public Address(String street, String country, String city, int postcode) {
		super();
		this.postcode = postcode;
		this.street = street;
		this.city = city;
		this.country = country;
	}

	/**
	 * Returns postcode of address.
	 * 
	 * @return postcode
	 */
	public int getPostcode() {
		return postcode;
	}

	/**
	 * Returns street of address.
	 * 
	 * @return street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Returns city of address.
	 * 
	 * @return city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Returns country of address.
	 * 
	 * @return country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * This method overrides the toString method
	 * and returns a string with all values of the address object.
	 * 
	 * @return String of address values
	 */
	@Override
	public String toString() {
		return "Address [postcode=" + postcode + ", country=" + country + ", street=" + street + ", city=" + city + "]";
	}
}
