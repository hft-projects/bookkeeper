package businessobjects;

/**
 * <h>Customer</h>
 * This class provides values and functions for customers.
 * Extends the actor class. 
 * @see Actor
 */
public class Customer extends Actor {

	private int customerId;

	/**
	 * Construtor for customer objects.
	 * 
	 * @param customerNr
	 * @param name
	 * @param address
	 * @param email
	 * @param telephone
	 */
	public Customer(int customerNr, String name, Address address, String email, String telephone) {
		super(name, address, email, telephone);
		this.customerId = customerNr;

	}

	/**
	 * Returns unique and autoincremental customer id.
	 * 
	 * @return customerId
	 */
	public int getCustomerId() {
		return customerId;

	}
}