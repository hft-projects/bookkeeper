package businessobjects;

import java.util.Date;

/**
 * <h1>Supplier Invoice</h1> 
 * This class provides values and functions for supplier invoices. Extends the invoice class.
 * @see Invoice
 */
public class SupplierInvoice extends Invoice {

	private String externalInvoiceId;

	/**
	 * Construtor for supplier invoice objects without the external invoice id.
	 * 
	 * @param invoiceId
	 * @param externalInvoiceId
	 * @param actorId
	 * @param status
	 * @param issueDate
	 * @param dueDate
	 * @param deliveryDate
	 * @param taxRate
	 * @param discount
	 */
	public SupplierInvoice(int invoiceId, String externalInvoiceId, int actorId, int status, Date issueDate,
			Date dueDate, Date deliveryDate, int taxRate, int discount) {
		super(invoiceId, null, actorId, status, issueDate, dueDate, deliveryDate, taxRate, discount);
		this.externalInvoiceId = externalInvoiceId;
	}

	/**
	 * Construtor for supplier invoice objects with the external invoice id.
	 * 
	 * @param externalInvoiceId
	 * @param actorId
	 * @param status
	 * @param issueDate
	 * @param dueDate
	 * @param deliveryDate
	 * @param taxRate
	 * @param discount
	 */
	public SupplierInvoice(String externalInvoiceId, int actorId, int status, Date issueDate, Date dueDate,
			Date deliveryDate, int taxRate, int discount) {
		super(-1, null, actorId, status, issueDate, dueDate, deliveryDate, taxRate, discount);
		this.externalInvoiceId = externalInvoiceId;
	}

	/**
	 * This method gets the unique and autoincremental invoice id and adds an
	 * identifier. For supplier invoices it is the string "ER". It is only
	 * successfull if the invoice id was already set, otherwise an
	 * IllegalStateException is thrown.
	 * 
	 * @throws IllegalStateException
	 * @return prefixed invoice id
	 */
	@Override
	public String getPrefixedInvoiceId() {
		if (getInvoiceId() == -1)
			throw new IllegalStateException("Invoice number not set");
		return "ER" + getInvoiceId();
	}

	/**
	 * Returns external invoice id of supplier
	 * 
	 * @return externalInvoiceId
	 */
	public String getExternalInvoiceId() {
		return externalInvoiceId;
	}
}
