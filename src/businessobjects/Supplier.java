package businessobjects;

/**
 * <h>Supplier</h>
 * This class provides values and functions for suppliers.
 * Extends the actor class. 
 * @see Actor
 */
public class Supplier extends Actor {

	private int supplierId;
	private String taxNumber;
	private String bankName;
	private String bankNumber;
	private String bankOwner;
	
	/**
	 * Construtor for supplier objects.
	 * 
	 * @param supplierId
	 * @param name
	 * @param address
	 * @param email
	 * @param telephone
	 * @param taxNumber
	 * @param bankName
	 * @param bankNumber
	 * @param bankOwner
	 */
	public Supplier(int supplierId, String name, Address address, String email, String telephone, String taxNumber, String bankName, String bankNumber, String bankOwner) {
		super(name, address, email, telephone);
		this.supplierId = supplierId;
		this.taxNumber = taxNumber;
		this.bankName = bankName;
		this.bankNumber = bankNumber;
		this.bankOwner = bankOwner;
	}

	/**
	 * Returns unique and autoincremental supplier id.
	 * 
	 * @return supplierId
	 */
	public int getSupplierId() {
		return supplierId;
	}

	/**
	 * Returns tax number.
	 * 
	 * @return taxNumber
	 */
	public String getTaxNumber() {
		return taxNumber;
	}

	/**
	 * Returns name of bank contact.
	 * 
	 * @return bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * Returns number of bank contact.
	 * 
	 * @return bankNumber
	 */
	public String getBankNumber() {
		return bankNumber;
	}

	/**
	 * Returns owner of bank contact.
	 * 
	 * @return bankOwner
	 */
	public String getBankOwner() {
		return bankOwner;
	}
}
