package businessobjects;

/**
 * <h1>Actor</h1> 
 * This abstract class provides common values and functions for customers and suppliers.
 */
public abstract class Actor {

	private String name;
	private Address address;
	private String email;
	private String telephone;

	/**
	 * Construtor for actor objects.
	 * 
	 * @param name
	 * @param address
	 * @param email
	 * @param telephone
	 */
	public Actor(String name, Address address, String email, String telephone) {
		this.name = name;
		this.address = address;
		this.email = email;
		this.telephone = telephone;
	}

	/**
	 * Returns name of actor.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns address of actor.
	 * 
	 * @return address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Returns email of actor.
	 * 
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Returns telephone number of actor.
	 * 
	 * @return telephone number
	 */
	public String getTelephone() {
		return telephone;
	}

	@Override
	public String toString() {
		return name;
	}
}
