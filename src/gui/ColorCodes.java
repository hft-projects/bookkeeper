package gui;

/**
 * <h1>Color Codes</h1> 
 * This class provides constants for the ui colors.
 */
public class ColorCodes {

	/**
	 * Constants for colors of the application.
	 */
	public static final String MAINBG = "#0F1019";
	public static final String MAINRED = "#EF253D";
}
