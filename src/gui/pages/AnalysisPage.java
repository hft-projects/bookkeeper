package gui.pages;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;

import gui.ColorCodes;
import gui.screens.dialogs.reports.ReporttypeSelectionDialog;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperIcons;

import gui.utils.ReportRenderer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <h1>Analysis Page</h1> 
 * This class provides functions to create the analysis page.
 */
public class AnalysisPage extends JPanel {

	private JPanel analysisDisplay;

	private SwingController controller;
	private JPanel viewerComponentPanel;

	private List<File> reportFiles;
	private DefaultListModel<File> listModel;
	private JList<File> listReports;
	
	public static final String REPORT_PATH = "reports";
	
	/**
	 * Constructor for creating the panel.
	 */
	public AnalysisPage() {

		setLayout(new BorderLayout(0, 0));
		setBackground(Color.decode(ColorCodes.MAINBG));

		reportFiles = new ArrayList<File>();
		
		add(BorderLayout.WEST, renderReportList());
		add(BorderLayout.CENTER, renderAnalysisDisplay());

	}

	/**
	 * This method renders the report list on the side menu.
	 * 
	 * @return report list panel
	 */
	private JPanel renderReportList() {

		JPanel reportListPanel = new JPanel();

		reportListPanel.setLayout(new BorderLayout());

		listReports = new JList<File>(); 

		refreshReports(getAllFilesInFolder(REPORT_PATH));
		
		listModel = new DefaultListModel<File>(); 
		for (File reportFile: reportFiles) {
			listModel.addElement(reportFile);

		}
		
		listReports.setModel(listModel);
		listReports.setCellRenderer(new ReportRenderer());
		listReports.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listReports.setBackground(Color.decode(ColorCodes.MAINBG));

		listReports.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();

				int firstIndex = e.getFirstIndex();
				int lastIndex = e.getLastIndex();
				boolean isAdjusting = e.getValueIsAdjusting();

				if (lsm.isSelectionEmpty() == false) {
					// Find out which indexes are selected.
					int minIndex = lsm.getMinSelectionIndex();
					int maxIndex = lsm.getMaxSelectionIndex();
					for (int i = minIndex; i <= maxIndex; i++) {
						if (lsm.isSelectedIndex(i)) {
							
							File selectedReport = listReports.getSelectedValue();
							displayReport(selectedReport);
						}
					}
				}

			}
		});

		JScrollPane invoiceListScrollPane = new JScrollPane(listReports);

		invoiceListScrollPane.setBackground(Color.decode(ColorCodes.MAINBG));

		reportListPanel.add(BorderLayout.CENTER, invoiceListScrollPane);

		reportListPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		reportListPanel.setPreferredSize(new Dimension(400, 400));

		JButton bCreateReport = BookkeeperButtons.renderNormalButton("Bericht erstellen", new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ReporttypeSelectionDialog rtsd = new ReporttypeSelectionDialog(reportListPanel);
				rtsd.showDialog();
				refreshReports(getAllFilesInFolder(REPORT_PATH));
			}
		});
		bCreateReport.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_PLUS_ICON_FILEPATH), 25, 25));

		
		reportListPanel.add(BorderLayout.PAGE_END, bCreateReport);
		
		return reportListPanel;
	}

	/**
	 * This private method renders the analysis display.
	 * 
	 * @return analysis display panel
	 */
	private JPanel renderAnalysisDisplay() {

		analysisDisplay = new JPanel(new BorderLayout());

		analysisDisplay.setBackground(Color.decode(ColorCodes.MAINBG));

		controller = new SwingController();

		SwingViewBuilder factory = new SwingViewBuilder(controller);

		viewerComponentPanel = factory.buildViewerPanel();

		controller.getDocumentViewController().setAnnotationCallback(
				new org.icepdf.ri.common.MyAnnotationCallback(controller.getDocumentViewController()));

		viewerComponentPanel.setVisible(false);
		analysisDisplay.add(BorderLayout.CENTER, viewerComponentPanel);

		this.validate();
		this.repaint();
		return analysisDisplay;

	}

	/**
	 * This private method updates the report list.
	 * @param newList
	 */
	private void refreshReports(ArrayList<File> newList) {

		reportFiles = new ArrayList<File>();

		reportFiles.addAll(newList);

		listModel = new DefaultListModel<File>(); 
		for (File reportFile: reportFiles) {
			listModel.addElement(reportFile);

		}
		
		listReports.setModel(listModel);
		listReports.updateUI();

		validate();
		repaint();
	}
	
	/**
	 * This private method displays the report.
	 * @param report
	 */
	private void displayReport(File report) {
		
		viewerComponentPanel.setVisible(true);

		File pdfFile = report;

		
		controller.openDocument(pdfFile.getAbsolutePath());

		this.validate();
		this.repaint();
		
	}
	
	/**
	 * This private method puts all created reports in folder.
	 * @param folderpath
	 * @return report list
	 */
	private ArrayList<File> getAllFilesInFolder(String folderpath) {
		
		File reportFolder = new File(REPORT_PATH);
		if(reportFolder.exists() == false)
			reportFolder.mkdirs();
		
		try (Stream<Path> walk = Files.walk(Paths.get(folderpath))) {

			List<String> result = walk.filter(Files::isRegularFile).map(x -> x.toString()).collect(Collectors.toList());

			ArrayList<File> files = new ArrayList<File>();
			for (String fs : result) {
				files.add(new File(fs));
			}

			return files;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
