package gui.pages;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import businessobjects.Actor;
import businessobjects.Supplier;
import dbaccess.ActorAccess;
import dbaccess.CustomerAccess;

import dbaccess.SupplierAccess;
import gui.ColorCodes;
import gui.screens.dialogs.CreateCustomerDialog;
import gui.screens.dialogs.CreateSupplierDialog;
import gui.utils.ActorListPopupMenu;
import gui.utils.ActorRenderer;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperIcons;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Actor Page</h1> 
 * This class provides functions to create the actor page.
 */
public class ActorPage extends JPanel {

	private List<Actor> actors;

	private JList<Actor> actorJList;
	private DefaultListModel<Actor> listModel;
	private JPanel topBar;

	private JButton bShowAllActors;
	private JButton bShowCustomers;
	private JButton bShowSuppliers;

	private JPanel actorDisplayPanel;

	private JLabel lblName;
	private JLabel lblTaxNr;
	private JLabel lblEmail;
	private JLabel lblPhone;
	private JLabel lblAdress;
	private JLabel lblBank;

	private JLabel lblBankTitle;

	private static final int TOP_STATUS_ALL = 0;
	private static final int TOP_STATUS_ALLCUSTOMERS = 1;
	private static final int TOP_STATUS_ALLSUPPLIERS = 2;

	private int selectedTopmenuButton = 0;

	/**
	 * Constructor for creating the panel.
	 */
	public ActorPage() {

		setLayout(new BorderLayout(0, 0));
		setBackground(Color.decode(ColorCodes.MAINBG));

		add(BorderLayout.PAGE_START, renderTopBar());
		add(BorderLayout.WEST, renderActorList());
		add(BorderLayout.CENTER, renderActorDisplay());

	}

	/**
	 * This method renders the top bar.
	 * 
	 * @return top bar
	 */
	private JPanel renderTopBar() {

		topBar = new JPanel();

		topBar.setPreferredSize(new Dimension(300, 70));
		topBar.setLayout(new BorderLayout());
		topBar.setBackground(Color.WHITE);

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.setBackground(topBar.getBackground());

		JButton bAddCustoemr = BookkeeperButtons.renderNormalButton("Kunde hinzufügen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CreateCustomerDialog dialog = new CreateCustomerDialog();
				
				showAllActors();

			}
		});
		bAddCustoemr.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.CUSTOMER_ICON_FILEPATH), 25, 25));
		buttonPanel.add(bAddCustoemr);

		JButton bAddSupplier = BookkeeperButtons.renderNormalButton("Lieferant hinzufügen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CreateSupplierDialog dialog = new CreateSupplierDialog();
				
				showAllActors();

			}
		});
		bAddSupplier.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.SUPPLIER_ICON_FILEPATH), 25, 25));
		buttonPanel.add(bAddSupplier);

		JButton bRefresh = BookkeeperButtons.renderNormalButton("Aktualisieren", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				showAllActors();
			}
		});

		bRefresh.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.REFRESH_ICON_FILEPATH), 25, 25));
		buttonPanel.add(bRefresh);

		topBar.add(BorderLayout.WEST, renderTopMenuDifferentSelecions());
		topBar.add(BorderLayout.EAST, buttonPanel);

		return topBar;
	}

	/**
	 * This method renders the different selection options in second top bar.
	 * 
	 * @return selection panel
	 */
	private JPanel renderTopMenuDifferentSelecions() {

		JPanel differentSelectionsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		differentSelectionsPanel.setBackground(topBar.getBackground());

		bShowAllActors = BookkeeperButtons.renderInvoicePageTopBarSelectionButton("Alle", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				selectedTopmenuButton = TOP_STATUS_ALL;
				showAllActors();
				colorTopButtonWhenSelected();
			}
		});
		bShowAllActors.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICESICON_FILEPATH), 25, 25));
		differentSelectionsPanel.add(bShowAllActors);

		bShowCustomers = BookkeeperButtons.renderInvoicePageTopBarSelectionButton("Kunden", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				selectedTopmenuButton = TOP_STATUS_ALLCUSTOMERS;
				showAllCustomers();
				colorTopButtonWhenSelected();
			}
		});
		bShowCustomers.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.CUSTOMER_ICON_FILEPATH), 25, 25));
		differentSelectionsPanel.add(bShowCustomers);

		bShowSuppliers = BookkeeperButtons.renderInvoicePageTopBarSelectionButton("Lieferanten", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				selectedTopmenuButton = TOP_STATUS_ALLSUPPLIERS;
				showAllSuppliers();
				colorTopButtonWhenSelected();
			}
		});
		bShowSuppliers.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.SUPPLIER_ICON_FILEPATH), 25, 25));
		differentSelectionsPanel.add(bShowSuppliers);

		return differentSelectionsPanel;
	}

	/**
	 * This method colors the buttons when they are selected.
	 */
	private void colorTopButtonWhenSelected() {
		
		bShowAllActors.setBackground(Color.WHITE);
		bShowAllActors.setBorder(new EmptyBorder(BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING,
				BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING));

		bShowCustomers.setBackground(Color.WHITE);
		bShowCustomers.setBorder(new EmptyBorder(BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING,
				BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING));

		bShowSuppliers.setBackground(Color.WHITE);
		bShowSuppliers.setBorder(new EmptyBorder(BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING,
				BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING));

		switch (selectedTopmenuButton) {
		case TOP_STATUS_ALL: {
			bShowAllActors.setBackground(Color.decode(ColorCodes.MAINRED));
			bShowAllActors.setBorder(new LineBorder(Color.decode(ColorCodes.MAINRED), BookkeeperButtons.BUTTONPADDING));

			break;

		}
		case TOP_STATUS_ALLCUSTOMERS: {
			bShowCustomers.setBackground(Color.decode(ColorCodes.MAINRED));
			bShowCustomers.setBorder(new LineBorder(Color.decode(ColorCodes.MAINRED), BookkeeperButtons.BUTTONPADDING));
			break;

		}

		case TOP_STATUS_ALLSUPPLIERS: {

			bShowSuppliers.setBackground(Color.decode(ColorCodes.MAINRED));
			bShowSuppliers.setBorder(new LineBorder(Color.decode(ColorCodes.MAINRED), BookkeeperButtons.BUTTONPADDING));
			break;
		}
		}
	}

	/**
	 * This method renders the actors list on the side menu.
	 * @return actor list
	 */
	private JPanel renderActorList() {

		JPanel actorListPanel = new JPanel(); 
		actorListPanel.setLayout(new BorderLayout());

		actorJList = new JList<Actor>();

		refreshActors(getActorsFromDB());

		listModel = new DefaultListModel<Actor>();
		for (Actor actor : actors) {
			listModel.addElement(actor);

		}

		actorJList.setModel(listModel);
		actorJList.setCellRenderer(new ActorRenderer());
		actorJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		actorJList.setBackground(Color.decode(ColorCodes.MAINBG));

		actorJList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();

				int firstIndex = e.getFirstIndex();
				int lastIndex = e.getLastIndex();
				boolean isAdjusting = e.getValueIsAdjusting();

				if (lsm.isSelectionEmpty() == false) {
					// Find out which indexes are selected.
					int minIndex = lsm.getMinSelectionIndex();
					int maxIndex = lsm.getMaxSelectionIndex();
					for (int i = minIndex; i <= maxIndex; i++) {
						if (lsm.isSelectedIndex(i)) {
							displayActor(actors.get(i));

							Actor actor = actorJList.getSelectedValue();

							ActorListPopupMenu alpm = new ActorListPopupMenu(actor);
							actorJList.setComponentPopupMenu(alpm);
							actorJList.setInheritsPopupMenu(true);
														
						}
					}
				}

			}
		});

		JScrollPane actorListScrollPane = new JScrollPane(actorJList); // A Scrollview thht contains the List

		actorListScrollPane.setBackground(Color.decode(ColorCodes.MAINBG));

		actorListPanel.add(BorderLayout.CENTER, actorListScrollPane);

		actorListPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		actorListPanel.setPreferredSize(new Dimension(400, 400));

		return actorListPanel;
	}

	/**
	 * This method renders the display of actors.
	 * @return actor display panel
	 */
	private JPanel renderActorDisplay() {

		int textInputPadding = 20;
		int spaceBetweenElements = 20;

		actorDisplayPanel = new JPanel(new GridBagLayout());
		actorDisplayPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		actorDisplayPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		actorDisplayPanel
				.setBorder(new EmptyBorder(textInputPadding, textInputPadding, textInputPadding, textInputPadding));
		actorDisplayPanel.setVisible(false);

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.anchor = GridBagConstraints.WEST;

		lblName = new JLabel("");
		lblName.setForeground(Color.decode(ColorCodes.MAINRED));
		lblName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 40));
		actorDisplayPanel.add(lblName, c);

		c.gridy = c.gridy++;
		actorDisplayPanel.add(getSpace(0, spaceBetweenElements), c);

		lblTaxNr = new JLabel("");
		lblTaxNr.setForeground(Color.WHITE);
		lblTaxNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		actorDisplayPanel.add(lblTaxNr, c);

		c.gridy = c.gridy++;
		actorDisplayPanel.add(getSpace(0, spaceBetweenElements), c);

		lblEmail = new JLabel("");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		actorDisplayPanel.add(lblEmail, c);

		c.gridy = c.gridy++;
		actorDisplayPanel.add(getSpace(0, spaceBetweenElements), c);

		lblPhone = new JLabel("");
		lblPhone.setForeground(Color.WHITE);
		lblPhone.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		actorDisplayPanel.add(lblPhone, c);

		c.gridy = c.gridy++;
		actorDisplayPanel.add(getSpace(0, spaceBetweenElements), c);

		c.gridy = c.gridy++;
		JLabel lblAdressTitle = new JLabel("Adresse:");
		lblAdressTitle.setForeground(Color.decode(ColorCodes.MAINRED));
		lblAdressTitle.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		actorDisplayPanel.add(lblAdressTitle, c);

		c.gridy = c.gridy++;
		lblAdress = new JLabel("");
		lblAdress.setForeground(Color.WHITE);
		lblAdress.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		actorDisplayPanel.add(lblAdress, c);

		c.gridy = c.gridy++;
		actorDisplayPanel.add(getSpace(0, spaceBetweenElements), c);

		c.gridy = c.gridy++;
		lblBankTitle = new JLabel("Bankdaten:");
		lblBankTitle.setForeground(Color.decode(ColorCodes.MAINRED));
		lblBankTitle.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		lblBankTitle.setVisible(false);
		actorDisplayPanel.add(lblBankTitle, c);

		c.gridy = c.gridy++;
		lblBank = new JLabel("");
		lblBank.setForeground(Color.WHITE);
		lblBank.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		actorDisplayPanel.add(lblBank, c);

		return actorDisplayPanel;
	}

	/**
	 * This private method refreshes the actors.
	 * 
	 * @param newList
	 */
	private void refreshActors(ArrayList<Actor> newList) {

		actors = new ArrayList<Actor>();
		actors.addAll(newList);

		listModel = new DefaultListModel<Actor>();
		for (Actor actor : actors) {
			listModel.addElement(actor);
		}

		actorJList.setModel(listModel);
		actorJList.updateUI();

		validate();
		repaint();
	}

	/**
	 * This private method refreshes the array list and displays all actors.
	 */
	private void showAllActors() {
		refreshActors(getActorsFromDB());

	}

	/**
	 * This private method refreshes the array list and displays all suppliers.
	 */
	private void showAllSuppliers() {

		ArrayList<Actor> suppliers = new ArrayList<Actor>();

		try {
			suppliers.addAll(SupplierAccess.getAllSuppliers());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		refreshActors(suppliers);
	}

	/**
	 * This method refreshes the array list and displays all customers.
	 */
	private void showAllCustomers() {

		ArrayList<Actor> customers = new ArrayList<Actor>();

		try {
			customers.addAll(CustomerAccess.getAllCustomers());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		refreshActors(customers);
	}

	/**
	 * This private method gets all actors from database 
	 * @return all actors
	 */
	private ArrayList<Actor> getActorsFromDB() {

		ArrayList<Actor> allActors = new ArrayList<Actor>();

		try {
			allActors.addAll(ActorAccess.getActorsFromDatabase());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return allActors;

	}

	/**
	 * This private method displays the actor in detail view
	 * @param actor
	 */
	private void displayActor(Actor actor) {

		actorDisplayPanel.setVisible(true);

		lblName.setText(actor.getName());
		lblPhone.setText("Tel.: " + actor.getTelephone());
		lblEmail.setText("Email: " + actor.getEmail());

		String adress = "<html>" + actor.getAddress().getStreet() + "<br/>" + actor.getAddress().getPostcode() + " "
				+ actor.getAddress().getCity() + "<br/>" + actor.getAddress().getCountry() + "</html>";
		lblAdress.setText(adress);

		lblBankTitle.setVisible(false);
		lblBank.setText("");
		lblTaxNr.setText("");

		if (actor.getClass().getSimpleName().equals("Supplier")) {

			Supplier s = (Supplier) actor;

			String bankDetails = "<html>" + s.getBankName() + "<br/> Kontoinhaber: " + s.getBankOwner() + "<br/> IBAN: "
					+ s.getBankNumber() + "</html>";
			lblBank.setText(bankDetails);

			lblBankTitle.setVisible(true);

			lblTaxNr.setText("USt.-Nr.: " + s.getTaxNumber());

		}

	}

	/*
	 * This method returns a JPanel that can be used as space between elements
	 */
	private JPanel getSpace(int width, int height) {

		JPanel space = new JPanel();

		space.setBackground(new Color(0f, 0f, 0f, .0f));
		space.setPreferredSize(new Dimension(width, height));

		return space;
	}
}
