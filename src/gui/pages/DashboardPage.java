package gui.pages;

import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.jfree.chart.ChartPanel;

import businessobjects.User;
import gui.ColorCodes;
import gui.screens.dialogs.invoices.InvoicetypeSelectionDialog;
import gui.utils.BookkeeperButtons;
import services.ChartCreation;
import services.ChartDataset;
import services.PositionLogic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * <h1>Dashboard Page</h1> 
 * This class provides functions to create the dashboard page.
 */
public class DashboardPage extends JPanel {

	private User user;

	private String StringbankBalanceString;
	private String StringTotalRevenue;
	private String StringTotalExpenses;

	/**
	 * Constructor for creating the panel.
	 */
	public DashboardPage(User user) {

		this.user = user;

		StringbankBalanceString = "0€";
		StringTotalExpenses = "0€";
		StringTotalRevenue = "0€";

		updateValues();

		setLayout(new BorderLayout(0, 0));
		setBackground(Color.WHITE);

		add(BorderLayout.PAGE_START, renderSecTopBar());
		add(BorderLayout.CENTER, renderMainContent());

	}

	/**
	 * This method renders the top bar with the funtion to add new invoices and the welcoming message.
	 * @return top bar
	 */
	private JPanel renderSecTopBar() {

		int topBarPadding = 10;

		JPanel topBar = new JPanel(new BorderLayout());
		topBar.setBackground(this.getBackground());
		topBar.setBorder(new EmptyBorder(topBarPadding, topBarPadding, topBarPadding, topBarPadding));

		JPanel leftSide = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel rightSide = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		leftSide.setBackground(topBar.getBackground());
		rightSide.setBackground(topBar.getBackground());

		topBar.add(BorderLayout.WEST, leftSide);
		topBar.add(BorderLayout.EAST, rightSide);

		JLabel lblWelcome = new JLabel("<html>Hallo " + "<b style=\"color:" + ColorCodes.MAINRED + "\\\">"
				+ user.getName() + "</b>" + " arbeite da weiter wo du aufgehört hast!</html>");
		lblWelcome.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		lblWelcome.setForeground(Color.decode(ColorCodes.MAINBG));
		int padding = 20;
		lblWelcome.setBorder(new EmptyBorder(padding, padding, 0, 0));
		leftSide.add(lblWelcome);

		JButton bNew = BookkeeperButtons.renderNormalButton("Neue Rechnung", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				InvoicetypeSelectionDialog itsd = new InvoicetypeSelectionDialog(rightSide);
				itsd.showDialog();
			}
		});

		rightSide.add(bNew);

		return topBar;
	}

	/**
	 * This private method renders the main content of the dashboard page.
	 * @return main content panel
	 */
	private JPanel renderMainContent() {

		int mainPadding = 10;
		JPanel mainContent = new JPanel(new BorderLayout());
		mainContent.setBackground(this.getBackground());
		mainContent.setBorder(new EmptyBorder(mainPadding, mainPadding, mainPadding, mainPadding));

		int innerPanelsPadding = 30;
		int subTitelSize = 25;
		Color subTitelColor = Color.decode(ColorCodes.MAINRED);
		Color subPanelsColor = Color.decode(ColorCodes.MAINBG);
		Dimension innerSidepanelsSize = new Dimension(600, 350);
		Border innerPannelsInnerBorder = new LineBorder(mainContent.getBackground(), 10);
		Border titlePadding = new EmptyBorder(20, 0, 0, 0);

		JPanel leftSide = new JPanel(new GridBagLayout());
		leftSide.setBackground(subPanelsColor);
		leftSide.setBorder(innerPannelsInnerBorder);
		leftSide.setPreferredSize(innerSidepanelsSize);

		GridBagConstraints cl = new GridBagConstraints();
		cl.gridx = 0;
		cl.anchor = GridBagConstraints.NORTHWEST;

		cl.gridy = 0;
		JLabel lblLeftTitel = new JLabel("Bank");
		lblLeftTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, subTitelSize));
		lblLeftTitel.setForeground(subTitelColor);
		lblLeftTitel.setBorder(titlePadding);
		leftSide.add(lblLeftTitel, cl);

		cl.gridy++;
		leftSide.add(renderValuePanel("Kontostand: ", StringbankBalanceString), cl);

		cl.gridy++;
		leftSide.add(renderValuePanel("Offene Einnahmen: ", StringTotalRevenue), cl);

		cl.gridy++;
		leftSide.add(renderValuePanel("Offene Ausgaben: ", StringTotalExpenses), cl);

		JPanel rightSide = new JPanel(new GridBagLayout());
		rightSide.setBackground(subPanelsColor);
		rightSide.setBorder(innerPannelsInnerBorder);
		rightSide.setPreferredSize(innerSidepanelsSize);

		GridBagConstraints cr = new GridBagConstraints();
		cr.gridx = 0;
		cr.anchor = GridBagConstraints.NORTHWEST;

		cr.gridy = 0;
		JLabel lblRightTitel = new JLabel("Übersicht");
		lblRightTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, subTitelSize));
		lblRightTitel.setForeground(subTitelColor);
		lblRightTitel.setBorder(titlePadding);
		rightSide.add(lblRightTitel, cr);

		cr.gridy++;

		Dimension chartSize = new Dimension(400, 350);
		
		LocalDateTime now = LocalDateTime.now();
		int year = now.getYear();
		ChartPanel panel = new ChartPanel(ChartCreation.createYearChartRevenue(year, ChartDataset.createYearDatasetRevenue(year)));
		panel.setPreferredSize(chartSize);

		rightSide.add(panel, cr);
		
		cr.gridx++;

		ChartPanel panel2 = new ChartPanel(ChartCreation.createYearChartExpenses(year, ChartDataset.createYearDatasetExpenses(year)));
		panel2.setPreferredSize(chartSize);
		
		rightSide.add(panel2, cr);

		mainContent.add(BorderLayout.WEST, leftSide);
		mainContent.add(BorderLayout.CENTER, rightSide);

		return mainContent;

	}

	/**
	 * This private method renders the value panel on the left side of the dashboard page.
	 * @param title
	 * @param value
	 * @return value panel
	 */
	private JPanel renderValuePanel(String title, String value) {

		Border padding = new EmptyBorder(20, 20, 20, 20);
		Border margin = new LineBorder(Color.decode(ColorCodes.MAINBG), 5);

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBackground(Color.WHITE);
		panel.setBorder(margin);
		panel.setPreferredSize(new Dimension(500, 80));

		JPanel leftSide = new JPanel(new FlowLayout(FlowLayout.LEFT));
		leftSide.setBackground(panel.getBackground());
		leftSide.setBorder(padding);

		JLabel lblBankBalance = new JLabel(title);
		lblBankBalance.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		lblBankBalance.setForeground(Color.decode(ColorCodes.MAINBG));
		leftSide.add(lblBankBalance);

		JPanel rightSide = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		rightSide.setBackground(panel.getBackground());
		rightSide.setBorder(padding);

		JLabel lblBankBalanceValue = new JLabel(value);
		lblBankBalanceValue.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		lblBankBalanceValue.setForeground(Color.decode(ColorCodes.MAINRED));
		rightSide.add(lblBankBalanceValue);

		panel.add(BorderLayout.WEST, leftSide);
		panel.add(BorderLayout.CENTER, rightSide);

		return panel;

	}

	/**
	 * This method updates the values of the value panel.
	 */
	public void updateValues(){

		StringbankBalanceString = PositionLogic.getProfit().setScale(2, RoundingMode.FLOOR).toString() + "€";
		StringTotalExpenses = PositionLogic.getOpenExpenses().setScale(2, RoundingMode.FLOOR).toString() + "€";
		StringTotalRevenue = PositionLogic.getOpenRevenue().setScale(2, RoundingMode.FLOOR).toString() + "€";

		validate();
		repaint();
	}
}
