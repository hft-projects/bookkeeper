package gui.pages;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;

import businessobjects.Actor;
import businessobjects.Invoice;
import dbaccess.ActorAccess;
import dbaccess.CustomerInvoiceAccess;
import dbaccess.SupplierInvoiceAccess;
import gui.ColorCodes;
import gui.screens.dialogs.invoices.InvoicetypeSelectionDialog;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperIcons;
import gui.utils.InvoiceListPopupMenu;
import gui.utils.InvoiceRenderer;
import services.PDF;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Invoice Page</h1> 
 * This class provides functions to create the dashboard page.
 */
public class InvoicePage extends JPanel {

	private JPanel topBar;
	private JPanel sideMenu;
	private JPanel invoiceDisplay;

	private JButton bShowAllInvoices;
	private JButton bShowCustomerInvoices;
	private JButton bShowSupplierInvoices;

	private List<Invoice> invoices;
	private DefaultListModel<Invoice> listModel;
	private JList<Invoice> listInvoices;

	private SwingController controller;
	private JPanel viewerComponentPanel;

	private static final int TOP_STATUS_ALLINVOICES = 0;
	private static final int TOP_STATUS_ALLCUSTOMERINVOICES = 1;
	private static final int TOP_STATUS_ALLSUPPLIERINVOICES = 2;

	private int selectedTopmenuButton = 0;

	/**
	 * Constructor for creating the panel.
	 */
	public InvoicePage() {

		setLayout(new BorderLayout(0, 0));
		setBackground(Color.decode(ColorCodes.MAINBG));

		invoices = new ArrayList<Invoice>();

		add(BorderLayout.PAGE_START, renderTopBar());
		add(BorderLayout.WEST, renderLeftPanels());
		add(BorderLayout.CENTER, renderInvoiceDisplay());

	}

	/**
	 * This private method updates the invoice list.
	 * 
	 * @param newList
	 */
	private void refreshInvoices(ArrayList<Invoice> newList) {

		invoices = new ArrayList<Invoice>();

		invoices.addAll(newList);

		listModel = new DefaultListModel<Invoice>(); // The Listmodell
		for (Invoice invoice : invoices) {
			listModel.addElement(invoice);
		}

		listInvoices.setModel(listModel);
		listInvoices.updateUI();

		validate();
		repaint();
	}

	/**
	 * This private method rendes and display the top bar line, but not the navigation.
	 * 
	 * @return top bar panel
	 */
	private JPanel renderTopBar() {

		topBar = new JPanel();

		topBar.setPreferredSize(new Dimension(300, 70));
		topBar.setLayout(new BorderLayout());
		topBar.setBackground(Color.WHITE);

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.setBackground(topBar.getBackground());

		JButton bAddInvoice = BookkeeperButtons.renderNormalButton("Neue Rechnung", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				InvoicetypeSelectionDialog itsd = new InvoicetypeSelectionDialog(buttonPanel);
				itsd.showDialog();

				refreshInvoices(getAllInvoicesFromDB());

			}
		});
		bAddInvoice.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICESICON_FILEPATH), 25, 25));
		buttonPanel.add(bAddInvoice);

		JButton bRefresh = BookkeeperButtons.renderNormalButton("Aktualisieren", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				showAllInvoices();

			}
		});

		bRefresh.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.REFRESH_ICON_FILEPATH), 25, 25));
		buttonPanel.add(bRefresh);

		topBar.add(BorderLayout.WEST, renderTopMenuDifferentSelecions());
		topBar.add(BorderLayout.EAST, buttonPanel);

		return topBar;

	}

	/**
	 * This private method renders the different selections at the top bar.
	 * 
	 * @return selection panel
	 */
	private JPanel renderTopMenuDifferentSelecions() {

		JPanel differentSelectionsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		differentSelectionsPanel.setBackground(topBar.getBackground());
		bShowAllInvoices = BookkeeperButtons.renderInvoicePageTopBarSelectionButton("Alle Rechnungen",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						selectedTopmenuButton = TOP_STATUS_ALLINVOICES;

						showAllInvoices();
						colorTopButtonWhenSelected();
					}
				});
		bShowAllInvoices.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICESICON_FILEPATH), 25, 25));
		differentSelectionsPanel.add(bShowAllInvoices);

		bShowCustomerInvoices = BookkeeperButtons.renderInvoicePageTopBarSelectionButton("Ausgangsrechnungen",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						selectedTopmenuButton = TOP_STATUS_ALLCUSTOMERINVOICES;

						showAllCustomerInvoices();
						colorTopButtonWhenSelected();
					}
				});
		bShowCustomerInvoices.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_PLUS_ICON_FILEPATH), 25, 25));
		differentSelectionsPanel.add(bShowCustomerInvoices);

		bShowSupplierInvoices = BookkeeperButtons.renderInvoicePageTopBarSelectionButton("Eingangsrechnungen",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						selectedTopmenuButton = TOP_STATUS_ALLSUPPLIERINVOICES;

						showAllSupplierInvoices();
						colorTopButtonWhenSelected();
					}
				});
		bShowSupplierInvoices.setIcon(BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_MIUNS_ICON_FILEPATH), 25, 25));
		differentSelectionsPanel.add(bShowSupplierInvoices);

		return differentSelectionsPanel;
	}

	/**
	 * This private method colors the buttons when selected.
	 */
	private void colorTopButtonWhenSelected() {
		// SelectedStyle

		bShowAllInvoices.setBackground(Color.WHITE);
		bShowAllInvoices.setBorder(new EmptyBorder(BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING,
				BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING));

		bShowCustomerInvoices.setBackground(Color.WHITE);
		bShowCustomerInvoices.setBorder(new EmptyBorder(BookkeeperButtons.BUTTONPADDING,
				BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING));

		bShowSupplierInvoices.setBackground(Color.WHITE);
		bShowSupplierInvoices.setBorder(new EmptyBorder(BookkeeperButtons.BUTTONPADDING,
				BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING, BookkeeperButtons.BUTTONPADDING));

		switch (selectedTopmenuButton) {
		case TOP_STATUS_ALLINVOICES: {
			bShowAllInvoices.setBackground(Color.decode(ColorCodes.MAINRED));
			bShowAllInvoices
					.setBorder(new LineBorder(Color.decode(ColorCodes.MAINRED), BookkeeperButtons.BUTTONPADDING));

			break;

		}
		case TOP_STATUS_ALLCUSTOMERINVOICES: {
			bShowCustomerInvoices.setBackground(Color.decode(ColorCodes.MAINRED));
			bShowCustomerInvoices
					.setBorder(new LineBorder(Color.decode(ColorCodes.MAINRED), BookkeeperButtons.BUTTONPADDING));
			break;

		}

		case TOP_STATUS_ALLSUPPLIERINVOICES: {

			bShowSupplierInvoices.setBackground(Color.decode(ColorCodes.MAINRED));
			bShowSupplierInvoices
					.setBorder(new LineBorder(Color.decode(ColorCodes.MAINRED), BookkeeperButtons.BUTTONPADDING));
			break;

		}
		}
	}

	/**
	 * This private method renders and display the side menu of the invoice page.
	 * 
	 * @return side menu
	 */
	private JPanel renderSideMenu() {

		int iconWidthHeight = 20;

		sideMenu = new JPanel();
		sideMenu.setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.anchor = GridBagConstraints.PAGE_START;

		sideMenu.setBackground(Color.decode(ColorCodes.MAINRED));
		sideMenu.setPreferredSize(new Dimension(300, 300));

		JButton bOpenInvoices = BookkeeperButtons.renderSidemenuButton("Offene Rechnungen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				showInvoicesFromStatus(Invoice.STATUS_OPEN);
			}
		}, sideMenu.getPreferredSize());
		bOpenInvoices.setIcon(
				BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_OPEN_ICON_FILEPATH), iconWidthHeight, iconWidthHeight));
		sideMenu.add(bOpenInvoices, gbc);

		JButton bInProcessInvoice = BookkeeperButtons.renderSidemenuButton("Rechnungen in Bearbeitung",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						showInvoicesFromStatus(Invoice.STATUS_IN_PROCESS);
					}
				}, sideMenu.getPreferredSize());
		bInProcessInvoice.setIcon(
				BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_INPROGRESS_ICON_FILEPATH), iconWidthHeight, iconWidthHeight));
		sideMenu.add(bOpenInvoices, gbc);
		sideMenu.add(bInProcessInvoice, gbc);

		JButton bOverdueInvoices = BookkeeperButtons.renderSidemenuButton("Überfällige Rechnungen",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						showInvoicesFromStatus(Invoice.STATUS_OVERDUE);
					}
				}, sideMenu.getPreferredSize());
		bOverdueInvoices.setIcon(
				BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_OVERDUE_ICON_FILEPATH), iconWidthHeight, iconWidthHeight));
		sideMenu.add(bOpenInvoices, gbc);
		sideMenu.add(bOverdueInvoices, gbc);

		JButton bPaidInvoides = BookkeeperButtons.renderSidemenuButton("Bezahlte Rechnungen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				showInvoicesFromStatus(Invoice.STATUS_PAID);
			}
		}, sideMenu.getPreferredSize());
		bPaidInvoides.setIcon(
				BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_PAYIED_ICON_FILEPATH), iconWidthHeight, iconWidthHeight));
		sideMenu.add(bOpenInvoices, gbc);
		sideMenu.add(bPaidInvoides, gbc);

		JButton bCanceledInvoides = BookkeeperButtons.renderSidemenuButton("Stornierte Rechnungen",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						showInvoicesFromStatus(Invoice.STATUS_CANCELED);
					}
				}, sideMenu.getPreferredSize());
		bCanceledInvoides.setIcon(
				BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_CANCELED_ICON_FILEPATH), iconWidthHeight, iconWidthHeight));
		sideMenu.add(bOpenInvoices, gbc);
		sideMenu.add(bCanceledInvoides, gbc);

		JPanel sep = new JPanel(new BorderLayout());
		sep.setPreferredSize(new Dimension(300, 300));
		sep.add(BorderLayout.PAGE_START, sideMenu);
		sep.setBackground(Color.decode(ColorCodes.MAINRED));

		return sep;
	}

	/**
	 * This private method rendes and display the invoice list.
	 * 
	 * @return invoice list panel
	 */
	private JPanel renderInvoiceList() {

		listInvoices = new JList<Invoice>(); 

		refreshInvoices(getAllInvoicesFromDB());

		listModel = new DefaultListModel<Invoice>();
		for (Invoice invoice : invoices) {
			listModel.addElement(invoice);

		}

		JPanel invoiceListPanel = new JPanel(); 
		invoiceListPanel.setLayout(new BorderLayout());

		listInvoices.setModel(listModel);
		listInvoices.setCellRenderer(new InvoiceRenderer());
		listInvoices.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listInvoices.setBackground(Color.decode(ColorCodes.MAINBG));

		listInvoices.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();

				int firstIndex = e.getFirstIndex();
				int lastIndex = e.getLastIndex();
				boolean isAdjusting = e.getValueIsAdjusting();

				if (lsm.isSelectionEmpty() == false) {
					// Find out which indexes are selected.
					int minIndex = lsm.getMinSelectionIndex();
					int maxIndex = lsm.getMaxSelectionIndex();
					for (int i = minIndex; i <= maxIndex; i++) {
						if (lsm.isSelectedIndex(i)) {
							displayInvoice(invoices.get(i));

							Invoice invoice = listInvoices.getSelectedValue();

							InvoiceListPopupMenu ilpm = new InvoiceListPopupMenu(invoice);
							listInvoices.setComponentPopupMenu(ilpm);
							listInvoices.setInheritsPopupMenu(true);

						}
					}
				}

			}
		});

		JScrollPane invoiceListScrollPane = new JScrollPane(listInvoices);

		invoiceListScrollPane.setBackground(Color.decode(ColorCodes.MAINBG));

		invoiceListPanel.add(BorderLayout.CENTER, invoiceListScrollPane);

		invoiceListPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		invoiceListPanel.setPreferredSize(new Dimension(400, 400));

		return invoiceListPanel;
	}

	/**
	 * This private method renders a combination of the side bar panels
	 * @return left panels
	 */
	private JPanel renderLeftPanels() {

		JPanel subPanel = new JPanel();

		subPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		subPanel.setLayout(new BorderLayout());
		subPanel.add(BorderLayout.WEST, renderSideMenu());
		subPanel.add(BorderLayout.EAST, renderInvoiceList());

		return subPanel;

	}

	/**
	 * This private method renders and display the space to show details of one invoice.
	 * @return space detail panel
	 */
	private JPanel renderInvoiceDisplay() {

		int padding = 50;
		int spaceBetweenElements = 50;

		invoiceDisplay = new JPanel();
		invoiceDisplay.setLayout(new BorderLayout());
		invoiceDisplay.setBackground(Color.decode(ColorCodes.MAINBG));
		
		controller = new SwingController();

		SwingViewBuilder factory = new SwingViewBuilder(controller);

		viewerComponentPanel = factory.buildViewerPanel();


		controller.getDocumentViewController().setAnnotationCallback(
				new org.icepdf.ri.common.MyAnnotationCallback(controller.getDocumentViewController()));

		viewerComponentPanel.setVisible(false);
		invoiceDisplay.add(BorderLayout.CENTER, viewerComponentPanel);

		this.validate();
		this.repaint();

		return invoiceDisplay;
	}

	/**
	 * This private method displays the detailed view of an invoice in the right panel.
	 * @param invoice
	 */
	private void displayInvoice(Invoice invoice) {

		Actor actor = null;
		try {
			actor = ActorAccess.getActorFromInvoice(invoice);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (actor == null) {
			return;
		}

		viewerComponentPanel.setVisible(true);
		
		File pdfFile = new File("pdfdire");
		deleteDirectory(pdfFile);
		pdfFile.mkdirs();

		PDF.createPDFfromIvoice(invoice, "pdfdire\\" + invoice.getPrefixedInvoiceId() + ".pdf");

		controller.openDocument(pdfFile.getAbsolutePath() + "\\" + invoice.getPrefixedInvoiceId() + ".pdf");

		this.validate();
		this.repaint();

	}

	/**
	 * This private method returns an array list with all invoices from the database
	 * @return
	 */
	private ArrayList<Invoice> getAllInvoicesFromDB() {

		ArrayList<Invoice> allInvoices = new ArrayList<Invoice>();

		try {
			allInvoices.addAll(SupplierInvoiceAccess.getAllSupplierInvoices());
			allInvoices.addAll(CustomerInvoiceAccess.getAllCustomerInvoices());
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		return allInvoices;

	}

	/**
	 * This private method refreshes the array list and displays all invoices.
	 */
	private void showAllInvoices() {
		refreshInvoices(getAllInvoicesFromDB());
	}

	/**
	 * This private method refreshes the array list and displays all customer
	 * invoices.
	 */
	private void showAllCustomerInvoices() {
		ArrayList<Invoice> customerInvoices = new ArrayList<Invoice>();

		try {
			customerInvoices.addAll(CustomerInvoiceAccess.getAllCustomerInvoices());
			refreshInvoices(customerInvoices);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}

	/**
	 * This private method refreshes the array list and displays all supplier
	 * invoices.
	 */
	private void showAllSupplierInvoices() {
		ArrayList<Invoice> supplierInvoices = new ArrayList<Invoice>();

		try {
			supplierInvoices.addAll(SupplierInvoiceAccess.getAllSupplierInvoices());
			refreshInvoices(supplierInvoices);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}

	/**
	 * This private method displays invoices according to status.
	 * @param invoiceStatus
	 */
	private void showInvoicesFromStatus(int invoiceStatus) {
		ArrayList<Invoice> invoices = new ArrayList<Invoice>();

		try {

			if (selectedTopmenuButton == TOP_STATUS_ALLSUPPLIERINVOICES) {
				invoices.addAll(SupplierInvoiceAccess.getSupplierInvoicesByStatus(invoiceStatus));
			} else if (selectedTopmenuButton == TOP_STATUS_ALLCUSTOMERINVOICES) {

				invoices.addAll(CustomerInvoiceAccess.getCustomerInvoicesByStatus(invoiceStatus));
			} else {
				invoices.addAll(SupplierInvoiceAccess.getSupplierInvoicesByStatus(invoiceStatus));
				invoices.addAll(CustomerInvoiceAccess.getCustomerInvoicesByStatus(invoiceStatus));

			}

			refreshInvoices(invoices);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

	}

	/**
	 * This private method returns a JPanel that can be used as space between elements.
	 * @param width
	 * @param height
	 * @return space panel
	 */
	private JPanel getSpace(int width, int height) {

		JPanel space = new JPanel();

		space.setBackground(new Color(0f, 0f, 0f, .0f));
		space.setPreferredSize(new Dimension(width, height));

		return space;

	}

	/**
	 * This method deletes a directory recursively.
	 * @param directoryToBeDeleted
	 * @return
	 */
	boolean deleteDirectory(File directoryToBeDeleted) {
	    File[] allContents = directoryToBeDeleted.listFiles();
	    if (allContents != null) {
	        for (File file : allContents) {
	            deleteDirectory(file);
	        }
	    }
	    return directoryToBeDeleted.delete();
	}
}
