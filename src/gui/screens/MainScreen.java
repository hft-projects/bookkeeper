package gui.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessobjects.User;
import gui.ColorCodes;
import gui.pages.ActorPage;
import gui.pages.AnalysisPage;
import gui.pages.DashboardPage;
import gui.pages.InvoicePage;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperLogo;

/**
 * <h1>Main Screen</h1> 
 * This class provides functions to create the main screen.
 */
public class MainScreen {

	private JFrame frame;

	private User user;

	private DashboardPage dp;
	private InvoicePage ip;
	private AnalysisPage ap;
	private ActorPage acp;

	/**
	 * This method creates the main screen.
	 */
	public MainScreen(User user) {
		this.user = user;

		dp = new DashboardPage(user);
		ip = new InvoicePage();
		ap = new AnalysisPage();
		acp = new ActorPage();

		initialize();
	}

	/**
	 * This method launches the main screen.
	 */
	public void startMainScreen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainScreen window = new MainScreen(user);
					window.frame.setVisible(true);
					window.frame.setIconImage(BookkeeperLogo.getLogoIconImage().getImage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * This private method initializes the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1550, 900);
		frame.setMinimumSize(new Dimension(1550, 850));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.decode(ColorCodes.MAINBG));
		frame.getContentPane().add(renderMenubar(), BorderLayout.NORTH);
		switchPage(dp);
	}

	/**
	 * This private method rendes the menu bar.
	 * @return menu bar
	 */
	private JPanel renderMenubar() {

		JPanel mb = new JPanel();
		mb.setLayout(new BoxLayout(mb, BoxLayout.X_AXIS));
		mb.setBackground(Color.decode(ColorCodes.MAINBG));

		JPanel menuLogoPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		menuLogoPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
		menuLogoPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		menuLogoPanel.setForeground(Color.decode(ColorCodes.MAINRED));

		menuLogoPanel.add(renderLogoInMenu());

		mb.add(menuLogoPanel, BorderLayout.WEST);

		JPanel menuLinksPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		menuLinksPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		menuLinksPanel.setBorder(new EmptyBorder(30, 30, 30, 30));

		JButton bDashboard = BookkeeperButtons.renderFlatButton("Dashboard", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPage(dp);
				dp.updateValues();

			}
		});

		menuLinksPanel.add(bDashboard);

		JButton bInvoices = BookkeeperButtons.renderFlatButton("Rechnungen", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPage(ip);

			}
		});
		menuLinksPanel.add(bInvoices);

		JButton bActor = BookkeeperButtons.renderFlatButton("Kunden/Lieferanten", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPage(acp);

			}
		});
		menuLinksPanel.add(bActor);
		
		JButton bAnalysis = BookkeeperButtons.renderFlatButton("Auswertung", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPage(ap);

			}
		});
		menuLinksPanel.add(bAnalysis);

		JButton bLogout = BookkeeperButtons.renderNormalButton("Logout", new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		menuLinksPanel.add(bLogout);

		mb.add(menuLinksPanel, BorderLayout.EAST);

		return mb;
	}

	/**
	 * This private method renders the logo in menu bar.
	 * @return logo
	 */
	private JPanel renderLogoInMenu() {
		JPanel logoPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JButton logoButton = new JButton();
		logoButton.setSize(166, 66);
		logoButton.setBackground(Color.decode(ColorCodes.MAINBG));
		logoButton.setBorderPainted(false);
		logoButton.setFocusPainted(false);
		Image scaledLogo = BookkeeperLogo.getLogoImage().getImage().getScaledInstance(logoButton.getWidth(),
				logoButton.getHeight(), Image.SCALE_SMOOTH);
		logoButton.setIcon(new ImageIcon(scaledLogo));
		logoButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switchPage(dp);

			}
		});
		logoButton.setVisible(true);
		logoPanel.add(logoButton);
		logoPanel.setSize(50, 80);
		logoPanel.setBackground(Color.decode(ColorCodes.MAINBG));

		return logoPanel;
	}

	/**
	 * This private method removves all pages from main screen.
	 */
	private void removeAllPages() {
		frame.remove(dp);
		frame.remove(ip);
		frame.remove(ap);
		frame.remove(acp);
	}

	/**
	 * This private method displays a page on the main screen.
	 * @param page
	 */
	private void switchPage(JPanel page) {
		removeAllPages();
		frame.getContentPane().add(page, BorderLayout.CENTER);
		frame.validate();
		frame.repaint();
	}
}
