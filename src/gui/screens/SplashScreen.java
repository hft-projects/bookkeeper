package gui.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.border.EmptyBorder;

import gui.ColorCodes;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperLogo;
import javax.swing.SwingConstants;

/**
 * <h1>Splash Screen</h1> 
 * This class provides functions to create the splash screen.
 */
public class SplashScreen {
	private JWindow frame;
	private JLabel lblError;
	private JLabel lblLoading;
	private JLabel lblLogo;
	private JPanel contentPanel;
	
	private JPanel errorDisplay;

	/**
	 * This method creates the splash screen.
	 */
	private void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame.setVisible(true);
					frame.setIconImage(BookkeeperLogo.getLogoIconImage().getImage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * This method closes the splash screen.
	 */
	public void close() {
		frame.setVisible(false);
	}

	/**
	 * This method creates the splash screen.
	 */
	public SplashScreen() {
		initialize();
		start();
	}

	/**
	 * This private method initializes the contents of the frame.
	 */
	private void initialize() {
		frame = new JWindow();
		frame.setAlwaysOnTop(true);
		frame.setSize(220, 400);
		frame.setLocationRelativeTo(null);

		contentPanel = renderSplashScreenContent();
		
		frame.getContentPane().add(contentPanel);
		frame.pack();

	}

	/**
	 * This private method renders the splash screen content.
	 * 
	 * @return splash screen content panel
	 */
	private JPanel renderSplashScreenContent() {

		JPanel ssContent = new JPanel(new BorderLayout());
		ssContent.setBounds(100, 100, 450, 300);
		ssContent.setBackground(Color.decode(ColorCodes.MAINBG));
		ssContent.setBorder(new EmptyBorder(50, 60, 50, 50));

		lblLogo = new JLabel();
		lblLogo.setVerticalAlignment(SwingConstants.TOP);
		lblLogo.setSize(250, 100);
		lblLogo.setBackground(Color.decode(ColorCodes.MAINBG));

		Image scaledLogo = BookkeeperLogo.getLogoImage().getImage().getScaledInstance(lblLogo.getWidth(), lblLogo.getHeight(),
				Image.SCALE_SMOOTH);
		lblLogo.setIcon(new ImageIcon(scaledLogo));

		lblLogo.setSize(150, 150);
		lblLogo.setBackground(Color.decode(ColorCodes.MAINBG));
		lblLogo.setAlignmentX(Component.CENTER_ALIGNMENT);

		Icon imageLoading = new ImageIcon(this.getClass().getResource("/assets/loading.gif"));
		lblLoading = new JLabel(imageLoading);

		ssContent.add(BorderLayout.NORTH, lblLogo);
		ssContent.add(BorderLayout.SOUTH, lblLoading);

		errorDisplay = new JPanel();
		errorDisplay.setVisible(false);
		errorDisplay.setBackground(Color.decode(ColorCodes.MAINBG));
		ssContent.add(errorDisplay);

		ssContent.setAlignmentX(Component.CENTER_ALIGNMENT);

		return ssContent;
	}

	/**
	 * This method displays an error message and the option to close the programm.
	 * @param errorMessage
	 */
	public void showError(String errorMessage) {

		lblLogo.setVisible(false);
		lblLoading.setVisible(false);	

		errorDisplay.setVisible(true);
		
		lblError = new JLabel(" ");
		lblError.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		lblError.setForeground(Color.decode(ColorCodes.MAINRED));
		lblError.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		JButton bClose = BookkeeperButtons.renderNormalSelectedButton("Close", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(1); // Exit Application

			}
		});
		
		System.out.println(errorMessage);
		lblError.setText(errorMessage);
		
		errorDisplay.add(lblError);
		errorDisplay.add(bClose);

		frame.validate();
		frame.pack();
	}
}
