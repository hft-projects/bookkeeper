package gui.screens;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import businessobjects.User;
import dbaccess.LoginAccess;
import gui.ColorCodes;
import gui.utils.BookkeeperLogo;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
/**
 * <h1>Login Screen</h1> 
 * This class provides functions to create the login screen.
 */
public class LoginScreen {

	private JFrame frame;

	private JTextField textUsername;
	private JPasswordField textPassword;

	private JLabel lblLogin;
	private JLabel lblWelcome;
	private JLabel lblUsername;
	private JLabel lblPassword;
	private JLabel lblError;

	private JButton bLogin;

	/**
	 * This method launches the login screen.
	 */
	public static void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginScreen window = new LoginScreen();
					window.frame.setVisible(true);
					window.frame.setIconImage(BookkeeperLogo.getLogoIconImage().getImage());

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * This method creates the login screen.
	 */
	public LoginScreen() {
		initialize();
	}

	/**
	 * This private method initializes the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JPanel panel = new JPanel();
		panel.setForeground(Color.DARK_GRAY);
		panel.setBorder(new EmptyBorder(100, 100, 100, 100));
		panel.setBackground(Color.WHITE);

		lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 50));
		lblLogin.setForeground(Color.decode(ColorCodes.MAINRED));
		lblLogin.setBorder(new EmptyBorder(10, 10, 10, 10));

		lblWelcome = new JLabel("Herzlich Willkommen bei BookKeeper");
		lblWelcome.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 30));
		lblWelcome.setForeground(Color.decode(ColorCodes.MAINBG));
		lblWelcome.setBorder(new EmptyBorder(10, 10, 40, 10));

		lblUsername = new JLabel("Benutzername");
		lblUsername.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		lblUsername.setForeground(Color.decode(ColorCodes.MAINBG));
		lblUsername.setBorder(new EmptyBorder(10, 10, 10, 10));

		lblPassword = new JLabel("Passwort");
		lblPassword.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		lblPassword.setForeground(Color.decode(ColorCodes.MAINBG));
		lblPassword.setBorder(new EmptyBorder(10, 10, 10, 10));

		textUsername = new JTextField();
		textUsername.setForeground(Color.decode(ColorCodes.MAINRED));
		textUsername.setBackground(Color.decode(ColorCodes.MAINBG));
		textUsername.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textUsername.setBorder(new EmptyBorder(20, 20, 20, 20));
		textUsername.setColumns(40);

		textPassword = new JPasswordField();
		textPassword.setForeground(new Color(239, 37, 61));
		textPassword.setBackground(Color.decode(ColorCodes.MAINBG));
		textPassword.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textPassword.setBorder(new EmptyBorder(20, 20, 20, 20));
		textPassword.setColumns(40);
		textPassword.setEchoChar('*');
		textPassword.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {

					onClickLoginButton();

					;
				}
			}
		});

		bLogin = new JButton();
		bLogin.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				onClickLoginButton();

			}
		});

		bLogin.setText("Login");
		bLogin.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		bLogin.setBorder(new EmptyBorder(20, 60, 20, 60));
		bLogin.setBackground(Color.decode(ColorCodes.MAINRED));
		bLogin.setForeground(Color.WHITE);

		lblError = new JLabel(" ");
		lblError.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		lblError.setForeground(Color.decode(ColorCodes.MAINRED));
		lblError.setBorder(new EmptyBorder(10, 10, 10, 10));
		lblError.setVisible(false);

		panel.add(lblLogin);
		panel.add(lblWelcome);
		panel.add(lblUsername);
		panel.add(textUsername);
		panel.add(lblPassword);
		panel.add(textPassword);
		panel.add(lblError);
		panel.add(bLogin);

		panel.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.setAlignmentY(Component.CENTER_ALIGNMENT);
		panel.setPreferredSize(new Dimension(600, 600));
		panel.setMaximumSize(new Dimension(600, 600));
		panel.setMinimumSize(new Dimension(600, 600));

		frame.getContentPane().add(panel);

		frame.setBackground(Color.decode(ColorCodes.MAINBG));
		frame.setBounds(100, 100, 1262, 751);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * This private method handles the event of clicking on the login button.
	 */
	private void onClickLoginButton() {
		try {

			User currentUser = LoginAccess.loginUser(textUsername.getText(), textPassword.getPassword());
			if (currentUser == null) {
				System.out.println("Der Benutzername oder das Passwort ist falsch");
				lblError.setText("Der Benutzername oder das Passwort ist falsch");
				lblError.setVisible(true);

			} else {
				System.out.println("Angemeldet als " + currentUser.getName());
				lblError.setVisible(false);
				MainScreen ms = new MainScreen(currentUser);
				ms.startMainScreen();
				frame.setVisible(false);

			}

		} catch (SQLException exp) {
			exp.printStackTrace();
		}
	}

}
