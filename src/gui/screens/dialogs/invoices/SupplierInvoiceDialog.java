package gui.screens.dialogs.invoices;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import com.toedter.calendar.JDateChooser;

import businessobjects.InvoicePosition;
import businessobjects.Supplier;
import businessobjects.SupplierInvoice;
import dbaccess.InvoicePositionsAccess;
import dbaccess.SupplierAccess;
import dbaccess.SupplierInvoiceAccess;
import gui.ColorCodes;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperLogo;
import gui.utils.BookkeeperTextfields;
import services.PositionLogic;
import services.StatusHelper;

/**
 * <h1>Supplier Invoice Dialog</h1> 
 * This class provides functions to create the dialog to create new supplier invoices.
 */
public class SupplierInvoiceDialog extends JDialog {

	private JComboBox<Supplier> supplierComboBox;
	private JComboBox<String> statusComboBox;
	private JComboBox<String> comboBoxTax;

	private JTextField textDiscount;
	private JTextField textInvoiceNr;

	private JDateChooser tfIssueDate;
	private JDateChooser tfDueDate;
	private JDateChooser tfDeliveryDate;

	private JTable positionsTable;

	private JLabel lblTitel;

	private JLabel lblTotal;
	private JLabel lblTotalWithTaxAndBoni;
	private JLabel lblTaxNr;

	private JButton btnAddSupplierInvoice;

	private SupplierInvoice toEditInvoice;

	private Dialogmode mode;

	public enum Dialogmode {
		Create, Edit;
	};

	/**
	 * This method creates the dialog.
	 */
	public SupplierInvoiceDialog() {

		setModal(true);
		setBackground(Color.decode(ColorCodes.MAINBG));
		setLayout(new BorderLayout());
		
		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());

		mode = Dialogmode.Create;
		
		setLocationRelativeTo(null);
		
		setMinimumSize(new Dimension(1000,1000));

		add(BorderLayout.PAGE_START, renderTopContent());
		add(BorderLayout.CENTER, renderForm());
		add(BorderLayout.PAGE_END, renderBottomContent());

		initializeValues();
	}

	/**
	 * This method creates the dialog when editing the supplier invoices.
	 * 
	 * @param toEditInvoice
	 */
	public SupplierInvoiceDialog(SupplierInvoice toEditInvoice) {

		setModal(true);
		setBackground(Color.decode(ColorCodes.MAINBG));
		setLayout(new BorderLayout());

		mode = Dialogmode.Edit;

		setLocationRelativeTo(null);
		
		this.toEditInvoice = toEditInvoice;

		add(BorderLayout.PAGE_START, renderTopContent());
		add(BorderLayout.CENTER, renderForm());
		add(BorderLayout.PAGE_END, renderBottomContent());

		initializeValues();

		try {
			for (int i = 0; i < supplierComboBox.getItemCount(); i++) {

				int selectedSupplierID = supplierComboBox.getItemAt(i).getSupplierId();

				if (selectedSupplierID == toEditInvoice.getActorId()) {
					supplierComboBox.setSelectedIndex(i);
				}
			}

			statusComboBox.setSelectedItem(StatusHelper.getStringFromStatus(toEditInvoice.getStatus()));
			comboBoxTax.setSelectedItem(toEditInvoice.getTaxRate());

			textDiscount.setText(String.valueOf(toEditInvoice.getDiscount()));
			textInvoiceNr.setText(String.valueOf(toEditInvoice.getExternalInvoiceId()));
			
			Supplier selectedSupplier = (Supplier)supplierComboBox.getSelectedItem();
			lblTaxNr.setText("USt-IdNr.: "+String.valueOf(selectedSupplier.getTaxNumber()));

			tfIssueDate.setDate(toEditInvoice.getIssueDate());
			tfDueDate.setDate(toEditInvoice.getDueDate());
			tfDeliveryDate.setDate(toEditInvoice.getDeliveryDate());

			DefaultTableModel model = (DefaultTableModel) positionsTable.getModel();
			for (InvoicePosition ip : InvoicePositionsAccess
					.getAllInvoicePositions(toEditInvoice.getPrefixedInvoiceId())) {
				Object[] newRow = { ip.getArticleNr(), ip.getArticleDesc(), ip.getQuantity(),
						ip.getPrice().setScale(2, RoundingMode.FLOOR) };

				model.addRow(newRow);

			}
			positionsTable.setModel(model);

			validate();
			pack();

			lblTitel.setText("Eingangsrechnung bearbeiten");
			btnAddSupplierInvoice.setText("Rechnung bearbeiten");

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * This private method renders the top content.
	 * 
	 * @return top content panel
	 */
	private JPanel renderTopContent() {

		int padding = 50;

		JPanel topContent = new JPanel();
		topContent.setBackground(getBackground());
		topContent.setBorder(new EmptyBorder(padding, padding, padding, padding));

		lblTitel = new JLabel("Eingangsrechnung anlegen");
		lblTitel.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 40));
		topContent.add(lblTitel);

		return topContent;

	}

	/**
	 * This private method rendes the form.
	 * 
	 * @return form panel
	 */
	private JPanel renderForm() {

		int padding = 20;
		int labelTextSize = 17;
		int spaceBetweenElements = 30;
		Dimension preferedComponentSize = new Dimension(400, 30);
		Border FormSidePadding = new EmptyBorder(20, 20, 20, 20);

		JPanel mainForm = new JPanel(new BorderLayout());
		mainForm.setBackground(getBackground());
		mainForm.setBorder(new EmptyBorder(padding, padding, padding, padding));

		// Left side of formular
		JPanel leftFormSide = new JPanel(new GridBagLayout());
		leftFormSide.setBackground(mainForm.getBackground());
		leftFormSide.setBorder(FormSidePadding);

		GridBagConstraints cl = new GridBagConstraints();
		cl.gridx = 0;
		cl.anchor = GridBagConstraints.WEST;

		cl.gridy = 0;

		JLabel lblInvoiceNr = new JLabel("Rechnungsnummer:");
		lblInvoiceNr.setForeground(Color.WHITE);
		lblInvoiceNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		leftFormSide.add(lblInvoiceNr, cl);

		cl.gridy = cl.gridy++;
		leftFormSide.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		textInvoiceNr = BookkeeperTextfields.renderNormalTextfield();
		textInvoiceNr.setPreferredSize(preferedComponentSize);
		leftFormSide.add(textInvoiceNr, cl);

		cl.gridy = cl.gridy++;
		leftFormSide.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		JLabel lblLieferant = new JLabel("Lieferant:");
		lblLieferant.setForeground(Color.WHITE);
		lblLieferant.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		leftFormSide.add(lblLieferant, cl);

		cl.gridy = cl.gridy++;
		leftFormSide.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		supplierComboBox = new JComboBox<Supplier>();
		supplierComboBox.setBackground(Color.WHITE);
		supplierComboBox.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		supplierComboBox.setPreferredSize(preferedComponentSize);
		supplierComboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Supplier selectedSupplier = (Supplier)supplierComboBox.getSelectedItem();
				lblTaxNr.setText("USt-IdNr.: "+ selectedSupplier.getTaxNumber());
				
			}
		});
		leftFormSide.add(supplierComboBox, cl);

		cl.gridy++;
		lblTaxNr = new JLabel("USt-IdNr.:");
		lblTaxNr.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTaxNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		leftFormSide.add(lblTaxNr, cl);

		cl.gridy = cl.gridy++;
		leftFormSide.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setForeground(Color.WHITE);
		lblStatus.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		leftFormSide.add(lblStatus, cl);

		cl.gridy = cl.gridy++;
		leftFormSide.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		statusComboBox = new JComboBox<String>();
		statusComboBox.setModel(new DefaultComboBoxModel<String>(
				new String[] { "In Bearbeitung", "Bezahlt", "Storniert", "Überfällig", "Offen" }));
		statusComboBox.setBackground(Color.WHITE);
		statusComboBox.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		statusComboBox.setPreferredSize(preferedComponentSize);
		leftFormSide.add(statusComboBox, cl);

		// Right side of formular
		JPanel rightFormSide = new JPanel(new GridBagLayout());
		rightFormSide.setBackground(mainForm.getBackground());
		rightFormSide.setBorder(FormSidePadding);

		GridBagConstraints cr = new GridBagConstraints();
		cr.gridx = 0;
		cr.anchor = GridBagConstraints.WEST;

		cr.gridy = 0;

		cr.gridy++;
		JLabel lblIssueDate = new JLabel("Ausstellungsdatum:");
		lblIssueDate.setForeground(Color.WHITE);
		lblIssueDate.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		rightFormSide.add(lblIssueDate, cr);

		cr.gridy = cr.gridy++;
		rightFormSide.add(getSpace(0, spaceBetweenElements), cr);

		cr.gridy++;
		tfIssueDate = new JDateChooser();
		tfIssueDate.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		tfIssueDate.setBackground(Color.WHITE);
		tfIssueDate.setForeground(Color.decode(ColorCodes.MAINBG));
		tfIssueDate.setPreferredSize(preferedComponentSize);
		rightFormSide.add(tfIssueDate, cr);

		cr.gridy = cr.gridy++;
		rightFormSide.add(getSpace(0, spaceBetweenElements), cr);

		cr.gridy++;
		JLabel lblDueDate = new JLabel("Fälligkeitsdatum:");
		lblDueDate.setForeground(Color.WHITE);
		lblDueDate.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		rightFormSide.add(lblDueDate, cr);

		cr.gridy = cr.gridy++;
		rightFormSide.add(getSpace(0, spaceBetweenElements), cr);

		cr.gridy++;
		tfDueDate = new JDateChooser();
		tfDueDate.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		tfDueDate.setBackground(Color.WHITE);
		tfDueDate.setForeground(Color.decode(ColorCodes.MAINBG));
		tfDueDate.setPreferredSize(preferedComponentSize);
		rightFormSide.add(tfDueDate, cr);

		cr.gridy++;
		JLabel lblDeliveryDate = new JLabel("Lieferungsdatum:");
		lblDeliveryDate.setForeground(Color.WHITE);
		lblDeliveryDate.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		rightFormSide.add(lblDeliveryDate, cr);

		cr.gridy = cr.gridy++;
		rightFormSide.add(getSpace(0, spaceBetweenElements), cr);

		cr.gridy++;
		tfDeliveryDate = new JDateChooser();
		tfDeliveryDate.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		tfDeliveryDate.setBackground(Color.WHITE);
		tfDeliveryDate.setForeground(Color.decode(ColorCodes.MAINBG));
		tfDeliveryDate.setPreferredSize(preferedComponentSize);
		rightFormSide.add(tfDeliveryDate, cr);

		// Postions
		JPanel bottomOfForm = new JPanel(new GridBagLayout());
		bottomOfForm.setBackground(mainForm.getBackground());
		bottomOfForm.setBorder(FormSidePadding);

		GridBagConstraints cb = new GridBagConstraints();
		cb.gridx = 0;
		cb.anchor = GridBagConstraints.WEST;

		cb.gridy = 0;
		JLabel lblPositions = new JLabel("Positionen:");
		lblPositions.setForeground(Color.WHITE);
		lblPositions.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		bottomOfForm.add(lblPositions, cb);

		cb.gridy = cb.gridy++;
		bottomOfForm.add(getSpace(0, spaceBetweenElements), cb);

		cb.gridy++;
		JPanel positionsPanel = new JPanel();

		cb.gridy++;
		positionsTable = new JTable();
		positionsTable.setCellSelectionEnabled(true);
		positionsTable.setModel(new DefaultTableModel(new Object[][] {

		}, new String[] { "Artikelnr.", "Beschreibung", "Menge", "Preis / Stk." }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
		positionsTable.getColumnModel().getColumn(0).setPreferredWidth(90);
		positionsTable.getColumnModel().getColumn(1).setPreferredWidth(510);
		positionsTable.getColumnModel().getColumn(2).setPreferredWidth(100);
		positionsTable.getColumnModel().getColumn(3).setPreferredWidth(100);

		positionsTable.setPreferredScrollableViewportSize(new Dimension(500, 70));

		positionsTable.setBackground(Color.WHITE);
		positionsTable.setForeground(Color.decode(ColorCodes.MAINBG));
		positionsTable.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));

		positionsTable.getModel().addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				// TODO Auto-generated method stub
				refreshTotalPrice();
			}
		});

		JTableHeader tableHeader = positionsTable.getTableHeader();
		tableHeader.setBackground(Color.decode(ColorCodes.MAINBG));
		tableHeader.setForeground(Color.WHITE);
		tableHeader.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 13));

		JScrollPane tableScrollPane = new JScrollPane(positionsTable);
		tableScrollPane.setPreferredSize(new Dimension(800, 80));
		tableScrollPane.setBackground(Color.WHITE);
		bottomOfForm.add(tableScrollPane, cb);

		cb.gridy = cb.gridy++;
		bottomOfForm.add(getSpace(0, spaceBetweenElements), cb);

		cb.gridy++;
		JPanel tableControls = renderPostionTableControls();
		tableControls.setBackground(mainForm.getBackground());
		bottomOfForm.add(tableControls, cb);

		cb.gridy = cb.gridy++;
		bottomOfForm.add(getSpace(0, spaceBetweenElements), cb);

		cb.gridy++;
		lblTotal = new JLabel("Rechnungsbetrag(Netto): ");
		lblTotal.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTotal.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		bottomOfForm.add(lblTotal, cb);

		// Tax rate and discount

		cb.gridy++;
		bottomOfForm.add(getSpace(0, spaceBetweenElements), cb);

		cb.gridy++;
		JLabel lblTax = new JLabel("Anzuwendender Steuersatz (in %): ");
		lblTax.setForeground(Color.WHITE);
		lblTax.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		bottomOfForm.add(lblTax, cb);

		cb.gridy++;
		comboBoxTax = new JComboBox<String>();
		comboBoxTax.setModel(new DefaultComboBoxModel<String>(new String[] { "19", "7" }));
		comboBoxTax.setBackground(Color.WHITE);
		comboBoxTax.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		comboBoxTax.setPreferredSize(new Dimension(800, 35));
		comboBoxTax.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				refreshTotalPrice();

			}
		});
		bottomOfForm.add(comboBoxTax, cb);

		cb.gridy++;
		bottomOfForm.add(getSpace(0, spaceBetweenElements), cb);

		cb.gridy = cb.gridy++;
		JLabel lblDiscount = new JLabel("Eingeräumte Rabatte, Skonti, Boni (in %): ");
		lblDiscount.setForeground(Color.WHITE);
		lblDiscount.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		bottomOfForm.add(lblDiscount, cb);

		cb.gridy++;
		textDiscount = BookkeeperTextfields.renderNormalTextfield();
		textDiscount.setPreferredSize(new Dimension(800, 35));
		textDiscount.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				refreshTotalPrice();
			}

			@Override
			public void keyReleased(KeyEvent e) {
				refreshTotalPrice();

			}

			@Override
			public void keyPressed(KeyEvent e) {
				refreshTotalPrice();

			}
		});
		bottomOfForm.add(textDiscount, cb);

		cb.gridy++;
		bottomOfForm.add(getSpace(0, spaceBetweenElements), cb);

		cb.gridy++;
		lblTotalWithTaxAndBoni = new JLabel("Rechnungsbetrag(Brutto + Boni): ");
		lblTotalWithTaxAndBoni.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTotalWithTaxAndBoni.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		bottomOfForm.add(lblTotalWithTaxAndBoni, cb);

		mainForm.add(BorderLayout.WEST, leftFormSide);
		mainForm.add(BorderLayout.CENTER, rightFormSide);
		mainForm.add(BorderLayout.SOUTH, bottomOfForm);

		return mainForm;

	}

	/**
	 * This private method renders the postions table controls
	 * 
	 * @return positions controls panel
	 */
	private JPanel renderPostionTableControls() {

		JPanel panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		panel.setPreferredSize(new Dimension(800, 50));

		JButton btnAddPosition = BookkeeperButtons.renderNormalButton("Position hinzufügen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AddPostionDialog apd = new AddPostionDialog(panel);
				apd.showDialog();
				InvoicePosition newPosition = apd.getNewPosition();

				if (newPosition != null) {
					Object[] newRow = { newPosition.getArticleNr(), newPosition.getArticleDesc(),
							newPosition.getQuantity(), newPosition.getPrice().setScale(2, RoundingMode.FLOOR) };
					DefaultTableModel model = (DefaultTableModel) positionsTable.getModel();

					model.addRow(newRow);
					positionsTable.setModel(model);

					validate();
					pack();

				}
			}
		});
		btnAddPosition.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel.add(btnAddPosition);

		JButton btnEditPostion = BookkeeperButtons.renderNormalButton("Position bearbeiten", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				DefaultTableModel model = (DefaultTableModel) positionsTable.getModel();
				if (positionsTable.getSelectedRow() > -1) {

					int artikelnr = Integer.valueOf(model.getValueAt(positionsTable.getSelectedRow(), 0).toString());
					String desc = model.getValueAt(positionsTable.getSelectedRow(), 1).toString();
					int quantiy = Integer.valueOf(model.getValueAt(positionsTable.getSelectedRow(), 2).toString());
					BigDecimal price = new BigDecimal(model.getValueAt(positionsTable.getSelectedRow(), 3).toString());

					InvoicePosition currentPosition = new InvoicePosition("0", artikelnr, desc, quantiy, price);

					AddPostionDialog apd = new AddPostionDialog(panel, currentPosition);
					apd.showDialog();
					InvoicePosition newPosition = apd.getNewPosition();

					model.removeRow(positionsTable.getSelectedRow());

					if (newPosition != null) {
						Object[] newRow = { newPosition.getArticleNr(), newPosition.getArticleDesc(),
								newPosition.getQuantity(), newPosition.getPrice().setScale(2, RoundingMode.FLOOR) };
						model.addRow(newRow);
						positionsTable.setModel(model);

						validate();
						pack();
					}
				}

			}
		});
		btnEditPostion.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel.add(btnEditPostion);

		JButton btnDeletePosition = BookkeeperButtons.renderNormalButton("Position entfernen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				DefaultTableModel model = (DefaultTableModel) positionsTable.getModel();
				if (positionsTable.getSelectedRow() > -1) {
					model.removeRow(positionsTable.getSelectedRow());

					positionsTable.setModel(model);

					validate();
					pack();
				}

			}
		});
		btnDeletePosition.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel.add(btnDeletePosition);

		return panel;
	}

	/**
	 * This private method rendes the bottom content
	 * 
	 * @return bottom content panel
	 */
	private JPanel renderBottomContent() {

		JPanel bottomContent = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		bottomContent.setBackground(getBackground());

		btnAddSupplierInvoice = BookkeeperButtons.renderNormalButton("Rechnung hinzufügen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				onClickSave();
			}
		});

		bottomContent.add(btnAddSupplierInvoice);

		return bottomContent;

	}

	/**
	 * This private method adds supplier invoices when clicking on button.
	 */
	private void onClickSave() {

		String externalInvoiceNumber = textInvoiceNr.getText();
		if (externalInvoiceNumber == null || externalInvoiceNumber.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Bitte geben sie die Rechnungsnummer erneut ein.");
			textInvoiceNr.setText("");
			return;
		}

		Supplier supplier = (Supplier) supplierComboBox.getSelectedItem();
		if (supplier == null) {
			JOptionPane.showMessageDialog(this, "Wählen Sie einen Lieferanten aus.");
			supplierComboBox.getSelectedItem();
			return;
		}

		Date issueDate = tfIssueDate.getDate();
		if (issueDate == null) {
			JOptionPane.showMessageDialog(this, "Wählen Sie ein Ausstellungsdatum aus.");
			tfIssueDate.getDate();
			return;
		}

		int status = StatusHelper.getStatusFromString((String) statusComboBox.getSelectedItem());

		Date dueDate = tfDueDate.getDate();
		if (dueDate == null) {
			JOptionPane.showMessageDialog(this, "Wählen Sie ein Fälligkeitssdatum aus.");
			tfDueDate.getDate();
			return;
		}

		Date deliveryDate = tfDeliveryDate.getDate();
		if (deliveryDate == null) {
			JOptionPane.showMessageDialog(this, "Wählen Sie ein Lieferdatum aus.");
			tfDeliveryDate.getDate();
			return;
		}

		int taxRate = Integer.valueOf((String) comboBoxTax.getSelectedItem());

		String onlyNumbersPattern = "\\d+";

		if (textDiscount.getText().matches(onlyNumbersPattern)) {

		} else if (textDiscount.getText().isEmpty() || textDiscount.getText() == " "){
			textDiscount.setText("0");
			}else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie bei den Rabatten nur Zahlen ein.");
			// textDiscount.getValue();
			return;
		}

		int discount = Integer.valueOf(textDiscount.getText());
		SupplierInvoice invoice;
		try {
			if (mode == Dialogmode.Create) {

				invoice = new SupplierInvoice(externalInvoiceNumber, supplier.getSupplierId(), status,
						issueDate, dueDate, deliveryDate, taxRate, discount);

				SupplierInvoiceAccess.addSupplierInvoice(invoice);
			} else {

				invoice = new SupplierInvoice(toEditInvoice.getInvoiceId(), externalInvoiceNumber,
						supplier.getSupplierId(), status, issueDate, dueDate, deliveryDate, taxRate, discount);

				SupplierInvoiceAccess.updateSupplierInvoice(invoice);

			}

			List<InvoicePosition> positions = new ArrayList<InvoicePosition>();

			DefaultTableModel model = (DefaultTableModel) positionsTable.getModel();

			if (mode == Dialogmode.Edit)
				InvoicePositionsAccess.deletePositions(toEditInvoice.getPrefixedInvoiceId());

			for (int count = 0; count < positionsTable.getRowCount(); count++) {

				int artikelnr = Integer.valueOf(model.getValueAt(count, 0).toString());
				String desc = model.getValueAt(count, 1).toString();
				int quantiy = Integer.valueOf(model.getValueAt(count, 2).toString());
				BigDecimal price = new BigDecimal(model.getValueAt(count, 3).toString());

				InvoicePosition currentPosition = new InvoicePosition(invoice.getPrefixedInvoiceId(), artikelnr, desc,
						quantiy, price);

				positions.add(currentPosition);
			}

			InvoicePositionsAccess.addPositions(invoice, positions);

			setVisible(false);
			dispose();

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this,
					"Es ist ein Fehler mit der Datenbank aufgetreten. Bitte versuche es später erneut!");
			e.printStackTrace();
		}

	}

	/**
	 * This private method initializes the value, for example the combo box.
	 */
	private void initializeValues() {
		List<Supplier> suppliers;

		try {
			suppliers = SupplierAccess.getAllSuppliers();

		} catch (SQLException e) {
			suppliers = new ArrayList<Supplier>();
			e.printStackTrace();
		}

		for (Supplier supplier : suppliers) {
			supplierComboBox.addItem(supplier);
		}

	}

	/**
	 * This private method displays the current sum of all positions
	 */
	private void refreshTotalPrice() {

		BigDecimal price = getTotalPrice();
		lblTotal.setText("Rechnungsbetrag(Netto): " + price.setScale(2, RoundingMode.FLOOR) + "€");

		String bonivalue = textDiscount.getText();
		String pattern = "\\d+";

		if (bonivalue.matches(pattern)) {
			BigDecimal boniprice = PositionLogic.getDiscountAmount(price, Integer.valueOf(bonivalue));
			price = price.subtract(boniprice);
		} else {

		}

		BigDecimal taxprice = PositionLogic.getTaxAmount(price,
				Integer.valueOf(comboBoxTax.getSelectedItem().toString()));
		price = price.add(taxprice);
		lblTotalWithTaxAndBoni
				.setText("Rechnungsbetrag(Brutto + Boni): " + price.setScale(2, RoundingMode.FLOOR) + "€");

	}

	/**
	 * This private method sums up all position prices.
	 * 
	 * @return total amount
	 */
	private BigDecimal getTotalPrice() {

		BigDecimal total = new BigDecimal(0);
		DefaultTableModel model = (DefaultTableModel) positionsTable.getModel();

		for (int count = 0; count < positionsTable.getRowCount(); count++) {

			BigDecimal currentPrice = new BigDecimal(model.getValueAt(count, 3).toString());
			total = total.add(currentPrice.multiply(new BigDecimal(model.getValueAt(count, 2).toString())));
		}

		return total;
	}

	/**
	 * This private method returns a JPanel that can be used as space between elements.
	 * 
	 * @param width
	 * @param height
	 * @return space panel
	 */
	private JPanel getSpace(int width, int height) {

		JPanel space = new JPanel();

		space.setBackground(new Color(0f, 0f, 0f, .0f));
		space.setPreferredSize(new Dimension(width, height));

		return space;
	}

	/**
	 * This method shows the dialog.
	 */
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
}
