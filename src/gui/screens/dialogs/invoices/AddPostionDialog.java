package gui.screens.dialogs.invoices;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

import businessobjects.InvoicePosition;
import gui.ColorCodes;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperTextfields;

/**
 * <h1>Add Positions Dialog</h1> 
 * This class provides functions to create the dialog to add new invoice positions.
 */
public class AddPostionDialog extends JDialog {

	private InvoicePosition newPosition;

	private JFormattedTextField textPrice = new JFormattedTextField();;
	private JTextField textArticleNr = new JTextField();;
	private JTextField textDiscription = new JTextField();;
	private JTextField textAmount = new JTextField();

	private JButton bAdd = new JButton();

	/**
	 * This method creates the dialog.
	 * @param cForPostion
	 */
	public AddPostionDialog(Component cForPostion) {

		setModal(true);

		setLocationRelativeTo(cForPostion);

		setBackground(Color.decode(ColorCodes.MAINBG));
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, renderDialog());
		
		setMinimumSize(new Dimension(400,500));

		newPosition = null;

		textPrice.setValue(new Double(0.0));

	}

	/**
	 * This method creates the dialog when editing the positions.
	 * @param cForPostion
	 * @param editPosition
	 */
	public AddPostionDialog(Component cForPostion, InvoicePosition editPosition) {

		setModal(true);

		setLocationRelativeTo(cForPostion);

		newPosition = editPosition;

		setBackground(Color.decode(ColorCodes.MAINBG));
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, renderDialog());

		textAmount.setText("" + editPosition.getQuantity());
		textDiscription.setText(editPosition.getArticleDesc());
		textPrice.setText("" + editPosition.getPrice());
		textArticleNr.setText("" + editPosition.getArticleNr());

		bAdd.setText("Position bearbeiten");

	}

	/**
	 * This method renders the content of the dialog.
	 * 
	 * @return content panel
	 */
	private JPanel renderDialog() {

		JPanel content = new JPanel();
		content.setLayout(new GridBagLayout());
		content.setBackground(getBackground());
		content.setBorder(new EmptyBorder(50, 50, 50, 50));

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.anchor = GridBagConstraints.WEST;

		c.gridy = 0;
		JLabel lblTitel = new JLabel("Position anlegen");
		lblTitel.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 40));
		content.add(lblTitel, c);

		c.gridy++;
		JLabel lblArticleNr = new JLabel("Artikel Nummer:");
		lblArticleNr.setForeground(Color.WHITE);
		lblArticleNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		content.add(lblArticleNr, c);

		c.gridy++;
		textArticleNr = BookkeeperTextfields.renderNormalTextfield();
		textArticleNr.setPreferredSize(new Dimension(300, 50));
		content.add(textArticleNr, c);

		c.gridy++;
		JLabel lblDiscription = new JLabel("Beschreibung:");
		lblDiscription.setForeground(Color.WHITE);
		lblDiscription.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		content.add(lblDiscription, c);

		c.gridy++;
		textDiscription = BookkeeperTextfields.renderNormalTextfield();
		textDiscription.setPreferredSize(new Dimension(300, 50));
		content.add(textDiscription, c);

		c.gridy++;
		JLabel lblAmount = new JLabel("Menge:");
		lblAmount.setForeground(Color.WHITE);
		lblAmount.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		content.add(lblAmount, c);

		c.gridy++;
		textAmount = BookkeeperTextfields.renderNormalTextfield();
		textAmount.setPreferredSize(new Dimension(300, 50));
		content.add(textAmount, c);

		c.gridy++;
		JLabel lblPrice = new JLabel("Preis:");
		lblPrice.setForeground(Color.WHITE);
		lblPrice.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		content.add(lblPrice, c);

		NumberFormat format = DecimalFormat.getCurrencyInstance(Locale.GERMANY);
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setMinimum(0.0);
		formatter.setAllowsInvalid(false);

		c.gridy++;
		textPrice = new JFormattedTextField(formatter);
		textPrice.setPreferredSize(new Dimension(300, 50));
		textPrice.setValue(newPosition != null ? new Double(newPosition.getPrice().toString()) : new Double(0.0));
		textPrice.setForeground(Color.decode(ColorCodes.MAINBG));
		textPrice.setBackground(Color.WHITE);
		textPrice.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textPrice.setBorder(new EmptyBorder(7, 7, 7, 7));

		textPrice.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {

					onClickAddButton();
				}
			}
		});
		content.add(textPrice, c);

		c.gridy++;
		bAdd = BookkeeperButtons.renderNormalButton("Position hinzufügen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				onClickAddButton();
			}
		});

		content.add(bAdd, c);
		return content;

	}

	/**
	 * This method adds positions when clicking on button.
	 */
	private void onClickAddButton() {

		String articlenr = textArticleNr.getText();

		String onlyNumbersPattern = "\\d+";

		if (articlenr == null || articlenr.isEmpty() || textArticleNr.getText().matches(onlyNumbersPattern) == false) {
			JOptionPane.showMessageDialog(this, "Bitte geben sie die Artikelnummer erneut ein.");
			textArticleNr.setText("");
			return;
		}

		String desc = textDiscription.getText();
		if (desc == null || desc.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Bitte geben sie die Beschreibung erneut ein.");
			textDiscription.setText("");
			return;
		}

		String amount = textAmount.getText();
		if (amount == null || amount.isEmpty() || textAmount.getText().matches(onlyNumbersPattern) == false) {
			JOptionPane.showMessageDialog(this, "Bitte geben sie die Menge erneut ein.");
			textAmount.setText("");
			return;
		}

		String price = textPrice.getText();
		if (price == null || price.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Bitte geben sie den Preis erneut ein.");
			textPrice.setText("");
			return;
		}

		NumberFormat format = NumberFormat.getCurrencyInstance();
		try {

			Number number = format.parse(textPrice.getText());
			System.out.println(number);
			newPosition = new InvoicePosition("0", Integer.valueOf(articlenr), desc, Integer.valueOf(amount),
					new BigDecimal(Double.valueOf(number.toString())));

		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		setVisible(false);
		dispose();
	}

	/**
	 * This mathod shows dialog.
	 */
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
	
	/**
	 * This method gets new positions.
	 * @return newPositions
	 */
	public InvoicePosition getNewPosition() {
		return newPosition;
	}
}
