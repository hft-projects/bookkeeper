package gui.screens.dialogs.invoices;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;

import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperIcons;
import gui.utils.BookkeeperLogo;

/**
 * <h1>Invoice Type Selection Dialog</h1> 
 * This class provides functions to create the dialog to choose between adding a new customer or supplier invoice.
 */
public class InvoicetypeSelectionDialog extends JDialog {

	/**
	 * This method creates the dialog.
	 * 
	 * @param cForPostion
	 */
	public InvoicetypeSelectionDialog(Component cForPostion) {

		int iconWidthHeight = 20;

		setLayout(new GridBagLayout());
		setLocationRelativeTo(cForPostion);
		setResizable(false);
		setModal(true);
		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.PAGE_START;

		c.gridy = 0;
		JButton bSupplierInvoice = BookkeeperButtons.renderNormalButton("Eingangsrechnung erfassen",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						SupplierInvoiceDialog sdn = new SupplierInvoiceDialog();
						sdn.showDialog();

					}
				});
		bSupplierInvoice.setIcon(
				BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_MIUNS_ICON_FILEPATH),
						iconWidthHeight, iconWidthHeight));

		add(bSupplierInvoice, c);

		c.gridy++;
		JButton bCustomerInvoice = BookkeeperButtons.renderNormalButton("Ausgangsrechnung erstellen",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						CustomerInvoiceDialog customerDialog = new CustomerInvoiceDialog();
						customerDialog.showDialog();

					}
				});
		bCustomerInvoice.setIcon(BookkeeperIcons.getScaledIcon(
				BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_PLUS_ICON_FILEPATH), iconWidthHeight, iconWidthHeight));
		add(bCustomerInvoice, c);

	}

	/**
	 * This method shows the dialog.
	 */
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
}
