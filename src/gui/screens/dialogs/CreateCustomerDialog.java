package gui.screens.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import businessobjects.Address;
import businessobjects.Customer;
import dbaccess.CustomerAccess;
import gui.ColorCodes;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperLogo;

/**
 * <h1>Create Customer Dialog</h1> This class provides functions to create the
 * dialog to create new customers.
 */
public class CreateCustomerDialog extends JDialog {

	private JLabel lblTitel;

	private JTextField textName;
	private JTextField textEmail;
	private JTextField textPhone;
	private JTextField textStreet;
	private JTextField textPostcode;
	private JTextField textCity;
	private JTextField textCountry;
	private JButton bCreateInvoice;

	private Customer toEditCustomer;
	private Dialogmode mode;

	public enum Dialogmode {
		Create, Edit;
	};

	/**
	 * This method creates the dialog.
	 */
	public CreateCustomerDialog() {

		mode = Dialogmode.Create;

		setModal(true);

		setBounds(100, 100, 800, 1000);
		setMinimumSize(new Dimension(800, 1000));

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().add(renderContent());

		setVisible(true);
		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());

	}

	/**
	 * This method creates the dialog when editing a customer.
	 * 
	 * @param toEditCustomer
	 */
	public CreateCustomerDialog(Customer toEditCustomer) {

		mode = Dialogmode.Edit;
		this.toEditCustomer = toEditCustomer;

		setModal(true);

		setBounds(100, 100, 800, 1000);
		setMinimumSize(new Dimension(800, 1000));

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().add(renderContent());

		lblTitel.setText("Kunde bearbeiten");

		textName.setText(toEditCustomer.getName());
		textEmail.setText(toEditCustomer.getEmail());
		textPhone.setText(toEditCustomer.getTelephone());

		textStreet.setText(toEditCustomer.getAddress().getStreet());
		textPostcode.setText("" + toEditCustomer.getAddress().getPostcode());
		textCity.setText(toEditCustomer.getAddress().getCity());
		textCountry.setText(toEditCustomer.getAddress().getCountry());
		bCreateInvoice.setText("Kunde bearbeiten");

		setVisible(true);
		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());

	}

	/**
	 * This method renders the content of the dialog.
	 * 
	 * @return content panel
	 */
	public JPanel renderContent() {

		int textInputPadding = 20;
		int spaceBetweenElements = 20;
		JPanel mainPanel = new JPanel(new GridBagLayout());

		mainPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		mainPanel.setBorder(new EmptyBorder(textInputPadding, textInputPadding, textInputPadding, textInputPadding));

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.anchor = GridBagConstraints.WEST;

		lblTitel = new JLabel("Kunde erstellen");
		lblTitel.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 50));
		lblTitel.setBorder(new EmptyBorder(0, 0, 40, 0));
		mainPanel.add(lblTitel, c);

		c.gridy = c.gridy++;
		mainPanel.add(getSpace(0, spaceBetweenElements), c);

		JLabel lblName = new JLabel("Kundenname:");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		mainPanel.add(lblName, c);

		c.gridy = c.gridy++;
		textName = new JTextField();
		textName.setForeground(Color.decode(ColorCodes.MAINBG));
		textName.setBackground(Color.WHITE);
		textName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textName.setBorder(new EmptyBorder(15, 15, 15, 15));
		textName.setColumns(40);
		mainPanel.add(textName, c);

		c.gridy = c.gridy++;
		mainPanel.add(getSpace(0, spaceBetweenElements), c);

		JLabel lblEmail = new JLabel("E-Mail:");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		mainPanel.add(lblEmail, c);

		c.gridy = c.gridy++;
		textEmail = new JTextField();
		textEmail.setForeground(Color.decode(ColorCodes.MAINBG));
		textEmail.setBackground(Color.WHITE);
		textEmail.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textEmail.setBorder(new EmptyBorder(15, 15, 15, 15));
		textEmail.setColumns(40);
		mainPanel.add(textEmail, c);

		c.gridy = c.gridy++;
		mainPanel.add(getSpace(0, spaceBetweenElements), c);

		JLabel lblPhone = new JLabel("Telefon:");
		lblPhone.setForeground(Color.WHITE);
		lblPhone.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		mainPanel.add(lblPhone, c);

		c.gridy = c.gridy++;
		textPhone = new JTextField();
		textPhone.setForeground(Color.decode(ColorCodes.MAINBG));
		textPhone.setBackground(Color.WHITE);
		textPhone.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textPhone.setBorder(new EmptyBorder(15, 15, 15, 15));
		textPhone.setColumns(40);
		mainPanel.add(textPhone, c);

		c.gridy = c.gridy++;
		mainPanel.add(getSpace(0, spaceBetweenElements), c);

		c.gridy = c.gridy++;
		JLabel lblStreet = new JLabel("Straße:");
		lblStreet.setForeground(Color.WHITE);
		lblStreet.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		mainPanel.add(lblStreet, c);

		c.gridy = c.gridy++;
		textStreet = new JTextField();
		textStreet.setForeground(Color.decode(ColorCodes.MAINBG));
		textStreet.setBackground(Color.WHITE);
		textStreet.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textStreet.setBorder(new EmptyBorder(15, 15, 15, 15));
		textStreet.setColumns(40);
		mainPanel.add(textStreet, c);

		c.gridy = c.gridy++;
		mainPanel.add(getSpace(0, spaceBetweenElements), c);

		c.gridy = c.gridy++;
		JLabel lblPostcode = new JLabel("Postleitzahl:");
		lblPostcode.setForeground(Color.WHITE);
		lblPostcode.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		mainPanel.add(lblPostcode, c);

		c.gridy = c.gridy++;
		textPostcode = new JTextField();
		textPostcode.setForeground(Color.decode(ColorCodes.MAINBG));
		textPostcode.setBackground(Color.WHITE);
		textPostcode.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textPostcode.setBorder(new EmptyBorder(15, 15, 15, 15));
		textPostcode.setColumns(40);
		mainPanel.add(textPostcode, c);

		c.gridy = c.gridy++;
		mainPanel.add(getSpace(0, spaceBetweenElements), c);

		c.gridy = c.gridy++;
		JLabel lblCity = new JLabel("Stadt:");
		lblCity.setForeground(Color.WHITE);
		lblCity.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		mainPanel.add(lblCity, c);

		c.gridy = c.gridy++;
		textCity = new JTextField();
		textCity.setForeground(Color.decode(ColorCodes.MAINBG));
		textCity.setBackground(Color.WHITE);
		textCity.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textCity.setBorder(new EmptyBorder(15, 15, 15, 15));
		textCity.setColumns(40);
		mainPanel.add(textCity, c);

		c.gridy = c.gridy++;
		mainPanel.add(getSpace(0, spaceBetweenElements), c);

		c.gridy = c.gridy++;
		JLabel lblCountry = new JLabel("Land:");
		lblCountry.setForeground(Color.WHITE);
		lblCountry.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		mainPanel.add(lblCountry, c);

		c.gridy = c.gridy++;
		textCountry = new JTextField();
		textCountry.setForeground(Color.decode(ColorCodes.MAINBG));
		textCountry.setBackground(Color.WHITE);
		textCountry.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textCountry.setBorder(new EmptyBorder(15, 15, 15, 15));
		textCountry.setColumns(40);
		mainPanel.add(textCountry, c);

		c.gridy = c.gridy++;
		mainPanel.add(getSpace(0, spaceBetweenElements), c);

		c.gridy = c.gridy++;
		bCreateInvoice = BookkeeperButtons.renderNormalButton("Kunde hinzufügen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				onClickAdd();
			}
		});
		mainPanel.add(bCreateInvoice, c);
		return mainPanel;
	}

	/**
	 * This private method returns a JPanel that can be used as space between
	 * elements.
	 * 
	 * @param width
	 * @param height
	 * @return space panel
	 */
	private JPanel getSpace(int width, int height) {

		JPanel space = new JPanel();

		space.setBackground(new Color(0f, 0f, 0f, .0f));
		space.setPreferredSize(new Dimension(width, height));

		return space;

	}

	/**
	 * This method adds customer when clicking on button.
	 */
	private void onClickAdd() {

		if (textName.getText().isEmpty() || textName.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie einen Namen ein.");
			return;
		}

		String onlyEmailPattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
		if (textEmail.getText().matches(onlyEmailPattern)) {

		} else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie einen richtige Email ein.");
			return;
		}

		String onlyNumbersPattern = "\\d+";

		if (textPhone.getText().matches(onlyNumbersPattern)) {

		} else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie bei der Telefonnummer nur Zahlen ein.");
			return;
		}

		if (textStreet.getText().isEmpty() || textStreet.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie eine Straße ein.");
			return;
		}

		if (textPostcode.getText().matches(onlyNumbersPattern)) {

		} else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie bei der Postleitzahl nur Zahlen ein.");
			return;
		}

		if (textCity.getText().isEmpty() || textCity.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie eine Stadt ein.");
			return;
		}

		if (textCountry.getText().isEmpty() || textCountry.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie ein Land ein.");
			return;
		}

		Customer newCustomer = new Customer(0, textName.getText(), new Address(textStreet.getText(),
				textCountry.getText(), textCity.getText(), Integer.valueOf(textPostcode.getText())),
				textEmail.getText(), textPhone.getText());

		if (mode == Dialogmode.Create) {
			try {
				CustomerAccess.addCustomer(newCustomer);
				dispose();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} else {
			try {
				CustomerAccess.updateCustomer(toEditCustomer.getCustomerId(), newCustomer);
				dispose();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
