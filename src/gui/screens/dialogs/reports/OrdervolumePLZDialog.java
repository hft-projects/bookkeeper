package gui.screens.dialogs.reports;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import businessobjects.Supplier;
import dbaccess.SupplierAccess;
import gui.ColorCodes;
import gui.pages.AnalysisPage;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperLogo;
import services.ReportPDF;

/**
 * <h1>Order Volume PLZ Dialog</h1> 
 * This class provides functions to create the
 * dialog to create a report of the order volume depending on the supplier.
 */
public class OrdervolumePLZDialog extends JDialog {

	private JButton bCreateReport;

	private JComboBox<Supplier> supplierComboBox;
	private JTextField textYear;
	private JComboBox<String> comboBoxMonths;

	/**
	 * This method creates the dialog.
	 */
	public OrdervolumePLZDialog(Component cForPostion) {
		setModal(true);

		setModal(true);
		setBackground(Color.decode(ColorCodes.MAINBG));
		setLayout(new BorderLayout());

		setMinimumSize(new Dimension(700, 500));

		setLocationRelativeTo(cForPostion);

		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());

		add(BorderLayout.CENTER, renderForm());
		add(BorderLayout.PAGE_END, renderBottomContent());
		
		List<Supplier> suppliers;

		try {
			suppliers = SupplierAccess.getAllSuppliers();

		} catch (SQLException e) {
			suppliers = new ArrayList<Supplier>();
			e.printStackTrace();
		}

		for (Supplier supplier : suppliers) {
			supplierComboBox.addItem(supplier);
		}

	}

	/**
	 * This method renders the form.
	 * @return form
	 */
	private JPanel renderForm() {

		int padding = 20;
		int labelTextSize = 17;
		int spaceBetweenElements = 10;
		Dimension preferedComponentSize = new Dimension(400, 30);
		Border FormSidePadding = new EmptyBorder(padding, padding, padding, padding);

		JPanel mainContent = new JPanel(new GridBagLayout());
		mainContent.setBackground(getBackground());
		mainContent.setBorder(FormSidePadding);

		GridBagConstraints cl = new GridBagConstraints();
		cl.gridx = 0;
		cl.anchor = GridBagConstraints.WEST;

		cl.gridy = 0;
		JLabel lblTitel = new JLabel("Bestellvolumen pro Lieferant und Zeitraum");
		lblTitel.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 30));
		mainContent.add(lblTitel);

		cl.gridy++;
		mainContent.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		JLabel lblSupplier = new JLabel("Lieferant: ");
		lblSupplier.setForeground(Color.WHITE);
		lblSupplier.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		mainContent.add(lblSupplier, cl);

		cl.gridy++;
		mainContent.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		supplierComboBox = new JComboBox<Supplier>();
		supplierComboBox.setBackground(Color.WHITE);
		supplierComboBox.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		supplierComboBox.setPreferredSize(new Dimension(550, 40));
		mainContent.add(supplierComboBox, cl);

		cl.gridy++;
		mainContent.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		JLabel lblYear = new JLabel("Jahr: ");
		lblYear.setForeground(Color.WHITE);
		lblYear.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		mainContent.add(lblYear, cl);

		cl.gridy++;
		mainContent.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		textYear = new JTextField();
		textYear.setForeground(Color.decode(ColorCodes.MAINBG));
		textYear.setBackground(Color.WHITE);
		textYear.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textYear.setBorder(new EmptyBorder(15, 15, 15, 15));
		textYear.setColumns(40);
		mainContent.add(textYear, cl);

		cl.gridy++;
		mainContent.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		JLabel lblMonth = new JLabel("Monat (Optional): ");
		lblMonth.setForeground(Color.WHITE);
		lblMonth.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		mainContent.add(lblMonth, cl);

		cl.gridy++;
		mainContent.add(getSpace(0, spaceBetweenElements), cl);

		comboBoxMonths = new JComboBox<String>();
		comboBoxMonths.setModel(new DefaultComboBoxModel<String>(new String[] { " ", "Januar", "Februar", "März",
				"April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember" }));
		comboBoxMonths.setBackground(Color.WHITE);
		comboBoxMonths.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		comboBoxMonths.setPreferredSize(new Dimension(550, 40));
		mainContent.add(comboBoxMonths, cl);

		return mainContent;

	}

	/**
	 * This private method renders the bottom content.
	 * 
	 * @return bottom content panel
	 */
	private JPanel renderBottomContent() {

		JPanel bottomContent = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		bottomContent.setBackground(getBackground());

		bCreateReport = BookkeeperButtons.renderNormalButton("Bericht erstellen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				onClickSave();
			}
		});

		bottomContent.add(bCreateReport);

		return bottomContent;

	}

	/**
	 * This method creates order volume report when clicking on button.
	 */
	private void onClickSave() {

		File reportFolder = new File(AnalysisPage.REPORT_PATH);
		if (reportFolder.exists() == false)
			reportFolder.mkdirs();

		String onlyNumbersPattern = "\\d+";

		int year = 0;
		if (textYear.getText().matches(onlyNumbersPattern)) {
			year = Integer.valueOf(textYear.getText());
		} else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie ein passendes Jahr ein");
			return;
		}

		Supplier supplier = (Supplier)supplierComboBox.getSelectedItem();

		
		if (comboBoxMonths.getSelectedIndex() == 0) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			ReportPDF.createOrdervolumePLZYearReport(year, supplier,
					reportFolder + "\\Bestellvolumen pro Lieferant und Zeitraum (Jahr)" + format.format(new Date()) + ".pdf");

		} else {
			
			int month = getMonthNumberFromString(comboBoxMonths.getSelectedItem().toString());
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			ReportPDF.createOrdervolumePLZMonthReport(year, month, supplier,
					reportFolder + "\\Bestellvolumen pro Lieferant und Zeitraum (Monat)" + format.format(new Date()) + ".pdf");
		}

		setVisible(false);

	}

	/**
	 * This private method returns a JPanel that can be used as space between elements.
	 * 
	 * @param width
	 * @param height
	 * @return space panel
	 */
	private JPanel getSpace(int width, int height) {

		JPanel space = new JPanel();

		space.setBackground(new Color(0f, 0f, 0f, .0f));
		space.setPreferredSize(new Dimension(width, height));

		return space;

	}

	/**
	 * This private method returns the number of the submitted month.
	 * @param month
	 * @return number of month
	 */
	private int getMonthNumberFromString(String month) {
		switch (month) {
		case "Januar":
			return 1;
		case "Februar":
			return 2;
		case "März":
			return 3;
		case "April":
			return 4;
		case "Mai":
			return 5;
		case "Juni":
			return 6;
		case "Juli":
			return 7;
		case "August":
			return 8;
		case "September":
			return 9;
		case "Oktober":
			return 10;
		case "November":
			return 11;
		case "Dezember":
			return 12;
		}
		return 0;
	}

	/**
	 * This mathod shows dialog.
	 */
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
}
