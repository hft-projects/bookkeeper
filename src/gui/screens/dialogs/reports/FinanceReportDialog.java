package gui.screens.dialogs.reports;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import gui.ColorCodes;
import gui.pages.AnalysisPage;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperLogo;
import services.ReportPDF;

/**
 * <h1>Finance Report Dialog</h1> 
 * This class provides functions to create the dialog to create a finance report.
 */
public class FinanceReportDialog extends JDialog {

	private JButton bCreateReport;

	private JTextField textPostcode;
	private JTextField textYear;
	private JComboBox<String> comboBoxMonths;

	/**
	 * This method creates the dialog.
	 */
	public FinanceReportDialog(Component cForPostion) {
		setModal(true);

		setModal(true);
		setBackground(Color.decode(ColorCodes.MAINBG));
		setLayout(new BorderLayout());

		setMinimumSize(new Dimension(700, 500));

		setLocationRelativeTo(cForPostion);

		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());

		add(BorderLayout.CENTER, renderForm());
		add(BorderLayout.PAGE_END, renderBottomContent());

	}

	/**
	 * This method renders the form.
	 * @return form
	 */
	private JPanel renderForm() {

		int padding = 20;
		int labelTextSize = 17;
		int spaceBetweenElements = 10;
		Dimension preferedComponentSize = new Dimension(400, 30);
		Border FormSidePadding = new EmptyBorder(padding, padding, padding, padding);

		JPanel mainContent = new JPanel(new GridBagLayout());
		mainContent.setBackground(getBackground());
		mainContent.setBorder(FormSidePadding);

		GridBagConstraints cl = new GridBagConstraints();
		cl.gridx = 0;
		cl.anchor = GridBagConstraints.WEST;

		cl.gridy = 0;
		JLabel lblTitel = new JLabel("Jahresabschluss Einnahmen und Ausgaben");
		lblTitel.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 30));
		mainContent.add(lblTitel);

		cl.gridy++;
		mainContent.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		JLabel lblYear = new JLabel("Jahr: ");
		lblYear.setForeground(Color.WHITE);
		lblYear.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, labelTextSize));
		mainContent.add(lblYear, cl);

		cl.gridy++;
		mainContent.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		textYear = new JTextField();
		textYear.setForeground(Color.decode(ColorCodes.MAINBG));
		textYear.setBackground(Color.WHITE);
		textYear.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textYear.setBorder(new EmptyBorder(15, 15, 15, 15));
		textYear.setColumns(40);
		mainContent.add(textYear, cl);

		return mainContent;
	}

	/**
	 * This private method renders the bottom content.
	 * 
	 * @return bottom content panel
	 */
	private JPanel renderBottomContent() {

		JPanel bottomContent = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		bottomContent.setBackground(getBackground());

		bCreateReport = BookkeeperButtons.renderNormalButton("Bericht erstellen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				onClickSave();
			}
		});

		bottomContent.add(bCreateReport);
		return bottomContent;
	}

	/**
	 * This method creates finance report when clicking on button.
	 */
	private void onClickSave() {

		File reportFolder = new File(AnalysisPage.REPORT_PATH);
		if (reportFolder.exists() == false)
			reportFolder.mkdirs();

		String onlyNumbersPattern = "\\d+";

		int year = 0;
		if (textYear.getText().matches(onlyNumbersPattern)) {
			year = Integer.valueOf(textYear.getText());
		} else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie ein passendes Jahr ein");
			return;
		}

		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		ReportPDF.createFinanceReport(year,
				reportFolder + "\\Jahresabschluss (" + format.format(new Date()) +").pdf");

		setVisible(false);

	}

	/**
	 * This private method returns a JPanel that can be used as space between elements.
	 * 
	 * @param width
	 * @param height
	 * @return space panel
	 */
	private JPanel getSpace(int width, int height) {

		JPanel space = new JPanel();

		space.setBackground(new Color(0f, 0f, 0f, .0f));
		space.setPreferredSize(new Dimension(width, height));

		return space;
	}

	/**
	 * This mathod shows dialog.
	 */
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
}
