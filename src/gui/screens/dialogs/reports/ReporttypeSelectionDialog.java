package gui.screens.dialogs.reports;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperIcons;
import gui.utils.BookkeeperLogo;

/**
 * <h1>Report Type Selection Dialog</h1> 
 * This class provides functions to create the dialog to choose between different report options.
 */
public class ReporttypeSelectionDialog extends JDialog {

	/**
	 * This method creates the dialog.
	 * 
	 * @param cForPostion
	 */
	public ReporttypeSelectionDialog(Component cForPostion) {

		int iconWidthHeight = 20;

		setLayout(new GridBagLayout());
		setLocationRelativeTo(cForPostion);
		setResizable(false);
		setModal(true);
		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.PAGE_START;

		c.gridy = 0;
		JButton bRevenuePLZ = BookkeeperButtons.renderNormalButton("Einnahmen nach PLZ und Zeitraum",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						RevenuePLZDialog rpd = new RevenuePLZDialog(cForPostion);
						rpd.showDialog();
					}
				});
		bRevenuePLZ.setIcon(
				BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_PLUS_ICON_FILEPATH),
						iconWidthHeight, iconWidthHeight));
		add(bRevenuePLZ, c);

		c.gridy++;
		JButton bOrderVolume = BookkeeperButtons.renderNormalButton("Bestellvolumen pro Lieferant und Zeitraum",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						OrdervolumePLZDialog ovpd = new OrdervolumePLZDialog(cForPostion);
						ovpd.showDialog();
					}
				});
		bOrderVolume.setIcon(BookkeeperIcons.getScaledIcon(
				BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_PLUS_ICON_FILEPATH), iconWidthHeight, iconWidthHeight));
		add(bOrderVolume, c);

		c.gridy++;
		JButton bFinanceReport = BookkeeperButtons.renderNormalButton("Jahresabschluss Einnahmen und Ausgaben",
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						FinanceReportDialog frd = new FinanceReportDialog(cForPostion);
						frd.showDialog();
					}
				});
		bFinanceReport.setIcon(BookkeeperIcons.getScaledIcon(
				BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_PLUS_ICON_FILEPATH), iconWidthHeight, iconWidthHeight));
		add(bFinanceReport, c);

	}

	/**
	 * This method shows the dialog.
	 */
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
}
