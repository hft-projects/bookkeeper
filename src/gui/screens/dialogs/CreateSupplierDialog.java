package gui.screens.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import businessobjects.Address;
import businessobjects.Supplier;
import dbaccess.SupplierAccess;
import gui.ColorCodes;
import gui.utils.BookkeeperButtons;
import gui.utils.BookkeeperLogo;

/**
 * <h1>Create Supplier Dialog</h1> 
 * This class provides functions to create the dialog to create new suppliers.
 */
public class CreateSupplierDialog extends JDialog {

	private JLabel lblTitel;

	private JTextField textName;
	private JTextField textEmail;
	private JTextField textPhone;
	private JTextField textStreet;
	private JTextField textPostcode;
	private JTextField textCity;
	private JTextField textCountry;

	private JTextField textTaxNr;
	private JTextField textBankName;
	private JTextField textBankNr;
	private JTextField textBankOwner;

	private JButton bCreateSupplier;

	private Supplier toEditSupplier;
	private Dialogmode mode;

	public enum Dialogmode {
		Create, Edit;
	};

	/**
	 * This method creates the dialog.
	 */
	public CreateSupplierDialog() {

		mode = Dialogmode.Create;
		
		setModal(true);

		setBounds(100, 100, 1400, 1050);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().add(renderContent());

		setVisible(true);
		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());
	}

	/**
	 * This method creates the dialog when editing a supplier.
	 * @param toEditCustomer
	 */
	public CreateSupplierDialog(Supplier toEditSupplier) {

		this.toEditSupplier = toEditSupplier;
		mode = Dialogmode.Edit;

		setModal(true);

		setBounds(100, 100, 1400, 1050);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().add(renderContent());


		
		lblTitel.setText("Lieferant bearbeiten");

		textName.setText(toEditSupplier.getName());
		textEmail.setText(toEditSupplier.getEmail());
		textPhone.setText(toEditSupplier.getTelephone());

		textStreet.setText(toEditSupplier.getAddress().getStreet());
		textPostcode.setText("" + toEditSupplier.getAddress().getPostcode());
		textCity.setText(toEditSupplier.getAddress().getCity());
		textCountry.setText(toEditSupplier.getAddress().getCountry());

		textTaxNr.setText(toEditSupplier.getTaxNumber());
		textBankNr.setText(toEditSupplier.getBankNumber());
		textBankOwner.setText(toEditSupplier.getBankOwner());
		textBankName.setText(toEditSupplier.getBankName());

		bCreateSupplier.setText("Lieferant bearbeiten");
		
		setVisible(true);
		setIconImage(BookkeeperLogo.getLogoIconImage().getImage());
	}

	/**
	 * This method renders the content of the dialog.
	 * 
	 * @return content panel
	 */
	public JPanel renderContent() {

		int textInputPadding = 15;
		int spaceBetweenElements = 15;

		Border formPadding = new EmptyBorder(50, 50, 50, 50);

		JPanel wrapperPanel = new JPanel(new BorderLayout());
		wrapperPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		wrapperPanel.setBorder(new EmptyBorder(20, 20, 20, 20));

		JPanel topPanel = new JPanel(new GridBagLayout());
		topPanel.setBackground(wrapperPanel.getBackground());

		topPanel.setBackground(Color.decode(ColorCodes.MAINBG));
		topPanel.setBorder(new EmptyBorder(textInputPadding, textInputPadding, textInputPadding, textInputPadding));

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.anchor = GridBagConstraints.WEST;

		lblTitel = new JLabel("Lieferant erstellen");
		lblTitel.setForeground(Color.decode(ColorCodes.MAINRED));
		lblTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 50));
		lblTitel.setBorder(new EmptyBorder(0, 0, 40, 0));
		topPanel.add(lblTitel, c);

		c.gridy = c.gridy++;
		topPanel.add(getSpace(0, spaceBetweenElements), c);

		JLabel lblName = new JLabel("Lieferantenname:");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		topPanel.add(lblName, c);

		c.gridy = c.gridy++;
		textName = new JTextField();
		textName.setForeground(Color.decode(ColorCodes.MAINBG));
		textName.setBackground(Color.WHITE);
		textName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textName.setBorder(new EmptyBorder(15, 15, 15, 15));
		textName.setColumns(40);
		topPanel.add(textName, c);

		c.gridy = c.gridy++;
		topPanel.add(getSpace(0, spaceBetweenElements), c);

		JLabel lblEmail = new JLabel("E-Mail:");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		topPanel.add(lblEmail, c);

		c.gridy = c.gridy++;
		textEmail = new JTextField();
		textEmail.setForeground(Color.decode(ColorCodes.MAINBG));
		textEmail.setBackground(Color.WHITE);
		textEmail.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textEmail.setBorder(new EmptyBorder(15, 15, 15, 15));
		textEmail.setColumns(40);
		topPanel.add(textEmail, c);

		c.gridy = c.gridy++;
		topPanel.add(getSpace(0, spaceBetweenElements), c);

		JLabel lblPhone = new JLabel("Telefon:");
		lblPhone.setForeground(Color.WHITE);
		lblPhone.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		topPanel.add(lblPhone, c);

		c.gridy = c.gridy++;
		textPhone = new JTextField();
		textPhone.setForeground(Color.decode(ColorCodes.MAINBG));
		textPhone.setBackground(Color.WHITE);
		textPhone.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textPhone.setBorder(new EmptyBorder(15, 15, 15, 15));
		textPhone.setColumns(40);
		topPanel.add(textPhone, c);

		c.gridy = c.gridy++;
		topPanel.add(getSpace(0, spaceBetweenElements), c);

		wrapperPanel.add(BorderLayout.PAGE_START, topPanel);

		JPanel leftSide = new JPanel(new GridBagLayout());
		leftSide.setBackground(wrapperPanel.getBackground());
		leftSide.setBorder(formPadding);

		GridBagConstraints cl = new GridBagConstraints();
		cl.gridx = 0;
		cl.anchor = GridBagConstraints.WEST;

		cl.gridy = 0;
		JLabel lblAdresTitel = new JLabel("Adresse");
		lblAdresTitel.setForeground(Color.decode(ColorCodes.MAINRED));
		lblAdresTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		leftSide.add(lblAdresTitel, cl);

		cl.gridy++;
		JLabel lblStreet = new JLabel("Straße:");
		lblStreet.setForeground(Color.WHITE);
		lblStreet.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		leftSide.add(lblStreet, cl);

		cl.gridy++;
		textStreet = new JTextField();
		textStreet.setForeground(Color.decode(ColorCodes.MAINBG));
		textStreet.setBackground(Color.WHITE);
		textStreet.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textStreet.setBorder(new EmptyBorder(15, 15, 15, 15));
		textStreet.setColumns(40);
		leftSide.add(textStreet, cl);

		cl.gridy++;
		leftSide.add(getSpace(0, spaceBetweenElements), cl);

		cl.gridy++;
		JLabel lblPostcode = new JLabel("Postleitzahl:");
		lblPostcode.setForeground(Color.WHITE);
		lblPostcode.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		leftSide.add(lblPostcode, cl);

		cl.gridy++;
		textPostcode = new JTextField();
		textPostcode.setForeground(Color.decode(ColorCodes.MAINBG));
		textPostcode.setBackground(Color.WHITE);
		textPostcode.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textPostcode.setBorder(new EmptyBorder(15, 15, 15, 15));
		textPostcode.setColumns(40);
		leftSide.add(textPostcode, cl);

		cl.gridy++;
		leftSide.add(getSpace(0, spaceBetweenElements), c);

		cl.gridy++;
		JLabel lblCity = new JLabel("Stadt:");
		lblCity.setForeground(Color.WHITE);
		lblCity.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		leftSide.add(lblCity, cl);

		cl.gridy++;
		textCity = new JTextField();
		textCity.setForeground(Color.decode(ColorCodes.MAINBG));
		textCity.setBackground(Color.WHITE);
		textCity.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textCity.setBorder(new EmptyBorder(15, 15, 15, 15));
		textCity.setColumns(40);
		leftSide.add(textCity, cl);

		cl.gridy++;
		leftSide.add(getSpace(0, spaceBetweenElements), c);

		cl.gridy++;
		JLabel lblCountry = new JLabel("Land:");
		lblCountry.setForeground(Color.WHITE);
		lblCountry.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		leftSide.add(lblCountry, cl);

		cl.gridy++;
		textCountry = new JTextField();
		textCountry.setForeground(Color.decode(ColorCodes.MAINBG));
		textCountry.setBackground(Color.WHITE);
		textCountry.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textCountry.setBorder(new EmptyBorder(15, 15, 15, 15));
		textCountry.setColumns(40);
		leftSide.add(textCountry, cl);

		wrapperPanel.add(BorderLayout.WEST, leftSide);
		
		JPanel rightSide = new JPanel(new GridBagLayout());
		rightSide.setBackground(wrapperPanel.getBackground());
		rightSide.setBorder(formPadding);

		GridBagConstraints cr = new GridBagConstraints();
		cr.gridx = 0;
		cr.anchor = GridBagConstraints.WEST;

		cr.gridy = 0;
		JLabel lblBankTitel = new JLabel("Bankdaten");
		lblBankTitel.setForeground(Color.decode(ColorCodes.MAINRED));
		lblBankTitel.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		rightSide.add(lblBankTitel, cr);

		cr.gridy++;
		JLabel lblTaxNr = new JLabel("USt.Id.:");
		lblTaxNr.setForeground(Color.WHITE);
		lblTaxNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		rightSide.add(lblTaxNr, cr);

		cr.gridy++;
		textTaxNr = new JTextField();
		textTaxNr.setForeground(Color.decode(ColorCodes.MAINBG));
		textTaxNr.setBackground(Color.WHITE);
		textTaxNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textTaxNr.setBorder(new EmptyBorder(15, 15, 15, 15));
		textTaxNr.setColumns(40);
		rightSide.add(textTaxNr, cr);

		cr.gridy++;
		rightSide.add(getSpace(0, spaceBetweenElements), cr);

		cr.gridy++;
		JLabel lblBankName = new JLabel("Bankname:");
		lblBankName.setForeground(Color.WHITE);
		lblBankName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		rightSide.add(lblBankName, cr);

		cr.gridy++;
		textBankName = new JTextField();
		textBankName.setForeground(Color.decode(ColorCodes.MAINBG));
		textBankName.setBackground(Color.WHITE);
		textBankName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textBankName.setBorder(new EmptyBorder(15, 15, 15, 15));
		textBankName.setColumns(40);
		rightSide.add(textBankName, cr);

		cr.gridy++;
		rightSide.add(getSpace(0, spaceBetweenElements), cr);

		cr.gridy++;
		JLabel lblBankNr = new JLabel("IBAN:");
		lblBankNr.setForeground(Color.WHITE);
		lblBankNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		rightSide.add(lblBankNr, cr);

		cr.gridy++;
		textBankNr = new JTextField();
		textBankNr.setForeground(Color.decode(ColorCodes.MAINBG));
		textBankNr.setBackground(Color.WHITE);
		textBankNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textBankNr.setBorder(new EmptyBorder(15, 15, 15, 15));
		textBankNr.setColumns(40);
		rightSide.add(textBankNr, cr);

		cr.gridy++;
		rightSide.add(getSpace(0, spaceBetweenElements), cr);

		cr.gridy++;
		JLabel lblBankOwner = new JLabel("Kontoinhaber:");
		lblBankOwner.setForeground(Color.WHITE);
		lblBankOwner.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));
		rightSide.add(lblBankOwner, cr);

		cr.gridy++;
		rightSide.add(getSpace(0, spaceBetweenElements), cr);

		cr.gridy = cr.gridy++;
		textBankOwner = new JTextField();
		textBankOwner.setForeground(Color.decode(ColorCodes.MAINBG));
		textBankOwner.setBackground(Color.WHITE);
		textBankOwner.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textBankOwner.setBorder(new EmptyBorder(15, 15, 15, 15));
		textBankOwner.setColumns(40);
		rightSide.add(textBankOwner, cr);

		cr.gridy++;
		rightSide.add(getSpace(0, spaceBetweenElements), cr);

		wrapperPanel.add(BorderLayout.EAST, rightSide);

		c.gridy = c.gridy++;
		bCreateSupplier = BookkeeperButtons.renderNormalButton("Lieferant hinzufügen", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				onClickAdd();
			}
		});
		wrapperPanel.add(BorderLayout.PAGE_END, bCreateSupplier);
		return wrapperPanel;
	}

	/**
	 * This private method returns a JPanel that can be used as space between elements
	 * 
	 * @param width
	 * @param height
	 * @return space panel
	 */
	private JPanel getSpace(int width, int height) {

		JPanel space = new JPanel();

		space.setBackground(new Color(0f, 0f, 0f, .0f));
		space.setPreferredSize(new Dimension(width, height));

		return space;

	}
	
	/**
	 * This method adds supplier when clicking on button.
	 */
	private void onClickAdd() {

		if (textName.getText().isEmpty() || textName.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie einen Namen ein.");
			return;
		}

		String onlyEmailPattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
		if (textEmail.getText().matches(onlyEmailPattern)) {

		} else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie einen richtige Email ein.");
			return;
		}

		String onlyNumbersPattern = "\\d+";

		if (textPhone.getText().matches(onlyNumbersPattern)) {

		} else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie bei der Telefonnummer nur Zahlen ein.");
			return;
		}

		if (textStreet.getText().isEmpty() || textStreet.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie eine Straße ein.");
			return;
		}

		if (textPostcode.getText().matches(onlyNumbersPattern)) {

		} else {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie bei der Postleitzahl nur Zahlen ein.");
			return;
		}

		if (textCity.getText().isEmpty() || textCity.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie eine Stadt ein.");
			return;
		}

		if (textCountry.getText().isEmpty() || textCountry.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie ein Land ein.");
			return;
		}
		
		if (textTaxNr.getText().isEmpty() || textTaxNr.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie die USt.Id ein.");
			return;
		}
		
		if (textBankName.getText().isEmpty() || textBankName.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie einen Banknamen ein.");
			return;
		}
		
		if (textBankNr.getText().isEmpty() || textBankNr.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie die IBAN ein.");
			return;
		}
		
		if (textBankOwner.getText().isEmpty() || textBankOwner.getText() == " ") {
			JOptionPane.showMessageDialog(this, "Bitte geben Sie einen Kontoinhaber ein.");
			return;
		}

		Supplier newSupplier = new Supplier(0, textName.getText(),
				new Address(textStreet.getText(), textCountry.getText(), textCity.getText(),
						Integer.valueOf(textPostcode.getText())),
				textEmail.getText(), textPhone.getText(), textTaxNr.getText(), textBankName.getText(),
				textBankNr.getText(), textBankOwner.getText());

		if (mode == Dialogmode.Create) {
			try {
				SupplierAccess.addSupplier(newSupplier);
				dispose();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} else {
			try {
				SupplierAccess.updateSupplier(toEditSupplier.getSupplierId(), newSupplier);
				dispose();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
