package gui.utils;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 * <h1>BookKeeper Icons</h1> 
 * This class provides functions to get the icons in the company design.
 */
public class BookkeeperIcons {

	/**
	 * Constants for icons in company design.
	 */
	public static final String INVOICESICON_FILEPATH = "/assets/icons/invoice_icon.png";
	public static final String INVOICE_MIUNS_ICON_FILEPATH = "/assets/icons/invoice_minus_icon.png";
	public static final String INVOICE_PLUS_ICON_FILEPATH = "/assets/icons/invoice_plus_icon.png";
	public static final String INVOICE_OPEN_ICON_FILEPATH = "/assets/icons/invoice_open_icon.png";
	public static final String INVOICE_OVERDUE_ICON_FILEPATH = "/assets/icons/invoice_overdue_icon.png";
	
	public static final String INVOICE_CANCELED_ICON_FILEPATH = "/assets/icons/invoice_canceled_icon.png";
	public static final String INVOICE_INPROGRESS_ICON_FILEPATH = "/assets/icons/invoice_inprogress_icon.png";
	public static final String INVOICE_PAYIED_ICON_FILEPATH = "/assets/icons/invoice_payied_icon.png";
	
	public static final String PLUS_ICON_FILEPATH = "/assets/icons/plus_icon.png";
	public static final String REFRESH_ICON_FILEPATH = "/assets/icons/refresh_icon.png";
	
	public static final String CUSTOMER_ICON_FILEPATH = "/assets/icons/customer_icon.png";
	public static final String SUPPLIER_ICON_FILEPATH = "/assets/icons/supplier_icon.png";
	
	/**
	 * This method returns the submitted image in new dimensions
	 * @param icon
	 * @param width
	 * @param height
	 * @return image
	 */
	public static ImageIcon getScaledIcon(ImageIcon icon, int width, int height) {

		Image scaledIcon = icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);

		return new ImageIcon(scaledIcon);
	}

	/**
	 * This method returns the icon based on the submitted path
	 * @param path
	 * @return logoImage
	 */
	public static ImageIcon getIcon(String path) {
		Class currentClass = new Object() { }.getClass().getEnclosingClass();
		ImageIcon logoImage = new ImageIcon(currentClass.getClass().getResource(path));
		return logoImage;
	}	
}
