package gui.utils;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import gui.ColorCodes;

/**
 * <h1>BookKeeper Textfields</h1> 
 * This class provides function to create textfield in company design.
 */
public class BookkeeperTextfields {

	/**
	 * This method returns a textfield in the company design.
	 * 
	 * @return textfield
	 */
	public static JTextField renderNormalTextfield() {

		Color textColor = Color.decode(ColorCodes.MAINBG);
		
		JTextField textField = new JTextField();

		textField.setForeground(textColor);
		textField.setBackground(Color.WHITE);
		textField.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		textField.setBorder(new EmptyBorder(7,7,7,7));
		
		return textField;
	}
}
