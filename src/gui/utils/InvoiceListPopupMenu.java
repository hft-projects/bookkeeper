package gui.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import businessobjects.CustomerInvoice;
import businessobjects.Invoice;
import businessobjects.SupplierInvoice;
import dbaccess.CustomerInvoiceAccess;
import dbaccess.SupplierInvoiceAccess;
import gui.ColorCodes;
import gui.screens.dialogs.invoices.CustomerInvoiceDialog;
import gui.screens.dialogs.invoices.SupplierInvoiceDialog;

/**
 * <h1>Invoice List Popup Menu</h1> 
 * This class provides functions to create the Popup menu for the invoice list
 */
public class InvoiceListPopupMenu extends JPopupMenu {

	JMenuItem editInvoice;
	JMenuItem changeStatus;
	JMenuItem deleteInvoice;

	Invoice invoice;

	/**
	 * This method creates the panel.
	 */
	public InvoiceListPopupMenu(Invoice invoice) {

		this.invoice = invoice;

		Color itemBackgroundColor = Color.WHITE;
		Color itemForegroundColor = Color.decode(ColorCodes.MAINBG);
		Font itemFont = new Font("Franklin Gothic Heavy", Font.PLAIN, 15);

		editInvoice = new JMenuItem("Rechnung bearbeiten");
		editInvoice.setBackground(itemBackgroundColor);
		editInvoice.setForeground(itemForegroundColor);
		editInvoice.setFont(itemFont);
		editInvoice.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (invoice.getClass().getSimpleName().equals("SupplierInvoice")) {

					SupplierInvoice toEditInvoice = (SupplierInvoice) invoice;
					SupplierInvoiceDialog spid = new SupplierInvoiceDialog(toEditInvoice);
					spid.showDialog();

				} else if (invoice.getClass().getSimpleName().equals("CustomerInvoice")) {
					CustomerInvoice toEditInvoice = (CustomerInvoice) invoice;
					CustomerInvoiceDialog cid = new CustomerInvoiceDialog(toEditInvoice);
					cid.showDialog();
				} else {
					return;
				}

			}
		});
		add(editInvoice);

		add(getStatusSubmenu(itemBackgroundColor, itemForegroundColor, itemFont));

		setBackground(Color.decode(ColorCodes.MAINBG));
		setForeground(Color.decode(ColorCodes.MAINRED));

	}

	/**
	 * This private method creates sub menu to quick edit the status of an invoice.
	 * @param background
	 * @param foreground
	 * @param font
	 * @return edit menu
	 */
	private JMenu getStatusSubmenu(Color background, Color foreground, Font font) {

		JMenu subMenu = new JMenu("Status ändern zu...");
		subMenu.setBackground(background);
		subMenu.setForeground(foreground);
		subMenu.setFont(font);
		subMenu.setOpaque(true);

		JMenuItem itemOpenStatus = new JMenuItem("Offen");
		itemOpenStatus.setBackground(background);
		itemOpenStatus.setForeground(foreground);
		itemOpenStatus.setFont(font);
		itemOpenStatus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editInvoiceStatus(Invoice.STATUS_OPEN);

			}
		});
		subMenu.add(itemOpenStatus);

		JMenuItem itemInProcessStatus = new JMenuItem("In Bearbeitung");
		itemInProcessStatus.setBackground(background);
		itemInProcessStatus.setForeground(foreground);
		itemInProcessStatus.setFont(font);
		itemInProcessStatus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editInvoiceStatus(Invoice.STATUS_IN_PROCESS);
			}
		});
		subMenu.add(itemInProcessStatus);

		JMenuItem itemPayedStatus = new JMenuItem("Bezahlt");
		itemPayedStatus.setBackground(background);
		itemPayedStatus.setForeground(foreground);
		itemPayedStatus.setFont(font);
		itemPayedStatus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editInvoiceStatus(Invoice.STATUS_PAID);

			}
		});
		subMenu.add(itemPayedStatus);

		JMenuItem itemCanceledStatus = new JMenuItem("Storniert");
		itemCanceledStatus.setBackground(background);
		itemCanceledStatus.setForeground(foreground);
		itemCanceledStatus.setFont(font);
		itemCanceledStatus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editInvoiceStatus(Invoice.STATUS_CANCELED);

			}
		});
		subMenu.add(itemCanceledStatus);

		JMenuItem itemOverdueStatus = new JMenuItem("Überfällig");
		itemOverdueStatus.setBackground(background);
		itemOverdueStatus.setForeground(foreground);
		itemOverdueStatus.setFont(font);
		itemOverdueStatus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editInvoiceStatus(Invoice.STATUS_OVERDUE);

			}
		});
		subMenu.add(itemOverdueStatus);

		return subMenu;

	}
	
	/**
	 * This method edits the invoice status.
	 * 
	 * @param invoiceStatus
	 */
	private void editInvoiceStatus(int invoiceStatus) {
		
		if (invoice.getClass().getSimpleName().equals("SupplierInvoice")) {

			SupplierInvoice newInvoice = (SupplierInvoice)invoice;
			newInvoice.setStatus(invoiceStatus);
			
			try {
				SupplierInvoiceAccess.updateSupplierInvoice(newInvoice);
			} catch (SQLException e) {
				e.printStackTrace();
			}

		} else if (invoice.getClass().getSimpleName().equals("CustomerInvoice")) {

			CustomerInvoice newInvoice = (CustomerInvoice)invoice;
			newInvoice.setStatus(invoiceStatus);
			
			try {
				CustomerInvoiceAccess.updateCustomerInvoice(newInvoice);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} else {
			return;
		}	
	}
}
