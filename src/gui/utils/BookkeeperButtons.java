package gui.utils;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import gui.ColorCodes;

/**
 * <h1>BookKeeper Buttons</h1> 
 * This class provides functions to create buttons in different company styles.
 */
public class BookkeeperButtons {

	public static final int BUTTONPADDING = 20;

	/**
	 * This method returns a normal button in the company design.
	 * @param text
	 * @param actionlistener
	 * @return normal button
	 */
	public static JButton renderNormalButton(String text, ActionListener actionlistener) {

		JButton newButton = renderButton(text, actionlistener);
		newButton.setFocusable(false);

		newButton.setBackground(Color.decode(ColorCodes.MAINRED));
		newButton.setForeground(Color.decode(ColorCodes.MAINBG));

		return newButton;
	}

	/**
	 * This method returns a normal selected button in the company design.
	 * @param text
	 * @param actionlistener
	 * @return normal selected button
	 */
	public static JButton renderNormalSelectedButton(String text, ActionListener actionlistener) {

		JButton newButton = renderButton(text, actionlistener);

		newButton.setBackground(Color.decode(ColorCodes.MAINRED));
		newButton.setForeground(Color.decode(ColorCodes.MAINBG));

		return newButton;
	}

	/**
	 * This method returns a top bar selection button in the company design.
	 * @param text
	 * @param actionlistener
	 * @return top bar selection button
	 */
	public static JButton renderInvoicePageTopBarSelectionButton(String text, ActionListener actionlistener) {

		JButton newButton = renderButton(text, actionlistener);
		newButton.setFocusable(true);

		newButton.setBackground(Color.WHITE);
		newButton.setForeground(Color.decode(ColorCodes.MAINBG));

		newButton.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
	
			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		return newButton;
	}

	/**
	 * This method returns a flat button in the company design.
	 * @param text
	 * @param actionlistener
	 * @return flatbutton
	 */
	public static JButton renderFlatButton(String text, ActionListener actionlistener) {

		JButton newButton = renderButton(text, actionlistener);

		newButton.setBackground(Color.decode(ColorCodes.MAINBG));
		newButton.setForeground(Color.decode(ColorCodes.MAINRED));

		return newButton;
	}

	/**
	 * This method returns a circular plus button in the company design.
	 * @param actionlistener
	 * @return circular plus button
	 */
	public static JButton renderPlusButton(ActionListener actionlistener) {

		ImageIcon plusImage = BookkeeperIcons.getScaledIcon(BookkeeperIcons.getIcon(BookkeeperIcons.INVOICE_PLUS_ICON_FILEPATH), 45, 45);

		JButton newButton = new JButton(plusImage);
		newButton.setBorder(new EmptyBorder(20, 20, 20, 20));
		newButton.setBackground(Color.decode(ColorCodes.MAINRED));
		newButton.addActionListener(actionlistener);
		newButton.setCursor(new Cursor(Cursor.HAND_CURSOR));

		return newButton;
	}

	/**
	 * This method returns a red side menu button in the company design.
	 * @param text
	 * @param actionlistener
	 * @param preferedSize
	 * @return red side menu button
	 */
	public static JButton renderSidemenuButton(String text, ActionListener actionlistener, Dimension preferedSize) {

		JButton newButton = new JButton();

		newButton.setText(text);
		newButton.setBorder(new EmptyBorder(BUTTONPADDING, BUTTONPADDING, BUTTONPADDING, BUTTONPADDING));
		newButton.addActionListener(actionlistener);

		newButton.setFont(new Font(newButton.getFont().getName(), newButton.getFont().getStyle(), 13));
		newButton.setHorizontalAlignment(SwingConstants.LEFT);

		newButton.setBackground(Color.decode(ColorCodes.MAINRED));
		newButton.setForeground(Color.WHITE);

		newButton.setPreferredSize(preferedSize);

		newButton.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {

				newButton.setFont(
						new Font(newButton.getFont().getName(), Font.CENTER_BASELINE, newButton.getFont().getSize()));
				newButton.setBackground(Color.decode(ColorCodes.MAINRED));
				newButton.setForeground(Color.WHITE);
				newButton.setBorder(new EmptyBorder(BUTTONPADDING, BUTTONPADDING, BUTTONPADDING, BUTTONPADDING));
			}

			@Override
			public void focusGained(FocusEvent e) {

				newButton.setBorder(new EmptyBorder(BUTTONPADDING, BUTTONPADDING, BUTTONPADDING, BUTTONPADDING));
				newButton.setBorder(new LineBorder(Color.decode(ColorCodes.MAINBG), BUTTONPADDING));
				newButton.setBackground(Color.decode(ColorCodes.MAINBG));
				newButton.setForeground(Color.WHITE);

			}
		});

		return newButton;
	}

	/**
	 * This method returns a general button to add different styles to.
	 * @param text
	 * @param actionlistener
	 * @return general button
	 */
	private static JButton renderButton(String text, ActionListener actionlistener) {

		JButton newButton = new JButton();

		newButton.setText(text);
		newButton.setBorder(new EmptyBorder(BUTTONPADDING, BUTTONPADDING, BUTTONPADDING, BUTTONPADDING));
		newButton.addActionListener(actionlistener);
		newButton.setFocusable(true);
		newButton.setFont(new Font(newButton.getFont().getName(), newButton.getFont().getStyle(), 15));

		newButton.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {

				newButton.setFont(
						new Font(newButton.getFont().getName(), Font.CENTER_BASELINE, newButton.getFont().getSize()));
			}

			@Override
			public void focusGained(FocusEvent e) {

				HashMap<TextAttribute, Object> textAttrMap = new HashMap<TextAttribute, Object>();
				textAttrMap.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);

				newButton.setBorder(new EmptyBorder(BUTTONPADDING, BUTTONPADDING, BUTTONPADDING, BUTTONPADDING));
				newButton.setBorder(new LineBorder(Color.decode(ColorCodes.MAINBG), BUTTONPADDING));

				newButton.setFont(newButton.getFont().deriveFont(textAttrMap));

			}
		});

		return newButton;
	}
}
