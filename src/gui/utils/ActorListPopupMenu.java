package gui.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import businessobjects.Actor;
import businessobjects.Customer;
import businessobjects.Supplier;

import gui.ColorCodes;
import gui.screens.dialogs.CreateCustomerDialog;
import gui.screens.dialogs.CreateSupplierDialog;

/**
 * <h1>Actor List Popup Menu</h1> 
 * This class provides functions to create the Popup menu for the actor list
 */
public class ActorListPopupMenu extends JPopupMenu {

	JMenuItem editActor;
	JMenuItem deleteActor;

	Actor actor;

	/**
	 * This method creates the panel.
	 */
	public ActorListPopupMenu(Actor actor) {

		this.actor = actor;

		Color itemBackgroundColor = Color.WHITE;
		Color itemForegroundColor = Color.decode(ColorCodes.MAINBG);
		Font itemFont = new Font("Franklin Gothic Heavy", Font.PLAIN, 15);

		editActor = new JMenuItem(getActorTypeString() + " bearbeiten");
		editActor.setBackground(itemBackgroundColor);
		editActor.setForeground(itemForegroundColor);
		editActor.setFont(itemFont);
		editActor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (actor.getClass().getSimpleName().equals("Supplier")) {
					Supplier toEditSupplier = (Supplier)actor;
					CreateSupplierDialog csd = new CreateSupplierDialog(toEditSupplier);
					
					
				} else {
					Customer toEditCustomer = (Customer)actor;
					CreateCustomerDialog ccd = new CreateCustomerDialog(toEditCustomer);
				}
				
			}
		});
		add(editActor);

		setBackground(Color.decode(ColorCodes.MAINBG));
		setForeground(Color.decode(ColorCodes.MAINRED));

	}
	
	/**
	 * This private method differanciates between customer or supplier.
	 * 
	 * @return customer or supplier string
	 */
	private String getActorTypeString() {

		if (actor.getClass().getSimpleName().equals("Supplier")) {
			return "Lieferant";
		} else {
			return "Kunde";
		}
	}
}
