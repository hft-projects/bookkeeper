package gui.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.sql.SQLException;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import businessobjects.Actor;
import businessobjects.Invoice;
import dbaccess.ActorAccess;
import gui.ColorCodes;

/**
 * <h1>Invoice Renderer</h1> 
 * This class provides functions to create the panel for invoices.
 */
public class InvoiceRenderer extends JPanel implements ListCellRenderer<Invoice> {

	JPanel mainPanel = new JPanel(new BorderLayout());
	
	JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	JPanel leftSide = new JPanel(new FlowLayout(FlowLayout.LEFT));
	JPanel rightSide = new JPanel(new FlowLayout(FlowLayout.RIGHT));

	JLabel lblStatus = new JLabel();
	JLabel lblActorName = new JLabel();
	JLabel lblInvoiceNr = new JLabel();

	/**
	 * This method creates the panel.
	 */
	public InvoiceRenderer() {

		setOpaque(true);
		setPreferredSize(new Dimension(300, 80));
		setLayout(new BorderLayout());
		setBackground(Color.WHITE);
		setForeground(Color.decode(ColorCodes.MAINBG));
		setBorder(new LineBorder(Color.decode(ColorCodes.MAINBG), 7));
		
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setPreferredSize(new Dimension(300, 60));
		
		lblActorName.setBorder(new EmptyBorder(5,0,0,0));
		
		lblStatus.setForeground(Color.decode(ColorCodes.MAINBG));
		lblActorName.setForeground(Color.decode(ColorCodes.MAINRED));
		lblInvoiceNr.setForeground(Color.decode(ColorCodes.MAINRED));

		lblStatus.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		lblInvoiceNr.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));
		lblActorName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));

		topPanel.setBackground(this.getBackground());
		topPanel.add(lblActorName);
		mainPanel.add(BorderLayout.NORTH, topPanel);

		leftSide.setBackground(this.getBackground());
		leftSide.add(lblStatus);
		mainPanel.add(BorderLayout.WEST, leftSide);

		rightSide.setBackground(this.getBackground());
		rightSide.add(lblInvoiceNr);
		mainPanel.add(BorderLayout.EAST, rightSide);
		
		add(BorderLayout.CENTER, mainPanel);

	}

	/**
	 * This method gets list cell renderer component.
	 */
	@Override
	public Component getListCellRendererComponent(JList<? extends Invoice> list, Invoice invoice, int index,
			boolean isSelected, boolean cellHasFocus) {

		Actor actor = null;
		try {
			actor = ActorAccess.getActorFromInvoice(invoice);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (actor == null) {
			return null;
		}

		String actorName = actor.getName();
		String status = getStatusStringFromInt(invoice.getStatus());
		String invoiceNr = invoice.getPrefixedInvoiceId();

		lblStatus.setText(status);
		lblActorName.setText(actorName);
		lblInvoiceNr.setText(invoiceNr);

		if (isSelected) {

			setBackground(Color.decode(ColorCodes.MAINRED));

			leftSide.setBackground(this.getBackground());
			rightSide.setBackground(this.getBackground());
			topPanel.setBackground(this.getBackground());
			mainPanel.setBackground(this.getBackground());

			lblStatus.setForeground(Color.WHITE);
			lblActorName.setForeground(Color.WHITE);
			lblInvoiceNr.setForeground(Color.WHITE);

		} else {

			setBackground(Color.WHITE);

			leftSide.setBackground(this.getBackground());
			rightSide.setBackground(this.getBackground());
			topPanel.setBackground(this.getBackground());
			mainPanel.setBackground(this.getBackground());
			
			lblStatus.setForeground(Color.decode(ColorCodes.MAINBG));
			lblActorName.setForeground(Color.decode(ColorCodes.MAINRED));
			lblInvoiceNr.setForeground(Color.decode(ColorCodes.MAINRED));

		}

		return this;
	}

	/**
	 * This method converts and returns the status constatns into string values.
	 * @param status
	 * @return status
	 */
	public static String getStatusStringFromInt(int status) {

		switch (status) {

		case Invoice.STATUS_CANCELED:
			return "Storniert";
		case Invoice.STATUS_IN_PROCESS:
			return "In Bearbeitung";
		case Invoice.STATUS_OVERDUE:
			return "Überfällig";
		case Invoice.STATUS_OPEN:
			return "Offen";
		case Invoice.STATUS_PAID:
			return "Bezahlt";

		}
		return null;
	}
}