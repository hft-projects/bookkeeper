package gui.utils;

import java.io.File;
import javax.swing.ImageIcon;

/**
 * <h1>BookKeeper Logo</h1> 
 * This class provides functions to get the company logo
 */
public class BookkeeperLogo {

	static final String LOGO_FILEPATH = "/assets/logo.png";
	static final String LOGOICON_FILEPATH = "/assets/logo icon trans.png";

	/**
	 * This method returns the comapny logo
	 * 
	 * @return logoImage
	 */
	public static ImageIcon getLogoImage() {

		Class currentClass = new Object() { }.getClass().getEnclosingClass();
		File logoFile = new File(LOGO_FILEPATH);

		ImageIcon logoImage = new ImageIcon(currentClass.getClass().getResource(LOGO_FILEPATH));
		return logoImage;

	}

	/**
	 * This method returns the company logo icon
	 * 
	 * @return logoImage as icon
	 */
	public static ImageIcon getLogoIconImage() {

		Class currentClass = new Object() { }.getClass().getEnclosingClass();
		File logoFile = new File(LOGOICON_FILEPATH);

		ImageIcon logoImage = new ImageIcon(currentClass.getClass().getResource(LOGOICON_FILEPATH));
		return logoImage;
	}
}
