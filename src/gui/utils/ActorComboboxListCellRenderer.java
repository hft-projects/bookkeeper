package gui.utils;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import businessobjects.Actor;
import gui.ColorCodes;

/**
 * <h1>Actor Combobox List Cell Renderer</h1> 
 * This class provides functions to create the panel for actor combobox.
 */
public class ActorComboboxListCellRenderer extends JLabel implements ListCellRenderer<Actor> {

	/**
	 * This method gets list cell renderer component.
	 */
	@Override
	public Component getListCellRendererComponent(JList<? extends Actor> list, Actor actor, int index,
			boolean isSelected, boolean cellHasFocus) {

		setText(actor.getName());
		setSize(200, 200);

		setBackground(Color.WHITE);
		setForeground(Color.decode(ColorCodes.MAINBG));

		list.setBackground(Color.WHITE);

		list.setSelectionForeground(Color.decode(ColorCodes.MAINRED));

		if (isSelected) {

			setForeground(Color.decode(ColorCodes.MAINRED));

			list.setSelectionForeground(Color.decode(ColorCodes.MAINRED));
		}
		return this;
	}
}
