package gui.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import gui.ColorCodes;

/**
 * <h1>Report Renderer</h1> 
 * This class provides functions to create the panel for company reports.
 */
public class ReportRenderer extends JPanel implements ListCellRenderer<File> {

	private JPanel contentPanel;
	
	private JLabel lblFileName = new JLabel();
	private JLabel lblDate = new JLabel();

	/**
	 * This method creates the panel.
	 */
	public ReportRenderer() {

		setOpaque(true);
		setPreferredSize(new Dimension(300, 80));
		setLayout(new BorderLayout());
		setBackground(Color.WHITE);
		setForeground(Color.decode(ColorCodes.MAINBG));
		setBorder(new LineBorder(Color.decode(ColorCodes.MAINBG), 7));

		contentPanel = new JPanel(new GridBagLayout());
		contentPanel.setBackground(getBackground());
		contentPanel.setBorder(new EmptyBorder(10,10,10,10));

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.PAGE_START;

		lblFileName.setForeground(Color.decode(ColorCodes.MAINRED));
		lblFileName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));

		lblDate.setForeground(Color.decode(ColorCodes.MAINBG));
		lblDate.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));

		c.gridy = 0;
		contentPanel.add(lblFileName, c);

		c.gridy++;
		contentPanel.add(lblDate, c);

		add(BorderLayout.CENTER, contentPanel);

	}
	
	/**
	 * This method gets list cell renderer component.
	 */
	@Override
	public Component getListCellRendererComponent(JList<? extends File> list, File file, int index,
			boolean isSelected, boolean cellHasFocus) {

		String fileName = file.getName();
		String fileCreationDate = getFileCreationDate(file);
		
		lblFileName.setText(fileName);
		lblDate.setText(fileCreationDate);

		if (isSelected) {

			setBackground(Color.decode(ColorCodes.MAINRED));

			contentPanel.setBackground(getBackground());
			
			lblFileName.setForeground(Color.WHITE);
			lblDate.setForeground(Color.WHITE);

		} else {

			setBackground(Color.WHITE);

			contentPanel.setBackground(getBackground());
			
			lblFileName.setForeground(Color.decode(ColorCodes.MAINRED));
			lblDate.setForeground(Color.decode(ColorCodes.MAINBG));

		}
		return this;
	}
	
	/**
	 * This private method gets the file creation date.
	 * @param file
	 * @return creation date
	 */
	private String getFileCreationDate(File file) {
		

		BasicFileAttributes attrs;

		try {
		    attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
		    FileTime time = attrs.creationTime();
		    
		    String pattern = "dd.MM.yyyy hh:mm";
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			
		    String formatted = simpleDateFormat.format( new Date( time.toMillis() ) );

		    return formatted;

		} catch (IOException e) {
		    e.printStackTrace();
		    return "";
		}
	}
}
