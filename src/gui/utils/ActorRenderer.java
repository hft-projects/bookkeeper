package gui.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import businessobjects.Actor;
import gui.ColorCodes;

/**
 * <h1>Actor Renderer</h1> 
 * This class provides functions to create the panel for actors.
 */
public class ActorRenderer extends JPanel implements ListCellRenderer<Actor> {

	private JPanel contentPanel;
	
	private JLabel lblName = new JLabel();
	private JLabel lblEmail = new JLabel();

	/**
	 * This method creates the panel.
	 */
	public ActorRenderer() {

		setOpaque(true);
		setPreferredSize(new Dimension(300, 80));
		setLayout(new BorderLayout());
		setBackground(Color.WHITE);
		setForeground(Color.decode(ColorCodes.MAINBG));
		setBorder(new LineBorder(Color.decode(ColorCodes.MAINBG), 7));

		contentPanel = new JPanel(new GridBagLayout());
		contentPanel.setBackground(getBackground());
		contentPanel.setBorder(new EmptyBorder(10,10,10,10));

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.PAGE_START;

		lblName.setForeground(Color.decode(ColorCodes.MAINRED));
		lblName.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 20));

		lblEmail.setForeground(Color.decode(ColorCodes.MAINBG));
		lblEmail.setFont(new Font("Franklin Gothic Heavy", Font.PLAIN, 15));

		c.gridy = 0;
		contentPanel.add(lblName, c);

		c.gridy++;
		contentPanel.add(lblEmail, c);

		add(BorderLayout.CENTER, contentPanel);

	}

	/**
	 * This method gets list cell renderer component.
	 */
	@Override
	public Component getListCellRendererComponent(JList<? extends Actor> list, Actor actor, int index,
			boolean isSelected, boolean cellHasFocus) {

		String actorName = actor.getName();
		String actorEmail = actor.getEmail();
		
		lblName.setText(actorName);
		lblEmail.setText(actorEmail);

		if (isSelected) {

			setBackground(Color.decode(ColorCodes.MAINRED));

			contentPanel.setBackground(getBackground());
			
			lblName.setForeground(Color.WHITE);
			lblEmail.setForeground(Color.WHITE);

		} else {

			setBackground(Color.WHITE);

			contentPanel.setBackground(getBackground());
			
			lblName.setForeground(Color.decode(ColorCodes.MAINRED));
			lblEmail.setForeground(Color.decode(ColorCodes.MAINBG));

		}
		return this;
	}
}
